import P5 from 'p5'
import { create, all } from 'mathjs'

const math = create(all)


export const palette = {
	'soft brown': '#51391aff',
	'soft red': '#bb3826ff',
	'dark brown': '#342515ff',
	'soft green': '#205f49ff',
	'greenish ash'  : '#8ca69cff',
	'deep dark blue'  : '#0f1324ff',
	'pupil black': '#0e110cff',
	'voodoo yellow'  : '#ffdba1ff',
	'voodoo orange'  : '#bb4026ff',
	'voodoo carmin'  : '#9f2820ff',
	'soft yellow'  : '#eea721ff', 
	'night blue'  : '#252767ff',
	'sea azure'  : '#02a2ddff'
}


function da_state() 
{
	return [palette['voodoo orange'], tse_state]
}


function tse_state()
{
	return [palette['voodoo yellow'], da_state]
}


/**
* Converts from math.matrix to p5 Vector
*
* @param vec 2x1 vector.
*/
export function to_point(p5, vec: math.Matrix, scale: number): P5.Vector {
    var scale_vec = math.multiply(vec, scale)
    return p5.createVector(
        math.subset(scale_vec, math.index(0,0)),
        math.subset(scale_vec, math.index(1,0)));
    }


export type Movement = {
    dx: number;
    dy: number;
    dtheta: number;
}


export class Chronos
{
    p5
    last_press
    this_press
    animation_pulse: number
    distance_since_last_crumb: number
    update_trajectory: boolean

    constructor(p5)
    {
        this.p5 = p5
        this.animation_pulse = 0
        this.distance_since_last_crumb = 0
        this.update_trajectory = false
    }

    animate(): Movement
    {
		const epsilon = 0.01
		// moving
		let dx = 0
		let dy = 0
		let dtheta = 0
		let this_press = this.p5.last_press;
		// x axis
		if (this.p5.keyIsDown(this.p5.LEFT_ARROW)) {
			dx -= epsilon;
			this.distance_since_last_crumb += epsilon
			this_press = this.p5.LEFT_ARROW;
		}
		if (this.p5.keyIsDown(this.p5.RIGHT_ARROW)) {
			dx += epsilon;
			this.distance_since_last_crumb += epsilon
			this_press = this.p5.RIGHT_ARROW;	
		}

		// y axis
		if (this.p5.keyIsDown(this.p5.UP_ARROW)) {
			dy += epsilon;
			this.distance_since_last_crumb += epsilon
			this_press = this.p5.UP_ARROW;
		}
		if (this.p5.keyIsDown(this.p5.DOWN_ARROW)) {
			dy -= epsilon;
			this.distance_since_last_crumb += epsilon
			this_press = this.p5.DOWN_ARROW;
		}
		
		// rotation
		if (this.p5.keyIsDown(65)) { // press a
			dtheta += 1*epsilon
			this_press = 65;
		}
		if (this.p5.keyIsDown(68)) {// press d
			dtheta -= 1*epsilon
			// here we put the same point as for turning in the opposite direction
			this_press = 65;
		}	

		// if moving we need to update the animation timer
		if(dx != 0 || dy != 0 || dtheta != 0)
			this.animation_pulse++;

		// add a new bread crumb to the trajectory
		if (this.distance_since_last_crumb > 0.1 || this_press != this.last_press) {
            this.update_trajectory = true
		}

		this.last_press = this_press;

        return {
            dx: dx,
            dy: dy,
            dtheta: dtheta
        }
    }

    should_update_trajectory()
    {
	    return this.update_trajectory
    }

    updated_trajectory()
    {
        this.distance_since_last_crumb = 0
        this.update_trajectory = false
    }
}


export class Arrow
{
    p5
    scale_unit: number

    constructor(p5, scale_unit)
    {
        this.p5 = p5
        this.scale_unit = scale_unit
    }
	
    draw(coord_chart: (point: math.Matrix) => math.Matrix, 
         center: math.Matrix,
         tangent_vec: math.Matrix,
         color
        ) : void
    {
        var vec_on_unit_chart = coord_chart(math.add(center, tangent_vec)) 
        if(vec_on_unit_chart) {
            var vec_on_canvas = to_point(this.p5, vec_on_unit_chart, this.scale_unit)
            this.drawArrow(this.p5.createVector(0,0), vec_on_canvas.mult(1/2), color)
        }

    }

    /* Draw an arrow for a vector at a given base position.
     */
    drawArrow(base: P5.Vector, vec: P5.Vector, myColor): void
    {
		const arrowSize = 7;
		this.p5.push();
		this.p5.stroke(myColor);
		this.p5.strokeWeight(2);
		this.p5.fill(myColor);
		this.p5.translate(base.x, base.y);
		this.p5.line(0, 0, vec.x, vec.y);
		this.p5.rotate(vec.heading());
		this.p5.translate(vec.mag() - arrowSize, 0);
		this.p5.triangle(0, arrowSize / 2, 0, -arrowSize / 2, arrowSize, 0);
		this.p5.pop();
    }

}


export class KeyboardHints {
	keyboard_hint_img: P5.Image;
	keyboard_hint_url: string;
	p5_handle;

	constructor(p5_handle, keyboard_hint_url: string)
	{
		this.keyboard_hint_url = keyboard_hint_url
		this.p5_handle = p5_handle
	}

	preload() : void
	{
		this.keyboard_hint_img = this.p5_handle.loadImage(this.keyboard_hint_url);
	}

	draw() : void
	{
		this.p5_handle.push()
		this.p5_handle.scale(1, -1)
		this.p5_handle.textSize(18);
		this.p5_handle.fill(palette['greenish ash'])

		let keyboard_pos_x = -this.p5_handle.width/2*8/10;
		let keyboard_pos_y = this.p5_handle.height/2*8/10;
		let img_width = 100;
		let img_height = 33;
		this.p5_handle.image(
			this.keyboard_hint_img, 
			keyboard_pos_x, 
			keyboard_pos_y, 
			img_width, 
			img_height
		)
		this.p5_handle.text(
			"Move with the keyboard arrow keys, A and D to rotate.", 
			keyboard_pos_x+img_width+10, 
			keyboard_pos_y+16
		) 
		this.p5_handle.pop()
	}
}


export class DebugText {

	static SEPARATOR = "#-#!".repeat(8)
	p5_handle;
	scale_unit: Number;

	constructor(p5_handle, scale_unit: Number)
	{
		this.p5_handle = p5_handle
		this.scale_unit = scale_unit
	}

	draw(debug_info: string[], debug: boolean): void
	{
		if(!debug)
		{
            return
        }

		this.p5_handle.push()
		this.p5_handle.scale(1, -1)
		this.p5_handle.textSize(18);
		this.p5_handle.fill(palette['greenish ash'])

		var text_y_offset = 1;
		for(let i=0;i < debug_info.length;i++) 
		{
			this.p5_handle.text(
				debug_info[i],
				-this.scale_unit*1.5,
				-this.scale_unit/2*text_y_offset
			);
			text_y_offset -= 0.1
		}

		this.p5_handle.pop()
	}
}


export class FpsMeter {

	p5_handle;
	fps: number;

	constructor(p5_handle)
	{
		this.p5_handle = p5_handle
		this.fps = 0
	}


	update(): void
	{
		if(this.fps < 1)
				this.fps = this.p5_handle.frameRate()
		let smoothing_ratio = 0.99
		this.fps = smoothing_ratio*this.fps + (1-smoothing_ratio)*this.p5_handle.frameRate();
	}

	toString(): string
	{
		return this.fps.toFixed(0)
	}
}


export class MoppetAnimation
{
	p5_handle;
	sprite_urls: string[];
	sprites: P5.Image[];
	n_sprites: number;
	moppet_size_pixels: number; 
	animation_speed: number; 

	constructor(p5_handle, sprite_urls: string[], moppet_size_pixels : number)
	{
		this.p5_handle = p5_handle
		this.sprite_urls = sprite_urls
		this.sprites = []
		this.n_sprites = this.sprite_urls.length
		this.moppet_size_pixels = moppet_size_pixels
		this.animation_speed = 1/4
	}

	preload() : void
	{
		this.sprite_urls.forEach(
			url => this.sprites.push(this.p5_handle.loadImage(url))
		)
	}

	draw(animation_pulse) : void
	{
		// we always plot at 0,0, should we just remove this???
		let p_canvas = this.p5_handle.createVector(0,0)
		this.p5_handle.push()
		this.p5_handle.translate(
			p_canvas.x-this.moppet_size_pixels/2,
		   	p_canvas.y+this.moppet_size_pixels/2
		)
		this.p5_handle.scale(1, -1);
		// plot the moppet
		this.p5_handle.image(
			this.sprites[math.floor(animation_pulse*this.animation_speed) % this.n_sprites],
			0, 0, 
			this.moppet_size_pixels, this.moppet_size_pixels
		);
		this.p5_handle.pop()
	}
}


export class Trajectory
{
	p5_handle
	// All our breadcrumbs, these are points of the Manifold "S"
	trajectory : math.Matrix[];

	constructor(p5_handle)
	{
		this.p5_handle = p5_handle
		this.trajectory = []
	}

	draw(coord_chart: (p: math.Matrix) => math.Matrix, scale_unit: number) : void
	{
		this.p5_handle.push()
		this.p5_handle.strokeWeight(2)
		this.p5_handle.stroke(palette["greenish ash"])
		var this_state = da_state
		for(let i = 0;i < this.trajectory.length-1;i++) {
			// get the color for this point
			var output = this_state()
			this_state = output[1]
			// project point i
			var this_point = coord_chart(this.trajectory[i])
			if(this_point == null)
				continue;
			var this_p5_point = this.to_point(this_point, scale_unit)
			// project point i+1
			var next_point = coord_chart(this.trajectory[i+1])
			if(next_point == null)
				continue;
			var next_p5_point = this.to_point(next_point, scale_unit)
			// plot
			this.p5_handle.stroke(output[0])
			this.p5_handle.point(this_p5_point.x, this_p5_point.y)
			this.p5_handle.line(this_p5_point.x, this_p5_point.y, 
					    next_p5_point.x, next_p5_point.y
					    )
		}
		if(this.trajectory.length){
			var canvas_center = this.p5_handle.createVector(0,0)
			// project last point
			var this_point = coord_chart(this.trajectory[this.trajectory.length-1])
			if(this_point) {
				var this_p5_point = this.to_point(this_point, scale_unit)
				this.p5_handle.point(this_p5_point.x, this_p5_point.y)
				this.p5_handle.line(this_p5_point.x, this_p5_point.y,
						    canvas_center.x, canvas_center.y
						    )
			}   
		}
		this.p5_handle.pop()
	}

	drop_pebble(point_on_manifold: math.Matrix) : void
	{
		this.trajectory.push(point_on_manifold)
	}

	reset() : void
	{
		this.trajectory = []
	}

	to_point(vec: math.Matrix, scale: number): P5.Vector {
		var scale_vec = math.multiply(vec, scale)
		return this.p5_handle.createVector(
			math.subset(scale_vec, math.index(0,0)),
			math.subset(scale_vec, math.index(1,0)));
    	}
}
