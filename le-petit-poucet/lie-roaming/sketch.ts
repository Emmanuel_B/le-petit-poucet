// run with: npx parcel <path to index.html>
//
// On ne s'afflige point d'avoir beaucoup d'enfants,
// Quand ils sont tous beaux, bien faits et bien grands,
// Et d'un extérieur qui brille ;
// Mais si l'un d'eux est faible ou ne dit mot,
// On le méprise, on le raille, on le pille ;
// Quelquefois cependant c'est ce petit marmot
// Qui fera le bonheur de toute la famille. 
//
// Le Petit Poucet, Charles Perrault
 
import P5 from 'p5'
import { create, all } from 'mathjs'
import {UniformSpaceRoamer, Euclidean, SO3, SL2, Minkowski, lie_bracket} from './geom' 
import {to_point, Chronos, Movement, palette, Arrow, DebugText, KeyboardHints, 
        Trajectory, FpsMeter, MoppetAnimation
       } from './graphics'


const math = create(all)


// load graphic assets
import keyboard_hint_url from "url:./assets/keyboard-hint.png";
import spider_url_1 from "url:./assets/spider/spider_gif-01.png";
import spider_url_2 from "url:./assets/spider/spider_gif-02.png";
import spider_url_3 from "url:./assets/spider/spider_gif-03.png";
import spider_url_4 from "url:./assets/spider/spider_gif-04.png";


const ANIMATION_SPEED_PER_ANIM_TICK = 1/4; 
const SPIDER_SIZE_PIXELS = 200
const DEBUG = false;

export class Background
{
    p5
    unit_in_pixels: number

    constructor(p5, unit_in_pixels)
    {
        this.p5=p5
        this.unit_in_pixels = unit_in_pixels
    }

    draw(debug: boolean) : void
    {
		this.p5.background(palette["pupil black"])
		// center and put Y axis going up
		this.p5.translate(this.p5.width/2, this.p5.height/2)
		this.p5.scale(1, -1)
		
			
		if(debug) {
            this.draw_unit_square()
		}
    }

    draw_unit_square() : void
    {
        this.p5.push()
        this.p5.fill(20)
        this.p5.rect(-this.unit_in_pixels/2, -this.unit_in_pixels/2, 
                     this.unit_in_pixels, this.unit_in_pixels
                    )
        this.p5.pop()
    }
}


export var draw = (p5) => {

	let keyboard_hint = new KeyboardHints(p5, keyboard_hint_url)
	let debug_text: DebugText;
	let fps_meter = new FpsMeter(p5)
	let spider = new MoppetAnimation(
		p5, 
		[spider_url_1, spider_url_2, spider_url_3, spider_url_4],
		SPIDER_SIZE_PIXELS
	)
	let trajectory = new Trajectory(p5);
    let arrow_maker: Arrow;
    let animator = new Chronos(p5);
    let background: Background;
    let unit_in_pixels: number;

	let current_point: math.Matrix; // current point
	let move_basis; // current Lie algebra basis
	let space_viewer : UniformSpaceRoamer;
	p5.toggle_menu_btn = null;

	p5.reset = () => {
		trajectory.reset();
		trajectory.drop_pebble(current_point);
	}

	p5.preload = () => {
		keyboard_hint.preload()
		spider.preload()
	}

    /*==--==--==--==--*==--==--==--==--==--==--==--==--==--==--==--==--*/
    p5.setup = (geom_name=null) => {
		var width = document.getElementById("sketch").offsetWidth;
		var height = document.getElementById("sketch").offsetHeight;

		// space selection buttons callbacks
		const radio_ids = ["radio-spherical", "radio-flat", "radio-hyperbolic"]
		radio_ids.forEach(function(element) { 
			var this_radio = document.getElementById(element);
			this_radio.onclick = function() { 
				var this_radio = document.getElementById(element); 
				p5.setup(this_radio.value);
				this_radio.blur();
				return true;
				};
			})
		// resets the trajectory
		document.getElementById("reset").onclick = function () { 
			p5.reset()
		};

		document.getElementById("toggle").onclick = toggle_side_panel;
			
		p5.toggle_menu_btn = p5.createButton('Toggle menu');
		p5.toggle_menu_btn.position(19, 19);
		p5.toggle_menu_btn.mousePressed(toggle_side_panel);
		p5.toggle_menu_btn.elt.style.display = "none";
		p5.toggle_menu_btn.elt.className = "basic-btn";

		var canvas = p5.createCanvas(width, p5.windowHeight);
		canvas.parent('sketch');
		unit_in_pixels = width/3;
			
        background = new Background(p5, unit_in_pixels)
		debug_text = new DebugText(p5, unit_in_pixels);
        arrow_maker = new Arrow(p5, unit_in_pixels);

		let Geometry: UniformSpaceRoamer = SO3 	
		if(!geom_name)
			geom_name = document.querySelector('input[name="geometry"]:checked').value;
		if(geom_name == "flat")
			Geometry = Euclidean
		if(geom_name == "hyperbolic")
			Geometry = SL2
		if(geom_name == "spherical")
			Geometry = SO3
		if(geom_name == "minkowski")
			Geometry = Minkowski
		
		space_viewer = new Geometry();
		current_point = space_viewer.start_pebble()
		trajectory.reset()
		trajectory.drop_pebble(current_point)
		move_basis = {"Dx": Geometry.Dx,
					  "Dy": Geometry.Dy,
					  "L": Geometry.L
					  }
					
	}


    /*==--==--==--==--*==--==--==--==--==--==--==--==--==--==--==--==--*/
    p5.draw =	() => {

		let coord_chart: (point: math.Matrix) => math.Matrix;
		coord_chart = space_viewer.get_view_4_pebble(current_point, move_basis)

        background.draw(DEBUG)
		
		fps_meter.update()

        debug_text.draw([
                "FPS: ".concat(fps_meter.toString()),
                "Number of points: ".concat(trajectory.trajectory.length.toString()),
                DebugText.SEPARATOR,
                ].concat(space_viewer.debug(current_point, move_basis)),
                DEBUG
        )

		keyboard_hint.draw()
		
		trajectory.draw(coord_chart, unit_in_pixels)

		// project this point to window coordinates
		var p_canvas = to_point(p5, coord_chart(current_point), unit_in_pixels)
		
		// Why is this useful?
		if(!p_canvas) {
            console.log("Projecting to local chart failed")
            return
        }

        p5.push()
        p5.strokeWeight(0.025*unit_in_pixels)
        p5.stroke(200)
        p5.point(p_canvas.x, p_canvas.y)
        p5.pop()

        if(DEBUG){
            // draw the elementary displacement vectors
            var dX: math.Matrix = space_viewer.get_tangent(current_point, move_basis.Dx)
            arrow_maker.draw(coord_chart, current_point, dX, 'black')

            var dY = space_viewer.get_tangent(current_point, move_basis.Dy)
            arrow_maker.draw(coord_chart, current_point, dY, 'red')
        }

        spider.draw(animator.animation_pulse)

        let move = animator.animate()

		// summing the movements:
		var X = math.chain(math.multiply(move.dx, move_basis.Dx)).
			add(math.multiply(move.dy, move_basis.Dy)).
			add(math.multiply(move.dtheta, move_basis.L)).
			done()
		
		if (animator.should_update_trajectory()) {
			trajectory.drop_pebble(current_point)
            animator.updated_trajectory()
		}
		
		[current_point, move_basis] = space_viewer.update(current_point, move_basis, X)
	}    
}


let my_sketch = new P5(draw);


// toggles the side panel
function toggle_side_panel() {
  var menu_div = document.getElementById("menu");
  var sketch_div = document.getElementById("sketch");
  // if the menu is not here => we show it
  if (menu_div.style.display === "none") {
    menu_div.style.display = "block";
    menu_div.style['grid-column'] = "1";
    sketch_div.style['grid-column'] = "2 / span 3";
    my_sketch.toggle_menu_btn.elt.style.display = "none";
  } else {
    menu_div.style.display = "none";
    sketch_div.style['grid-column'] = "1 / span 4";
    my_sketch.toggle_menu_btn.elt.style.display = "block";
  }
}; 

