/* geom.js
This file defines all the geometries used in the application.
Every geometry is defined by subclassing the UniformSpaceRoamer interface.

We call a point where the user can move a pebble, 
and the space of all the positions "pebble space".
The "pebble space" is a submanifold of R^3.

We call a point in the lie algebra a lie vector.

*/

import { create, all } from 'mathjs'

const math = create(all)

type Pebble = math.Matrix
type LieVector = math.Matrix

/* TODO
* - View debug function for Minkowski spacetime
* - Implement the functions for Minkowski spacetime
* - Implement Galilean 1+1
*/

/**
* Interface for geometries.
*/
export class UniformSpaceRoamer {
	
	/**
	* A random pebble of the manifold to kick start the thing.
	* @returns sample pebble of the manifold.
	*/
	start_pebble(): math.Matrix { return math.matrix([0, 0, 0])}
	
	/**
	* Moves a pebble on the manifold along a movement.
    * This is the action of the lie algebra on the pebble manifold.
    *
	* @params {lie vector} movement A lie algebra vector
	* @params {pebble} pebble Pebble to move
	* @returns {pebble} Updated `pebble` after being displaced by `movement`.
	*/
	move_pebble(movement: math.Matrix, pebble: math.Matrix): math.Matrix {
        // dummy code so that the type checker shuts up
        return movement
    }

	/**
	* Moves a lie vector along another lie vector.
    * Note that this is just the lie bracket.
    *
	* @params {lie vector} movement A lie algebra vector
	* @params {lie vector} lie_vector Pebble to move
	* @returns {lie vector} Updated `lie_vector` after being displaced by `movement`.
	*/
	move_lie(movement: math.Matrix, lie_vector) {
		// this default implementation will only work if the lie vectors are matrices
		return lie_bracket(movement, lie_vector)	
	}

	/**
	* Updates the whole action according to a user's movement.
    *
    * @params {pebble} center Position pebble.
	* @params {lie basis} lie_basis Original movement basis
	* @params {lie vector} movement A lie algebra vector
	* @returns {[center, lie basis]} Updated `lie_basis` and `center`.
	*/
	update(center, lie_basis, movement: math.Matrix) {	
	}		

	/**
	* Get tangent at a given point and direction.
    * Note that this is given by the derivative of the Ad action at that point.
    *
	* @params {pebble} center Pebble at which we compute the tangent
	* @params {lie vector} lie_vector Direction
	* @returns {vector 3} A vector in R3.
	*/
	get_tangent(center: math.Matrix, lie_vector: math.Matrix): math.Matrix {
		// Default implementation that uses a numerical computation
		const epsilon = 0.01
		var dX: math.Matrix = this.move_pebble(math.multiply(lie_vector, epsilon), center)
		dX = math.subtract(dX, center)
		return math.multiply(dX, 1/epsilon)
	}

	/**
	* Creates view function to project pebbles to 2D.
    *
	* @params {pebble} center Pebble which is at the center of the view.
	* @params {lie basis} lie_basis Current lie basis representing orientation.
	* @returns {function pebble -> normalized coords} A function that maps 
    *   	pebbles to normalized coords.
	*/
	get_view_4_pebble(center, lie_basis) : (point: math.Matrix) => math.Matrix
	{
		// dummy function just to shut up the type checker
		return function(point) { return point }
	}

	//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//
	// debugging
	
	/**
	* Compute killing form for 2 lie vectors.
    *
	* @params {lie vector} lie_vector_X First lie vector.
	* @params {lie vector} lie_vector_Y Second lie vector.
	* @returns {float} A scalar.
	*/
	killing(lie_vector_X, lie_vector_Y) {}

	/**
	* Computes debugging information.
    *
	* @params {vector} center_pebble Center point in pebble space.
	* @params {matrix dict} move_basis Lie algebra basis
	* @returns {str list} List of debug info to print.
	*/
	debug(center_pebble, move_basis) {
		return ["Subclass `debug` to print out information."]
	}
}


/** For this class the action data is given by the Lie basis
 * More precisely `action_data` is a dict with 3 keys Dx, Dy and L.
 * This class serves as a basis for all actions whose action data is given by the Lie basis.
 */
export class Euclidean extends UniformSpaceRoamer {
	move_basis = {
		'Dx': Euclidean.Dx,
		'Dy': Euclidean.Dy,
		'L': Euclidean.L
	}
    center;

	/**
	* Moves a pebble on the manifold along a movement.
    * This is the action of the lie algebra on the pebble manifold.
    *
	* @params {lie vector} movement A lie algebra vector
	* @params {pebble} pebble Pebble to move
	* @returns {pebble} Updated `pebble` after being displaced by `movement`.
	*/
	move_pebble(movement, pebble) {
		let pebble2 = this.lie_act_on_points(movement, pebble);
		return this.reproject_point(pebble2);
	}

	/**
	* Creates view function to project pebbles to 2D.
    *
	* @params {pebble} center Pebble which is at the center of the view.
	* @params {lie basis} lie_basis Current lie basis representing orientation.
	* @returns {function pebble -> normalized coords} A function that maps 
    *   	pebbles to normalized coords.
	*/
	get_view_4_pebble(center, lie_basis) {
		var dX = this.get_tangent(center, lie_basis.Dx)
		var dY = this.get_tangent(center, lie_basis.Dy)
		var current_view = this.view_from_tangents(dX, dY)
		var myself = this
		return function (point) {
			return myself.view(
				center,
				current_view,
				point)
		}
	}
	
	/**
	* Updates the whole action according to a user's movement.
    *
    * @params {pebble} center Position pebble.
	* @params {lie basis} move_basis Original movement basis
	* @params {lie vector} movement A lie algebra vector
	* @returns {[center, lie basis]} Updated `lie_basis` and `center`.
	*/
	update(center, move_basis, movement) {	
		// Moving the movement directions (lie basis)
		for (var key in move_basis) {
				// check if the property/key is defined in the object itself, not in parent
				if (move_basis.hasOwnProperty(key)) {           
				move_basis[key] = math.add(move_basis[key], lie_bracket(movement, move_basis[key]))
				}
		}
		// reproject the lie basis so that it stays in the right orbit
		move_basis = this.reproject_lie(move_basis)

		// Updating the center
		let current_point = math.matrix(this.lie_to_center(move_basis.L))
		current_point = math.reshape(current_point, [3, 1])
		return [current_point, move_basis];
	}		

    /**
    * Killing form.
    *
    * 
    * @params {math.matrix} X 3x3 matrix in the lie algebra
    * @params {math.matrix} Y 3x3 matrix in the lie algebra  
    *  
    * @return A scalar.
    */
    killing(X, Y) {
  		return math.trace(math.multiply(X, Y)); 
	}

    /**
    * Print some debug information.
    * @params {math.matrix dictionary} A basis of the lie algebra. 
    * @return {string list} Debug information.
    */
	debug(center, move_basis) {
		var lie_info = [];
		// computing the killing matrix invariant
		let move_basis_arr = [move_basis.Dx, move_basis.Dy, move_basis.L];
		var B = math.identity(3);
		for(let i = 0; i < 3; i ++)
			for(let j = 0;j < 3; j++)
				B = math.subset(
					B, 
					math.index(i, j), 
					this.killing(move_basis_arr[i], move_basis_arr[j])
				);
		lie_info.push("Killing form matrix: ".concat(math.format(B, 3)))
		// computing the Dx, Dy additional invariants.
		let Dx_inv_2 = math.trace(math.multiply(
			move_basis.Dx,
			math.transpose(move_basis.Dx))
			)
		lie_info.push("Dx 2nd invariant: ".concat(math.format(Dx_inv_2, 3)));
		let Dy_inv_2 = math.trace(math.multiply(
			move_basis.Dy,
			math.transpose(move_basis.Dy))
			)
		lie_info.push("Dy 2nd invariant: ".concat(math.format(Dy_inv_2, 3)));
		// compute structure coefficients
		// this is the change of basis matrix that goes new 2 old
	    let new_to_old = math.transpose(math.matrix([
			this.lie_to_std_basis(move_basis.Dx),
			this.lie_to_std_basis(move_basis.Dy),
			this.lie_to_std_basis(move_basis.L) ])	)
		let old_2_new = math.inv(new_to_old);
		// prepare the commutators to print
        let coms = [lie_bracket(move_basis.Dx, move_basis.Dy),
			lie_bracket(move_basis.L, move_basis.Dx),
			lie_bracket(move_basis.L, move_basis.Dy)]
		let bra_names = ["[Dx, Dy]", "[L, Dx]", "[L, Dy]"]
		for(let i = 0; i < 3;i++){
			let vec = math.multiply(old_2_new, this.lie_to_std_basis(coms[i]));
			lie_info.push(
				`${bra_names[i]} = ${math.format(vec.get([0]),3)}Dx +\
				${math.format(vec.get([1]), 3)}Dy +\
				${math.format(vec.get([2]), 3)}L`)
		}

		// Centering error computation (acting by L should leave center point fixed)
		let act_L_on_center = this.move_pebble(math.multiply(move_basis.L, 0.1), center)
		let act_L_error = math.subtract(act_L_on_center, center)
		act_L_error = math.multiply(math.transpose(act_L_error), act_L_error)
		lie_info.push("L centering error:".concat(math.format(act_L_error, 3)))

		var dX = this.get_tangent(center, move_basis.Dx)
		var dY = this.get_tangent(center, move_basis.Dy)
		lie_info.push('Tangent Dx:'.concat(math.format(dX, 3)))
		lie_info.push('Tangent Dy:'.concat(math.format(dY, 3)))

		return lie_info;
	}

	move(lie_vector) {
		this.move_basis = this.move_lie_basis(this.move_basis, lie_vector)
		// update center
		this.center = this.lie_to_center(this.move_basis['L'])	
	}
    
    identity() {
        return math.matrix([[1, 0, 0], [0, 1, 0], [0, 0, 1]]);
    }
    
    /**
    * Return the initial point, which should be inside the initial coordinate chart.
    *
    * @return {math.matrix} The origin vector of the plane (0,0,1)
    */
    start_pebble() {
        return math.matrix([[0], [0], [1]]);
    }
    
    /**
    * Lie algebra action on the manifold.
    *
    * Here is the action of the infinitesimal Euclidean isometries on the plane.
    * 
    * @params {math.matrix} X 3x3 matrix in the lie algebra
    * @params {math.matrix} point A point on the manifold. Here this is just a point of the plane z=1.
    *
    * @return {math.matrix} A point on the manifold. Here this is just a point of the plane.
    */
    lie_act_on_points(X, point) {
		// embbed the point in the usual representation of the lie algebra
		let point_lie = math.chain(math.multiply(point.get([0, 0]), Euclidean.Dx)).
			add(math.multiply(point.get([1, 0]), Euclidean.Dy)).
			add(math.multiply(point.get([2, 0]), Euclidean.L)).done()
        //return math.multiply(math.add(this.identity(), X), point);
		// use the bracket as the action
		point_lie = math.add(point_lie, lie_bracket(X, point_lie));
		return math.reshape(math.matrix(this.lie_to_std_basis(point_lie)), [3, 1]);
    }
    
    /**
    * 
    * Initial coordinate chart.
    *
    * @params {math.matrix} center A point in the manifold that will be the center of the view.
    * @params {math.matrix} Transformation matrix.
    * @params {math.matrix} point A point on the manifold. Here this is just a point of the plane.
    *
    * @return {math.matrix} (If the point is in the initial coordinate chart) 2D coordinates in the chart.
    */
    view(center, trans, point) {
        point = math.subtract(point, center)
        point = math.multiply(trans, point)
        return math.subset(point, math.index([0, 1], 0)); // project back to 2D;
    }
    
    /**
    * @params {math.matrix} point A point on the manifold. Here this is just a point of the plane.
    */
    distance_from_manifold(point) {
        return math.pow(math.subset(point, math.index(2, 0))-1, 2)
    }
    
    /**
    *   How far are we from the view manifold?
    *
    *   @params {math.matrix} mat A 3x3 matrix
    *
    *   @return {float} The distance to the group, 0 means the matrix lies on the group.
    */
    view_error(mat) {
        // we extract a submatrix which should be a rot. matrix
        var R = math.subset(mat, math.index([0,1], [0,1]))
        var error_M = math.subtract(
            math.multiply(R, math.transpose(R)),
            math.matrix([[1, 0], [0, 1]]))
        // return Frobenius norm of error matrix
        return math.trace(math.multiply(error_M, math.transpose(error_M)))
    }
    
    /** 
    * Projects back to the manifold.
    *
    * @params {math.matrix} point A point in R^3 maybe not on the plane sub-manifold.
    *
    * @return {math.matrix} A point on the manifold.
    */
    reproject_point(point) {
        math.subset(point, math.index(2, 0), 1)
        return point
    }
    
    /** Reproject matrix to manifold
    */
    reproject_view(mat, lr=0.1) {
        // we extract a submatrix which should be a rot. matrix
        var R = math.subset(mat, math.index([0,1], [0,1]))
        var grad = math.subtract(
            math.multiply(R, math.transpose(R)),
            math.matrix([[1, 0], [0, 1]]))
        grad = math.multiply(grad,  R)
        R = math.subtract(R, math.multiply(lr, grad))
        var I3 = math.matrix([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
        var updated = math.subset(I3, math.index([0, 1], [0, 1]), R)
        return updated
    }
    
    view_from_tangents(dx, dy) {
        var dx_n = math.divide(dx, math.sqrt(math.sum(math.dotPow(dx, 2))))
        var dy_n = math.divide(dy, math.sqrt(math.sum(math.dotPow(dy, 2))))
        var dz = math.cross(dx_n, dy_n)
        
//         dx = math.subset(dx, math.index([0, 1, 2], 0))
//         dy = math.subset(dx, math.index([0, 1, 2], 0))
//         dz = math.subset(dx, math.index([0, 1, 2], 0))
        return math.concat(math.transpose(dx_n), math.transpose(dy_n), dz, 0)
    }

	/**
	* Maps a lie vector to R3.
	*/
    lie_to_std_basis(X) {
		return [X.get([0, 2]), X.get([1, 2]), X.get([1, 0])]
	}

	/**
	* Reprojects a basis of the lie algebra.
    * When moving around using the lie bracket, the lie algebra basis can get
	* distorted. This is because the lie bracket is only a first order
	* approximation of the group action.
	* Consequently it is necessary to correct the Lie algebra basis so that
	* it conserves the same properties throughout the roaming experience.
	* The conjugacy class of each lie vector should be kept constant,
	* as well as the killing form matrix, and the structure coefficients.
	*
	* @params (matrix dict) Dictionary mapping names to Lie vectors (matrices).
	* @returns (matrix dict) Same structure as input but corrected.  
	*/
	reproject_lie(move_basis){
		let norm = 1;
		// 1st step: renormalize Dx
		let norm_Dx = math.sqrt(math.trace(math.multiply(
			move_basis.Dx,
			math.transpose(move_basis.Dx))))
		move_basis.Dx = math.multiply(norm,
			math.divide(move_basis.Dx, norm_Dx))
		// 3rd step: find Dy
		// we use [L, Dx] = Dy
		 move_basis.Dy = lie_bracket(move_basis.L, move_basis.Dx)
		return move_basis;
	}
 
	/**
	* Returns the fixed point of the corresponding Lie transformation.
	*/
    lie_to_center(L) {
		return this.lie_to_std_basis(L);
	}
}

Euclidean.L = math.matrix([[0, -1, 0], [1, 0, 0], [0, 0, 0]])
Euclidean.Dx = math.matrix([[0, 0, 1], [0, 0, 0], [0, 0, 0]])
Euclidean.Dy = math.matrix([[0, 0, 0], [0, 0, 1], [0, 0, 0]])


/**
* Roaming in spherical geometry using the lie action of SO(3).
*
*/
export class SO3 extends Euclidean {

	/**
	* Moves a pebble on the manifold along a movement.
    * This is the action of the lie algebra on the pebble manifold.
    *
	* @params {lie vector} movement A lie algebra vector
	* @params {pebble} pebble Pebble to move
	* @returns {pebble} Updated `pebble` after being displaced by `movement`.
	*/
	move_pebble(movement, pebble) {
		let pebble2 = this.lie_act_on_points(movement, pebble);
		return this.reproject_point(pebble2);
	}


    killing(X, Y) {
		return math.multiply(
			-1, 
			math.trace(
				math.multiply(
					X, 
					math.transpose(Y)
			)))
	}

	debug(center, move_basis) {
		var lie_info = [];
		let move_basis_arr = [move_basis.Dx, move_basis.Dy, move_basis.L];
		// computing killing form matrix
		var B = math.identity(3);
		for(let i = 0; i < 3; i ++)
			for(let j = 0;j < 3; j++)
				B = math.subset(
					B, 
					math.index(i, j), 
					this.killing(move_basis_arr[i], move_basis_arr[j])
				);
		lie_info.push("Killing form matrix: ".concat(math.format(B, 3)))
		// compute the structure coefficients
		let bra_names = ["[Dx, Dy]", "<L, Dx>", "<L, Dy>"]
		let bra_kets = [lie_bracket(move_basis.Dx, move_basis.Dy),
				lie_bracket(move_basis.L, move_basis.Dx),
				lie_bracket(move_basis.L, move_basis.Dy)]
		for(let i = 0; i < 3;i++){
			let vec = math.matrix([this.killing(bra_kets[i], move_basis.Dx),
				this.killing(bra_kets[i], move_basis.Dy),
				this.killing(bra_kets[i], move_basis.L)])
			vec = math.lusolve(B, vec)
			lie_info.push(
				`${bra_names[i]} = ${math.format(vec.get([0, 0]),3)}Dx +\
				${math.format(vec.get([1, 0]), 3)}Dy +\
				${math.format(vec.get([2, 0]), 3)}L`)
		}
		
		// Centering error computation (acting by L should leave center point fixed)
		let act_L_on_center = this.move_pebble(math.multiply(move_basis.L, 0.1), center)
		let act_L_error = math.subtract(act_L_on_center, center)
		act_L_error = math.multiply(math.transpose(act_L_error), act_L_error)
		lie_info.push("L centering error:".concat(math.format(act_L_error, 3)))

		return lie_info;
	}

    initial_basis() {
        return [];
    }
    
    /**
    * Return the initial point, which should be inside the initial coordinate chart.
    *
    * @return {math.matrix} This point of the 2-sphere (0,0,1)
    */
    start_pebble() {
        return math.matrix([[0], [0], [1]]);
    }
    
    
    /**
    * 
    * Coordinate chart function.
    *
    * @params {math.matrix} center A point in the manifold that will be the center of the view.
    * @params {math.matrix} Transformation matrix.
    * @params {math.matrix} point A point on the manifold. Here this is just a point of the plane.
    *
    * @return {math.matrix} (If the point is in the initial coordinate chart) 2D coordinates in the chart.
    */
    view(center, trans, point) {
        var clip = 1.0;  // if z value is bigger than this we clip the point
        point = math.subtract(point, center)
        point = math.multiply(trans, point)
        // clipping
        var z = math.subset(point, math.index(2, 0))
        if(math.abs(z) > clip) {
            return null
        }
        return math.subset(point, math.index([0, 1], 0)); // project back to 2D;
    }
    
    /**
    * @params {math.matrix} point A point on the manifold. Here this is just a point of the plane.
    */
    distance_from_manifold(point) {
        return math.sum(math.dotPow(point, 2))-1
    }
    
    /**
    *   How far are we from the view manifold?
    *
    *   @params {math.matrix} mat A 3x3 matrix
    *
    *   @return {float} The distance to the group, 0 means the matrix lies on the group.
    */
    view_error(mat) {
        var error_M = math.subtract(
            math.multiply(mat, math.transpose(mat)),
            this.identity())
        // return Frobenius norm of error matrix
        return math.trace(math.multiply(error_M, math.transpose(error_M)))
    }
    
    /**
    */
    reproject_point(point) {
        return math.divide(point,  math.sum(math.dotPow(point, 2)))
    }
    
    /** Reproject matrix to manifold
    */
    reproject_view(mat, lr=0.1) {
        var grad = math.subtract(
            math.multiply(mat, math.transpose(mat)),
            this.identity())
        grad = math.multiply(grad,  mat)
        return math.subtract(mat, math.multiply(lr, grad))
    }

	
	/**
	* Reproject lie algebra to manifold.
	*/
	reproject_lie(move_basis){
		let norm = math.sqrt(2);
		// 1st step renormalize L
		let norm_L = math.sqrt(-this.killing(move_basis.L, move_basis.L));
		move_basis.L = math.multiply(norm,
			math.divide(move_basis.L, norm_L))
		// 2nd step: orthogonalize and renormalize Dx
		let W = math.multiply(
			move_basis.L,
			// minus because <L, L> = - norm
			-this.killing(move_basis.L, move_basis.Dx)/norm/norm);
		 move_basis.Dx = math.subtract(move_basis.Dx, W);
		let norm_Dx = math.sqrt(-this.killing(move_basis.Dx, move_basis.Dx));
		move_basis.Dx = math.multiply(norm,
			math.divide(move_basis.Dx, norm_Dx))
		// 3rd step: find Dy
		// we use [L, Dx] = Dy
		move_basis.Dy = lie_bracket(move_basis.L, move_basis.Dx)
		return move_basis;
	}

	/**
	* Maps a lie vector to R3.
	*/
    lie_to_std_basis(X) {
		return [X.get([0, 2]), X.get([1, 2]), X.get([1, 0])]
	}

	lie_to_center(L) {
		return this.lie_to_std_basis(L);
	}
    

	/**
    * Lie algebra action on the manifold.
    *
    * Here is the action of SO3 on so3.
    * 
    * @params {math.matrix} X 2x2 matrix in the lie algebra
    * @params {math.matrix} point A point of R3 assumed to represent 
	* a lie algebra vector in the initial basis.
    *
    * @return {math.matrix} A point on the manifold.
    */
    lie_act_on_points(X, point) {
		let point_lie = math.chain(math.multiply(point.get([0, 0]), SO3.Dx)).
			add(math.multiply(point.get([1, 0]), SO3.Dy)).
			add(math.multiply(point.get([2, 0]), SO3.L)).done()
		// use the bracket as the action
		point_lie = math.add(point_lie, lie_bracket(X, point_lie));
		return math.reshape(math.matrix(this.lie_to_std_basis(point_lie)), [3, 1]);
    }
}

SO3.L = math.matrix([[0, -1, 0], [1, 0, 0], [0, 0, 0]])
SO3.Dx = math.matrix([[0, 0, 1], [0, 0, 0], [-1, 0, 0]])
SO3.Dy = math.matrix([[0, 0, 0], [0, 0, 1], [0, -1, 0]])


/**
* Roaming in hyperbolic geometry using the lie action of SL2.
*
*/
export class SL2 extends Euclidean {
    initial_basis() {
        return [];
    }
    
    /**
    * Return the initial point, which should be inside the initial coordinate chart.
    *
    * @return {math.matrix} This point of the hyperboloid (0,0,1)
    */
    start_pebble() {
        return math.matrix([[0], [0], [1]]);
    }
    
    /** Returns the identity of SL2. */
    identity() {
        return math.matrix([[1, 0], [0, 1]])
    }
    
    /**
    * 
    * Coordinate chart function.
    *
    * @params {math.matrix} center A point in the manifold that will be the center of the view.
    * @params {math.matrix} Transformation matrix.
    * @params {math.matrix} point A point on the manifold. Here this is just a point of the plane.
    *
    * @return {math.matrix} (If the point is in the initial coordinate chart) 2D coordinates in the chart.
    */
    view(center, trans, point) {
        var clip = 0.5;  // if z value is bigger than this we clip the point
        point = math.subtract(point, center)
        point = math.multiply(trans, point)
        // clipping
        //         var z = math.subset(point, math.index(2, 0))
        //         if(math.abs(z) > clip) {
        //             return null
        //         }
        return math.subset(point, math.index([0, 1], 0)); // project back to 2D;
    }
    
    /**  How far is the point from the hyperbolic plane.
    * 
    * @params {math.matrix} point A point on the manifold. Here this is just a point of the plane.
    */
    distance_from_manifold(point) {
        return math.abs(this.invariant_form(point)-(-1))
    }
    
    /** Minkowski metric
    */
    invariant_form(X, Y=null) {
        if(Y == null)
            Y=math.clone(X)
        let new_X = math.reshape(math.clone(X), [3, 1])
        let new_Y = math.reshape(math.clone(Y), [3, 1])
        var B = math.matrix([
            [1, 0,  0],
            [0, 1,  0],
            [0, 0, -1]
        ])
        var XY = math.multiply(
            math.transpose(new_X), 
            math.multiply(B, new_Y))
        return math.subset(XY, math.index(0, 0))
    }
    
    /** Hyperbolic cross product.
    */
    h_cross(X, Y) {
        var W = math.cross(X, Y);
        return math.multiply(W, SL2.H);
    }
    
    /**
    *   How far are we from the view manifold?
    *
    *   @params {math.matrix} mat A 3x3 matrix
    *
    *   @return {float} The distance to the group, 0 means the matrix lies on the group.
    */
    view_error(mat) {
        var error_M = math.chain(math.transpose(mat))
            .multiply(SL2.H)
            .multiply(mat)
            .subtract(SL2.H)
            .done()
        // return Frobenius norm of error matrix
        return math.trace(math.multiply(error_M, math.transpose(error_M)))
    }
    
    /** Reproject the point to the manifold.
    */
    reproject_point(point) {
        return math.divide(point, math.sqrt(math.abs(this.invariant_form(point))))
    }
    
    /**
    * Lie algebra action on the manifold.
    *
    * Here is the action of SL2 on the hyperboloid.
    * 
    * @params {math.matrix} X 2x2 matrix in the lie algebra
    * @params {math.matrix} point A point on the hyperboloid in R3
    *
    * @return {math.matrix} A point on the manifold.
    */
    lie_act_on_points(X, point) {
        //return math.multiply(this.rep_3d(math.add(this.identity(), X)), point);
		// embbed the point in the usual representation of the lie algebra
		let point_lie = math.chain(math.multiply(point.get([0, 0]), SL2.Dx)).
			add(math.multiply(point.get([1, 0]), SL2.Dy)).
			add(math.multiply(point.get([2, 0]), SL2.L)).done()
        //return math.multiply(math.add(this.identity(), X), point);
		// use the bracket as the action
		point_lie = math.add(point_lie, lie_bracket(X, point_lie));
		return math.reshape(math.matrix(this.lie_to_std_basis(point_lie)), [3, 1]);
    }
    
    /**
    * A 3-dimensional representation for SL2.
    *
    * @params {matrix} mat 2x2 matrix in SL2
    *
    * @return {matrix} 3x3 matrix
    */
    rep_3d(mat) {
        // extract the component of the matrix
        var a = math.subset(mat, math.index(0, 0))
        var b = math.subset(mat, math.index(0, 1))
        var c = math.subset(mat, math.index(1, 0))
        var d = math.subset(mat, math.index(1, 1))
        
        return math.matrix([
            [(a*a-b*b-c*c+d*d)/2, c*d-a*b, (-a*a-b*b+c*c+d*d)/2],
            [b*d-a*c, a*d+b*c, a*c+b*d],
            [(-a*a+b*b-c*c+d*d)/2, a*b+c*d, (a*a+b*b+c*c+d*d)/2]
        ])
    }
    
    view_from_tangents(dx, dy) {
        var dx_n = math.divide(dx, math.sqrt(this.invariant_form(dx)))
        var dy_n = math.subtract(dy, 
                                  math.multiply(
            this.invariant_form(dx_n, dy),
            dx_n)
                                  )
        dy_n = math.divide(dy_n, math.sqrt(this.invariant_form(dy_n)))
        var dz = this.h_cross(dx_n, dy_n)
        
       return math.inv(math.transpose(math.concat(math.transpose(dx_n), math.transpose(dy_n), dz, 0)))
    }
    
	killing(X, Y){
		return math.multiply(
			4,
			math.trace(math.multiply(X, Y)))
	}

    /** Print some debug information.
    * @params {math.matrix dictionary} A basis of the lie algebra. 
    * @return {string list} Debug information.
    */
	debug(center, move_basis) {
		var lie_info = [];
		let move_basis_arr = [move_basis.Dx, move_basis.Dy, move_basis.L];
		var B = math.identity(3);
		for(let i = 0; i < 3; i ++)
			for(let j = 0;j < 3; j++)
				B = math.subset(
					B, 
					math.index(i, j), 
					this.killing(move_basis_arr[i], move_basis_arr[j])
				);
		lie_info.push("Killing form matrix: ".concat(math.format(B, 3)))
		// compute the structure coefficients
		let bra_names = ["[Dx, Dy]", "<L, Dx>", "<L, Dy>"]
		let bra_kets = [lie_bracket(move_basis.Dx, move_basis.Dy),
				lie_bracket(move_basis.L, move_basis.Dx),
				lie_bracket(move_basis.L, move_basis.Dy)]
		for(let i = 0; i < 3;i++){
			let vec = math.matrix([this.killing(bra_kets[i], move_basis.Dx),
				this.killing(bra_kets[i], move_basis.Dy),
				this.killing(bra_kets[i], move_basis.L)])
			vec = math.lusolve(B, vec)
			lie_info.push(
				`${bra_names[i]} = ${math.format(vec.get([0, 0]),3)}Dx +\
				${math.format(vec.get([1, 0]), 3)}Dy +\
				${math.format(vec.get([2, 0]), 3)}L`)
		}
		
		// Centering error computation (acting by L should leave center point fixed)
		let act_L_on_center = this.move_pebble(math.multiply(move_basis.L, 0.1), center)
		let act_L_error = math.subtract(act_L_on_center, center)
		act_L_error = math.multiply(math.transpose(act_L_error), act_L_error)
		lie_info.push("L centering error:".concat(math.format(act_L_error, 3)))

		return lie_info;
	}
	
	/**
	* Reproject lie algebra to manifold.
	*/
	reproject_lie(move_basis){
		let norm = math.sqrt(2);
		// 1st step renormalize L
		let norm_L = math.sqrt(-this.killing(move_basis.L, move_basis.L));
		move_basis.L = math.multiply(norm,
			math.divide(move_basis.L, norm_L))
		// 2nd step: orthogonalize and renormalize Dx
		let W = math.multiply(
			move_basis.L,
			// minus because <L, L> = - norm
			-this.killing(move_basis.L, move_basis.Dx)/norm/norm);
		move_basis.Dx = math.subtract(move_basis.Dx, W);
		let norm_Dx = math.sqrt(this.killing(move_basis.Dx, move_basis.Dx));
		move_basis.Dx  = math.multiply(norm,
			math.divide(move_basis.Dx, norm_Dx))
		// 3rd step: find Dy
		// we use [L, Dx] = Dy
		move_basis.Dy = lie_bracket(move_basis.L, move_basis.Dx)
		return move_basis;
	}
	
	/**
	* Maps a lie vector to R3.
	*/
    lie_to_std_basis(X) {
		return [2*X.get([0, 0]), 
			X.get([0, 1])+X.get([1, 0]),
			X.get([1, 0])-X.get([0, 1])]
	}

	lie_to_center(L) {
		return this.lie_to_std_basis(L);
	}
}

// Invariant bilinear form for the chosen embedding of SL2
SL2.H = math.matrix([
            [1, 0,  0],
            [0, 1,  0],
            [0, 0, -1]
        ])

SL2.L = math.matrix([[0, -1/2], [1/2, 0]])
SL2.Dx = math.matrix([[1/2, 0], [0, -1/2]])
SL2.Dy = math.matrix([[0, 1/2], [1/2, 0]])


/** Special relativity 1+1 spacetime
* Pebble manifold = all points of the form (x,y,1)
*/
export class Minkowski extends Euclidean {

	/**
	* Reprojects a basis of the lie algebra.
    * When moving around using the lie bracket, the lie algebra basis can get
	* distorted. This is because the lie bracket is only a first order
	* approximation of the group action.
	* Consequently it is necessary to correct the Lie algebra basis so that
	* it conserves the same properties throughout the roaming experience.
	* The conjugacy class of each lie vector should be kept constant,
	* as well as the killing form matrix, and the structure coefficients.
	*
	* @params (matrix dict) Dictionary mapping names to Lie vectors (matrices).
	* @returns (matrix dict) Same structure as input but corrected.  
	*/
	reproject_lie(move_basis) {
		// Reproject L first
		math.subset(move_basis.L, math.index(0,1), -1)
		math.subset(move_basis.L, math.index(1,0), -1)
		// Normalize Dx
		math.subset(move_basis.Dx, math.index(0,1), 0)
		math.subset(move_basis.Dx, math.index(1,0), 0)

		var x = move_basis.Dx.get([0,2])
		var t = move_basis.Dx.get([1,2])
		var norm_dx = math.sqrt(x*x - t*t)
		move_basis.Dx = math.divide(move_basis.Dx, norm_dx)
		// Get back H or Dy
		move_basis.Dy = lie_bracket(move_basis.Dx, move_basis.L)
		return move_basis;
	}


	/** 
    * Projects back to the manifold.
    *
    * @params {math.matrix} point A point in R^3 maybe not on the plane sub-manifold.
    *
    * @return {math.matrix} A point on the manifold.
    */
    reproject_point(point) {
        math.subset(point, math.index(2, 0), 1)
        return point
    }

	/**
	* Maps a lie vector to R3.
	*/
    lie_to_std_basis(X) {
		return [X.get([0, 2]),
			X.get([1, 2]),
			-X.get([0, 1])]
	}

	/**
	* Returns the fixed point of the corresponding Lie transformation.
	*/
	lie_to_center(X){
		return this.lie_to_std_basis(X);
	}

	/**
	* A random point of the manifold to kick start the thing.
	* @returns sample data point of the manifold.
	*/
	start_pebble() { 
        return math.matrix([[0], [0], [1]]);
	}
    

	/**
	* Get tangent at a given point and direction.
    * Note that this is given by the derivative of the Ad action at that point.
    *
	* @params {pebble} center Pebble at which we compute the tangent
	* @params {lie vector} lie_vector Direction
	* @returns {vector 3} A vector in R3.
	*/
	get_tangent(center, lie_vector) {
		// The tangent is given by [lie_vector, center]
		// First we need to put back the center in matrix form
		let point_lie = math.chain(math.multiply(center.get([0, 0]), Minkowski.Dx)).
			add(math.multiply(center.get([1, 0]), Minkowski.Dy)).
			add(math.multiply(center.get([2, 0]), Minkowski.L)).done()
		// then we get the tangent in lie form
		let tangent_lie = lie_bracket(lie_vector, point_lie)
		// convert back to vector and return
        let tangent = math.transpose(math.matrix([this.lie_to_std_basis(tangent_lie)]))
		return tangent
	}

	/**
    * Killing form.
    *
    * 
    * @params {math.matrix} X 3x3 matrix in the lie algebra
    * @params {math.matrix} Y 3x3 matrix in the lie algebra  
    *  
    * @return A scalar.
    */
    killing(X, Y) {
  		return math.trace(math.multiply(X, Y)); 
	}
    
	/**
    * Print some debug information.
    * @params {math.matrix dictionary} A basis of the lie algebra. 
    * @return {string list} Debug information.
    */
	debug(center, move_basis) {
		var lie_info = [];
		// computing the killing matrix invariant
		let move_basis_arr = [move_basis.Dx, move_basis.Dy, move_basis.L];
		var B = math.identity(3);
		for(let i = 0; i < 3; i ++)
			for(let j = 0;j < 3; j++)
				B = math.subset(
					B, 
					math.index(i, j), 
					this.killing(move_basis_arr[i], move_basis_arr[j])
				);
		lie_info.push("Dx matrix: ".concat(math.format(move_basis.Dx, 3)))
		lie_info.push("Killing form matrix: ".concat(math.format(B, 3)))
		// computing the Dx, Dy additional invariants.
		var x = move_basis.Dx.get([0,2])
		var t = move_basis.Dx.get([1,2])
		lie_info.push("Dx 2nd invariant: ".concat(math.format(x*x-t*t, 3)));
		var x = move_basis.Dy.get([0,2])
		var t = move_basis.Dy.get([1,2])
		lie_info.push("Dy 2nd invariant: ".concat(math.format(x*x - t*t, 3)));
		// compute structure coefficients
		// this is the change of basis matrix that goes new 2 old
       	console.log('hi debug: ')
	    let new_to_old = math.transpose(math.matrix([
			this.lie_to_std_basis(move_basis.Dx),
			this.lie_to_std_basis(move_basis.Dy),
			this.lie_to_std_basis(move_basis.L) ])	)
		let old_2_new = math.inv(new_to_old);
		// prepare the commutators to print
        let coms = [lie_bracket(move_basis.Dx, move_basis.Dy),
			lie_bracket(move_basis.L, move_basis.Dx),
			lie_bracket(move_basis.L, move_basis.Dy)]
		let bra_names = ["[Dx, Dy]", "[L, Dx]", "[L, Dy]"]
		for(let i = 0; i < 3;i++){
			let vec = math.multiply(old_2_new, this.lie_to_std_basis(coms[i]));
			lie_info.push(
				`${bra_names[i]} = ${math.format(vec.get([0]),3)}Dx +\
				${math.format(vec.get([1]), 3)}Dy +\
				${math.format(vec.get([2]), 3)}L`)
		}

		// Centering error computation (acting by L should leave center point fixed)
		let act_L_on_center = this.move_pebble(math.multiply(move_basis.L, 0.01), center)
		let act_L_error = math.subtract(act_L_on_center, center)
		act_L_error = math.multiply(math.transpose(act_L_error), act_L_error)
		lie_info.push("L centering error:".concat(math.format(act_L_error, 3)))

		// Let's compute the view matrix
		var dX = this.get_tangent(center, move_basis.Dx)
		var dY = this.get_tangent(center, move_basis.Dy)
		var current_view = this.view_from_tangents(dX, dY)
		lie_info.push('View matrix: '.concat(math.format(current_view, 3)))
		lie_info.push('View error: '.concat(this.view_error(current_view)))
		lie_info.push('Tangent Dx:'.concat(math.format(dX, 3)))
		lie_info.push('Tangent Dy:'.concat(math.format(dY, 3)))

		return lie_info;
	}
    
	view_from_tangents(dx, dy) {
        //var dx_n = math.divide(dx, math.sqrt(math.sum(math.dotPow(dx, 2))))
        //var dy_n = math.divide(dy, math.sqrt(math.sum(math.dotPow(dy, 2))))
        var dz = math.matrix([[0],[0],[1]])
		var dx_n = math.multiply(dx, 2)
		var dy_n = math.multiply(dy, 2)
        //return math.concat(math.transpose(dx_n), math.transpose(dy_n), math.transpose(dz), 0)
		let mat = math.inv(math.concat(dx_n, dy_n, dz))
       	console.log('hi: '.concat(math.format(mat, 3)))
		return mat
    }
    
	/**
    * Lie algebra action on the manifold.
    *
    * Here is the action of the infinitesimal Euclidean isometries on the plane.
    * 
    * @params {math.matrix} X 3x3 matrix in the lie algebra
    * @params {math.matrix} point A point on the manifold. Here this is just a point of the plane z=1.
    *
    * @return {math.matrix} A point on the manifold. Here this is just a point of the plane.
    */
    lie_act_on_points(X, point) {
		// embbed the point in the usual representation of the lie algebra
		let point_lie = math.chain(math.multiply(point.get([0, 0]), Minkowski.Dx)).
			add(math.multiply(point.get([1, 0]), Minkowski.Dy)).
			add(math.multiply(point.get([2, 0]), Minkowski.L)).done()
        //return math.multiply(math.add(this.identity(), X), point);
		// use the bracket as the action
		point_lie = math.add(point_lie, lie_bracket(X, point_lie));
		return math.reshape(math.matrix(this.lie_to_std_basis(point_lie)), [3, 1]);
    }

    /**
    *   How far are we from the view manifold?
    *
    *   @params {math.matrix} mat A 3x3 matrix
    *
    *   @return {float} The distance to the group, 0 means the matrix lies on the group.
    */
    view_error(mat) {
		// 2D metric
		var eta = math.matrix([[1,0], [0,-1]]);
        // we extract a submatrix which should be a hyperbolic rot. matrix
        var R = math.subset(mat, math.index([0,1], [0,1]))
		var R_inv = math.chain(eta).
			multiply(math.transpose(R)).
			multiply(eta).
			done()
        var error_M = math.subtract(
            math.multiply(R, R_inv),
            math.matrix([[1, 0], [0, 1]]))
        // return Frobenius norm of error matrix
        return math.trace(math.multiply(error_M, math.transpose(error_M)))
    }
    
}

Minkowski.Dx = math.matrix([[0,0,1], [0,0,0], [0,0,0]])
Minkowski.Dy= math.matrix([[0,0,0], [0,0,1], [0,0,0]])
Minkowski.L = math.matrix([[0,-1,0], [-1,0,0], [0,0,0]])


/**
* Computes the lie bracket of 2 matrices.
*
* @param {math.matrix} X Element of the lie algebra
* @param {math.matrix} Y Element of the lie algebra
*
* @return {math.matrix} [X, Y]
*/
export function lie_bracket(X, Y) {
    return math.subtract(math.multiply(X, Y), math.multiply(Y, X))

}
