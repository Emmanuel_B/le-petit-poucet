/****####****####****####****####****####****####****####****####****####****####****####
Run the tests with `npm run test`
*/


import {Euclidean, SO3, SL2, lie_bracket } from './geom' 
import { create, all } from 'mathjs'

const math = create(all)

//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**
// Hyperbolic geometry tests
//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**

test('red_3d maps identity to identity', () => {
    let sv = new SL2()
    let comp = math.deepEqual(
        sv.rep_3d(sv.identity()),
        math.identity(3))
    expect(comp).toBe(true);
});

test('red_3d respects the composition law', () => {
    let sv = new SL2()
    let shear1 = math.matrix([[1, 1], [0, 1]])
    let r45 = math.rotationMatrix(math.pi/8)
    let comp = math.deepEqual(
        sv.rep_3d(math.multiply(shear1, r45)),
        math.multiply(sv.rep_3d(shear1), sv.rep_3d(r45)))
    expect(comp).toBe(true);
});


test('red_3d respects the composition law', () => {
    let sv = new SL2()
    let shear1 = math.matrix([[1, 1], [0, 1]])
    let h_acc = math.matrix([[2, 0], [0, 1/2]])
    let comp = math.deepEqual(
        sv.rep_3d(math.multiply(shear1, h_acc)),
        math.multiply(sv.rep_3d(shear1), sv.rep_3d(h_acc)))
    expect(comp).toBe(true);
});

test('red_3d maps to H-invariant matrices', () => {
    let sv = new SL2()
    let h_acc = math.matrix([[2, 0], [0, 1/2]])
    let rep = sv.rep_3d(h_acc)
    let comp = math.deepEqual(
        math.chain(math.transpose(rep)).multiply(SL2.H).multiply(rep).done(),
        SL2.H)
    expect(comp).toBe(true);
});

test('red_3d maps to H-invariant matrices', () => {
    let sv = new SL2()
    let shear1 = math.matrix([[1, 1], [0, 1]])
    let rep = sv.rep_3d(shear1)
    let comp = math.deepEqual(
        math.chain(math.transpose(rep)).multiply(SL2.H).multiply(rep).done(),
        SL2.H)
    expect(comp).toBe(true);
});

test('red_3d maps to H-invariant matrices', () => {
    let sv = new SL2()
    let r45 = math.rotationMatrix(math.pi/8)
    let rep = sv.rep_3d(r45)
    let comp = math.deepEqual(
        math.chain(math.transpose(rep)).multiply(SL2.H).multiply(rep).done(),
        SL2.H)
    expect(comp).toBe(true);
});

test('sl2 killing form on a lie algebra basis', () => {
    let sv = new SL2()
	let comp_L = math.deepEqual(
		sv.killing(SL2.L, SL2.L),
		-2)
	let comp_Dx = math.deepEqual(
		sv.killing(SL2.Dx, SL2.Dx),
		2)	
	let comp_Dy = math.deepEqual(
		sv.killing(SL2.Dy, SL2.Dy),
		2)
	let comp_LDx = math.deepEqual(
		sv.killing(SL2.L, SL2.Dx),
		0)
    expect(comp_L).toBe(true);
    expect(comp_Dx).toBe(true);
    expect(comp_Dy).toBe(true);
    expect(comp_LDx).toBe(true);
});

//ppppddddppppddddppppddddppppddddppppddddppppddddppppddddppppddddppppddddppppddddppppdddd
// Bilinear form tests

test('Bili norm test', () => {
    let sv = new SL2()
    let ez_plus = math.matrix([0, 0, 1])
    let ez_minus = math.matrix([0, 0, -1])
    let comp = math.deepEqual(
        -1,
        sv.invariant_form(ez_plus))
    expect(comp).toBe(true);
    
    comp = math.deepEqual(
        -1,
        sv.invariant_form(ez_minus))
    
    expect(comp).toBe(true);
});

test('Bili dot test', () => {
    let sv = new SL2()
    let v1 = math.matrix([1, 2, 3])
    let v2 = math.matrix([4, 5, 6])
    let comp = math.deepEqual(
        -4,
        sv.invariant_form(v1, v2))
    expect(comp).toBe(true);
});

test('Bili dot test #2', () => {
    let sv = new SL2()
    let v1 = math.matrix([-1.,  4.5, -3.5])
    let v2 = math.matrix([-1, 1, 3])
    
    let comp = math.deepEqual(
        16,
        sv.invariant_form(v1, v2))
    expect(comp).toBe(true);
});



//pqpqpqpqpqpqpqpqpqpqpqpqpqpqpqpqpqpqpqpqpqpqpqpqpqpqpqpqpqpqpqpqpqpqpqpqpqpqpqpqpqpqpqpq
// cross product tests

test('h_cross test', () => {
    let sv = new SL2()
    let e1 = math.matrix([1, 0, 0])
    let e2 = math.matrix([0, 1, 0])
    let w = sv.h_cross(e1, e2)
    let comp = math.deepEqual(
        w,
        math.matrix([0, 0, -1]))
    expect(comp).toBe(true);
});

// The cross product is defined via the following relation
// 3D vector u x v such that det(u, v, w) = H(u x v, w) forall w.

test('h_cross test', () => {
    let sv = new SL2()
    let e1 = math.matrix([1.5, -2, 3])
    let e2 = math.matrix([1, 1, -1])
    let da_cross = sv.h_cross(e1, e2)
    let w = math.matrix([-1, 1, 3]) // a test vector
    
    let comp = math.deepEqual(
        math.det(math.matrix([e1, e2, w])),
        sv.invariant_form(da_cross, w)
    )

    expect(comp).toBe(true);
});



//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**
// Spherical geometry tests
//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**

test('so3 killing form on a lie algebra basis', () => {
    let sv = new SO3()
	let comp_L = math.deepEqual(
		sv.killing(SO3.L, SO3.L),
		-2)
	let comp_Dx = math.deepEqual(
		sv.killing(SO3.Dx, SO3.Dx),
		-2)	
	let comp_Dy = math.deepEqual(
		sv.killing(SO3.Dy, SO3.Dy),
		-2)
	let comp_LDx = math.deepEqual(
		sv.killing(SO3.L, SO3.Dx),
		0)
    expect(comp_L).toBe(true);
    expect(comp_Dx).toBe(true);
    expect(comp_Dy).toBe(true);
    expect(comp_LDx).toBe(true);
});

//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**
// Euclidean geometry tests
//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**

test('Euclidean killing form on a lie algebra basis', () => {
    let sv = new Euclidean()
	let comp_L = math.deepEqual(
		sv.killing(Euclidean.L, Euclidean.L),
		-2)
	let comp_Dx = math.deepEqual(
		sv.killing(Euclidean.Dx, Euclidean.Dx),
		0)	
	let comp_Dy = math.deepEqual(
		sv.killing(Euclidean.Dy, Euclidean.Dy),
		0)
	let comp_LDx = math.deepEqual(
		sv.killing(Euclidean.L, Euclidean.Dx),
		0)
    expect(comp_L).toBe(true);
    expect(comp_Dx).toBe(true);
    expect(comp_Dy).toBe(true);
    expect(comp_LDx).toBe(true);
});

//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**
// Minkowski space tests
//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**//**
