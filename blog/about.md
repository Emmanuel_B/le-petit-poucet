---
layout: article 
title: About
permalink: /about/
key: page-about
---


👁️ This blog delves into the question: "What is spacetime?"
We explore its mathematical and philosophical aspects and connect them to concrete sensations.
Our goal is to describe space and time through what you can see, touch, and hear.

We live in spacetime; it is our home! Let's get to know it.

## Who is this blog for?

Some parts are intuitive and accessible, while others require technical knowledge of group theory and topology.


## About Me

I'm a nomadic data scientist passionate about science and meditation.

Meditation can make one perceive the world as sensations, including space and time, and even the self, a concept known in Buddhism as "no-self." After some profound experiences, I read works by Poincaré and Piaget (see resources), which deepened my perspective. This led me to start this blog.

Feel free to reach out!

## Acknowledgements

Many thanks to Judah [@im_stillshining](https://www.instagram.com/im_stillshining/) for realizing Le petit poucet's spider. 
Thanks to Vivek Claver for reviewing some of the content.

## Licenses

You are free to use and redistribute this work, even commercially, as long as you include a reference.
More precisely:

- Articles are licensed under a creative common's Attribution 4.0 International.
- Code is licensed under MIT license.


## Resources

Interesting content at the intersection of mathematics and perception.

### Mathematics

#### Terrence Tao blog

* [Grothendieck's definition of a group](https://terrytao.wordpress.com/2009/10/19/grothendiecks-definition-of-a-group/)  
	Defining a group in terms of a special Sheaf.
	
*  [What is a Gauge](https://terrytao.wordpress.com/2008/09/27/what-is-a-gauge/)  
	The relationship between group and connections.

* [Group cohomology in triangles](https://terrytao.wordpress.com/2012/05/11/cayley-graphs-and-the-algebra-of-groups/)

#### Topology

* [Compactness, the concept so much of math is built on](https://www.youtube.com/watch?v=td7Nz9ATyWY)

### Other

* [Rhythm / Pitch Duality: hear rhythm become pitch before your ears](https://dantepfer.com/blog/?p=277)

* Poincaré, H. (1905). *La Valeur de la Science*. Paris: Ernest Flammarion.


* Piaget, J., & Inhelder, B. (1956). *The Child's Conception of Space*. London: Routledge & Kegan Paul.


* Piaget, J. (1969). *The Child's Conception of Time*. London: Routledge & Kegan Paul.


* ["The Two Cultures"](https://sciencepolicy.colorado.edu/students/envs_5110/snow_1959.pdf) C.P. Snow


* [Alternative Science Respectability Checklist](https://www.preposterousuniverse.com/blog/2007/06/19/the-alternative-science-respectability-checklist/) S. Carroll
