---
layout: article 
title: 🥁 Rhythmic Clock ⏰
permalink: /rhythmic-clock
key: page-clock
---


<style>
	audio {
		display: block;
		margin-bottom: 20px;
	}
   .audio-container {
	   display: none;
   }
</style>

<div id="audio-container">
	<audio id="synchronizedAudio" class="audio-container">
		<source id="audioSource" type="audio/mp3">
		Your browser does not support the audio element.
	</audio>
</div>

<a id="playPauseButton" class="button button--xl button--primary button--pill">▶ Play</a>


## Using the Clock

The clock is practical, and with 15-30 minutes of practice, you'll be able to tell the time.
For more details, refer to the [detailed instructions]({% post_url 2024-07-09-Rhythmic-Clock %}).

## Cheatsheet

### Sounds

- ⚪ Synthethizer playing the melody
- ⚫ Silence
- 🟠 Cymbal (indicates the hour)
- 🔴 Bell (indicates minutes)

### Melody Structure

| Melody | ⚪    | ⚪    | ⚪   | ⚪  | ⚫   | ⚫  | ⚫  | ⚫  | ⚪    | ⚫    | ⚫    | ⚪    | ⚪    | ⚫  | ⚪ | ⚫  |
|--|-------|-------|------|-----|------|-----|-----|-----|-------|-------|-------|-------|-------|----|----|----|
| **Hour** | |  |   |   | 12  |  1  | 2  | 3       | 4 | 5   | 6     |  7          | 8 |  9  | 10   | 11   |
| **Cymbal** | |      |     |   |   |    |   |   🟠         |  |    |      |            |  |    |    |    |
| **Quarter** | **1** |  1  | 1   |  1 | **2** |  2  | 2  | 2  | **3** | 3    | 3    | 3   | **4** | 4   | 4   |  4  |
| **Minute** | |    |    |   |   |    |   |            | 🔴  |  🔴   |   🔴   |      🔴   |  |    |    |    |


- **Hour**: The hour is indicated by the position of the cymbal 🟠 in the rhythm (here it indicates 3 o'clock).
- **Quarter**: The hour quarter is indicated by the position of the bells 🔴 (here it indicates the 3rd quarter, minutes 30-44).
- **Minute**: The minute within the quarter is given by the minute pattern. In this example, the pattern is 🔴🔴🔴🔴, which represents minute 0.

So, the time indicated is 3:30.

<audio controls>
  <source src="{{ '/assets/rhythmic_clock/sample_3h30.mp3' | relative_url }}" type="audio/mpeg">
  Your browser does not support the audio element.
</audio>

### All minute patterns

<ol start="0">
  <li>🔴 🔴 🔴 🔴</li>
  <li>🔴 🔴 🔴 ⚫</li>
  <li>🔴 🔴 ⚫ ⚫</li>
  <li>🔴 ⚫ ⚫ ⚫</li>
  <li>⚫ ⚫ ⚫ 🔴</li>
  <li>⚫ ⚫ 🔴 ⚫</li>
  <li>⚫ 🔴 ⚫ ⚫</li>
  <li>🔴 ⚫ ⚫ 🔴</li>
  <li>⚫ ⚫ 🔴 🔴</li>
  <li>⚫ 🔴 🔴 ⚫</li>
  <li>🔴 🔴 ⚫ 🔴</li>
  <li>🔴 ⚫ 🔴 ⚫</li>
  <li>⚫ 🔴 ⚫ 🔴</li>
  <li>🔴 ⚫ 🔴 🔴</li>
  <li>⚫ 🔴 🔴 🔴</li>
</ol>


<script src="{{ '/assets/rhythmic_clock/synchronize-audio.js' | relative_url }}"></script>
<script>
	const baseUrl = '{{ "/assets/rhythmic_clock/" | relative_url }}';
	register(baseUrl);
</script>
