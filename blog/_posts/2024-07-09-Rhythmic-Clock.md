---
layout: article
title: Rhythmic Clock
date: 2024-07-09 10:59:47
cover: 'assets/rhythmic_clock/rhythmic-clock-circular.webp'
categories: time
license: true
tags: music rhythm clock time
aside:
    toc: true
---

This clock uses rhythms to show the time.
In this article, you'll learn how the hours and minutes are turned into music.
When you're ready, press play and "hear the time."

<style>
	audio {
		display: block;
		margin-bottom: 20px;
	}
   .audio-container {
	   display: none;
   }

.media-container {
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 0 auto;
    width: 90%;
}

.image-container {
	width: 50%
}

.image-container img {
    max-width: 100%;
    height: auto;
}

.button-container {
	width: 50%
}
</style>


<div class="media-container">

<div class="button-container">
	<p>Current time:
	<a id="playPauseButton" class="button button--xl button--primary button--pill">▶ Play</a>
	</p>
</div>

<div class="image-container" markdown="1">
![clock]({{ '/assets/rhythmic_clock/rhythmic-clock-circular.webp' | relative_url }})
</div>

</div>

<div id="audio-container">
	<audio id="synchronizedAudio" class="audio-container">
		<source id="audioSource" type="audio/mp3" src="">
		Your browser does not support the audio element.
	</audio>
</div>


## Clock Instructions

### The base rhythm



| **Bar** | **1** |       | .    |     | **2** |    | .  |    | **3** |       | .     |       | **4** |    |    |    |
||-------|-------|------|-----|-------|----|----|----|-------|-------|-------|-------|-------|----|----|----|
| **Synth** | ⚪    | ⚪    | ⚪   | ⚪  | ⚫    | ⚫  | ⚫  | ⚫  | ⚪    | ⚫    | ⚫     | ⚪    | ⚪    | ⚫  | ⚪ | ⚫  |


The base rhythm is composed of 4 bars of 4 ticks each,
making a total of 16 ticks for the whole phrase.
The base rhythm is played by a synth, where ⚪ represents a tick with sound and ⚫ represents a tick of silence.
This rhythm repeats continuously.

<audio controls>
  <source src="{{ '/assets/rhythmic_clock/sample_melody.mp3' | relative_url }}" type="audio/mpeg">
  Your browser does not support the audio element.
</audio>

### Telling the hour

To indicate the hour, we label the last 12 ticks from 0 to 11 (similar to a wall clock).
There is a symbol (🟠) on the tick that corresponds to the hour.
📀 It is played by a cymbal.

For example, this represents 12 o'clock:


| **Hour** | .|   .   | .    |  . | 12  |  1  | 2  | 3           | 4 | 5   | 6     |  7          | 8 |  9  | 10   | 11   |
|-------|-------|------|-----|------|-----|-----|-----|-------|-------|-------|-------|-------|----|----|----|
| **Synth**| ⚪    | ⚪    | ⚪   | ⚪  | ⚫   | ⚫  | ⚫  | ⚫  | ⚪    | ⚫    | ⚫    | ⚪    | ⚪    | ⚫  | ⚪ | ⚫  |
| **Cymbal**| |      |     |   | 🟠  |    |   |            |  |    |      |            |  |    |    |    |

<audio controls>
  <source src="{{ '/assets/rhythmic_clock/sample_12oclock.mp3' | relative_url }}" type="audio/mpeg">
  Your browser does not support the audio element.
</audio>

And this represents 6 o'clock:


| **Hour**| .|   .   | .    |  . | 12  |  1  | 2  | 3           | 4 | 5   | 6     |  7          | 8 |  9  | 10   | 11   |
|-------|-------|------|-----|------|-----|-----|-----|-------|-------|-------|-------|-------|----|----|----|
| **Synth**| ⚪    | ⚪    | ⚪   | ⚪  | ⚫   | ⚫  | ⚫  | ⚫  | ⚪    | ⚫    | ⚫    | ⚪    | ⚪    | ⚫  | ⚪ | ⚫  |
| **Cymbal** | |      |     |   |   |    |   |            |  |    |   🟠   |            |  |    |    |    |

<audio controls>
  <source src="{{ '/assets/rhythmic_clock/sample_6oclock.mp3' | relative_url }}" type="audio/mpeg">
  Your browser does not support the audio element.
</audio>

The clock doesn't tell if it's am or pm.

### Telling the Quarter Hour

🔔  The minute information is conveyed by a bell-like percussion (🔴).
There are 4 quarter hours in an hour, and the bar in which the bell rings indicates the quarter hour.

In this example, the bell falls in the third bar, corresponding to the third quarter (minutes 30 to 44):


| **Quarter** | **1** |       | .    |     | **2** |    | .  |    | **3** |       | .     |       | **4** |    |    |    |
|-------|-------|------|-----|------|-----|-----|-----|-------|-------|-------|-------|-------|----|----|----|
| **Synth** | ⚪    | ⚪    | ⚪   | ⚪  | ⚫   | ⚫  | ⚫  | ⚫  | ⚪    | ⚫    | ⚫    | ⚪    | ⚪    | ⚫  | ⚪ | ⚫  |
| **Bell**| |      |     |   |   |    |   |            |  |    |   🔴   |            |  |    |    |    |

<audio controls>
  <source src="{{ '/assets/rhythmic_clock/sample_min_35.mp3' | relative_url }}" type="audio/mpeg">
  Your browser does not support the audio element.
</audio>

It does not matter where the bell rings within the bar or how many times it rings.
For example, this is also the third quarter:


| **Quarter** | **1** |       | .    |     | **2** |    | .  |    | **3** |       | .     |       | **4** |    |    |    |
|-------|-------|------|-----|------|-----|-----|-----|-------|-------|-------|-------|-------|----|----|----|
| **Synth** | ⚪    | ⚪    | ⚪   | ⚪  | ⚫   | ⚫  | ⚫  | ⚫  | ⚪    | ⚫    | ⚫    | ⚪    | ⚪    | ⚫  | ⚪ | ⚫  |
| **Bell**| |      |     |   |   |    |   |            | 🔴  |  🔴   |   🔴   |      🔴   |  |    |    |    |

<audio controls>
  <source src="{{ '/assets/rhythmic_clock/sample_min_30.mp3' | relative_url }}" type="audio/mpeg">
  Your browser does not support the audio element.
</audio>

### Telling the Minute

Each quarter has 15 minutes,
so we encode minutes 0 to 14 within each quarter using a rhythmic sequence of 4 ticks.

For example, minute 0 is represented by the pattern: 🔴 🔴 🔴 🔴.


| **Quarter** | **1** |       | .    |     | **2** |    | .  |    | **3** |       | .     |       | **4** |    |    |    |
|-------|-------|------|-----|------|-----|-----|-----|-------|-------|-------|-------|-------|----|----|----|
| **Synth**| ⚪    | ⚪    | ⚪   | ⚪  | ⚫   | ⚫  | ⚫  | ⚫  | ⚪    | ⚫    | ⚫    | ⚪    | ⚪    | ⚫  | ⚪ | ⚫  |
| **Bell**| 🔴|    🔴  |    🔴 |  🔴 |   |    |   |            |  |    |      |            |  |    |    |    |

Here, the bell falls within the first bar, indicating minute 0 of the first quarter (minute 0 of the hour).

<audio controls>
  <source src="{{ '/assets/rhythmic_clock/sample_min_0.mp3' | relative_url }}" type="audio/mpeg">
  Your browser does not support the audio element.
</audio>

Let’s revisit a previous example:

| **Quarter**| **1** |       | .    |     | **2** |    | .  |    | **3** |       | .     |       | **4** |    |    |    |
|-------|-------|------|-----|------|-----|-----|-----|-------|-------|-------|-------|-------|----|----|----|
| **Synth**| ⚪    | ⚪    | ⚪   | ⚪  | ⚫   | ⚫  | ⚫  | ⚫  | ⚪    | ⚫    | ⚫    | ⚪    | ⚪    | ⚫  | ⚪ | ⚫  |
| **Bell**| |      |     |   |   |    |   |            | 🔴  |  🔴   |   🔴   |      🔴   |  |    |    |    |


This represents minute 0 of the third quarter, which is minute 30 of the hour.

<audio controls>
  <source src="{{ '/assets/rhythmic_clock/sample_min_30.mp3' | relative_url }}" type="audio/mpeg">
  Your browser does not support the audio element.
</audio>

The pattern for minute 11 is 🔴 ⚫ 🔴 ⚫ , you can see:


| **Quarter**| **1** |       | .    |     | **2** |    | .  |    | **3** |       | .     |       | **4** |    |    |    |
|-------|-------|------|-----|------|-----|-----|-----|-------|-------|-------|-------|-------|----|----|----|
| **Synth**| ⚪    | ⚪    | ⚪   | ⚪  | ⚫   | ⚫  | ⚫  | ⚫  | ⚪    | ⚫    | ⚫    | ⚪    | ⚪    | ⚫  | ⚪ | ⚫  |
| **Bell**| |      |     |  |  🔴 |   |  🔴  |     |  |    |      |            |  |    |    |    |

This falls in the second bar, indicating minute 11 of the second quarter (minute 26).

<audio controls>
  <source src="{{ '/assets/rhythmic_clock/sample_min_26.mp3' | relative_url }}" type="audio/mpeg">
  Your browser does not support the audio element.
</audio>

### Pattern Sequences For Minutes


To determine the pattern for each minute, enumerate the ticks from 0 to 14, excluding the first tick of bar 2.
Here’s the enumeration:

| **0** |  1    | 2    | 3   | ❌ | 4  | 5  | 6  | **7** |  8    | 9     | 10     | **11** | 12  | 13  | 14   |
|-------|-------|------|-----|-------|----|----|----|-------|-------|-------|-------|-------|----|----|----|
| ⚪    | ⚪    | ⚪   | ⚪  | ⚫    | ⚫  | ⚫  | ⚫  | ⚪    | ⚫    | ⚫     | ⚪    | ⚪    | ⚫  | ⚪ | ⚫  |

To find the pattern for minute 7, start at that index and write down the next 4 ticks: ⚪ ⚫ ⚫ ⚪.

| 0 |  1    | 2    | 3   | ❌ | 4  | 5  | 6  | **7** |  8    | 9     | 10     | 11 | 12  | 13  | 14   |
|-------|-------|------|-----|-------|----|----|----|-------|-------|-------|-------|-------|----|----|----|
| ⚪    | ⚪    | ⚪   | ⚪  | ⚫    | ⚫  | ⚫  | ⚫  | ⚪    | ⚫    | ⚫     | ⚪    | ⚪    | ⚫  | ⚪ | ⚫  |
|    |    |   |  |    |   |   |   |  ⬆️   |  ⬆️  |  ⬆️   |  ⬆️  |     |   |  |   |

If you reach the end of the sequence, loop around.
For example, minute 13: ⚪ ⚫ ⚪ ⚪.

Skipping the ❌ is necessary as it represents 4 silences (⚫ ⚫ ⚫ ⚫), which are inaudible.


Alternatively, learn the patterns by heart:

<ol start="0">
  <li>🔴 🔴 🔴 🔴</li>
  <li>🔴 🔴 🔴 ⚫</li>
  <li>🔴 🔴 ⚫ ⚫</li>
  <li>🔴 ⚫ ⚫ ⚫</li>
  <li>⚫ ⚫ ⚫ 🔴</li>
  <li>⚫ ⚫ 🔴 ⚫</li>
  <li>⚫ 🔴 ⚫ ⚫</li>
  <li>🔴 ⚫ ⚫ 🔴</li>
  <li>⚫ ⚫ 🔴 🔴</li>
  <li>⚫ 🔴 🔴 ⚫</li>
  <li>🔴 🔴 ⚫ 🔴</li>
  <li>🔴 ⚫ 🔴 ⚫</li>
  <li>⚫ 🔴 ⚫ 🔴</li>
  <li>🔴 ⚫ 🔴 🔴</li>
  <li>⚫ 🔴 🔴 🔴</li>
</ol>

The following audio contains each minute from 0 to 59.
Each phrase is repeated twice: you'll hear minutes 0, 0, 1, 1, ..., 58, 58, 59, 59.
The total duration is 8 minutes.

<audio controls>
  <source src="{{ '/assets/rhythmic_clock/sample_all_min.mp3' | relative_url }}" type="audio/mpeg">
  Your browser does not support the audio element.
</audio>

### Putting it all Together

Here's a sample with both hour and minutes indicating 3:30.


| Melody | ⚪    | ⚪    | ⚪   | ⚪  | ⚫   | ⚫  | ⚫  | ⚫  | ⚪    | ⚫    | ⚫    | ⚪    | ⚪    | ⚫  | ⚪ | ⚫  |
|--|-------|-------|------|-----|------|-----|-----|-----|-------|-------|-------|-------|-------|----|----|----|
| **Hour** | |  |   |   | 12  |  1  | 2  | 3       | 4 | 5   | 6     |  7          | 8 |  9  | 10   | 11   |
| **Cymbal** | |      |     |   |   |    |   |   🟠         |  |    |      |            |  |    |    |    |
| **Quarter** | **1** |  1  | 1   |  1 | **2** |  2  | 2  | 2  | **3** | 3    | 3    | 3   | **4** | 4   | 4   |  4  |
| **Minute** | |    |    |   |   |    |   |            | 🔴  |  🔴   |   🔴   |      🔴   |  |    |    |    |


<audio controls>
  <source src="{{ '/assets/rhythmic_clock/sample_3h30.mp3' | relative_url }}" type="audio/mpeg">
  Your browser does not support the audio element.
</audio>

You're now ready to go back to the head of the article and listen to the current time.

## How It's Made


The base rhythm is a De Bruijn sequence of span 4, ensuring every 4 consecutive ticks are unique.
This lets us use subrhythms to create minute patterns.

I use Python to generate the audio.
The clock is a 12-hour loop.
Each hour of audio is created with Python using the Mido library.
Then, I use FluidSynth to convert MIDI to WAV and compress it to mono MP3 to save space.

On the JavaScript side, a function loads and synchronizes the correct audio based on the current time.
It resynchronizes periodically and handles hour changes.

The Python code is available on [Gitlab](https://gitlab.com/Emmanuel_B/rhythmic-clock).


<script src="{{ '/assets/rhythmic_clock/synchronize-audio.js' | relative_url }}"></script>
<script>
	const baseUrl = '{{ "/assets/rhythmic_clock/" | relative_url }}';
	register(baseUrl);
</script>
