---
layout: article
title:  "Galilean 1+1 Notebook"
date: 2022-09-15 16:50:47 +0000
key: "GALILEAN_1P1"
comment: false
visible: false
hidden: true
mermaid: true
mathjax: true
mathjax_autonumber: true
categories: mathematics 
license: true
cover: 'assets/galilean_1p1/one-line-girl.svg'
aside:
    toc: true
tags: ["group theory", mathematics, "galilean group"]
---

# Galilean Group in 1+1 dimensions


             _
         __ ( }
       '---. _`---,
       ___/ /
     /,---'\\
           //
          '==
_Art by Joan Stark_

__Table of Contents:__
1. Key Properties and Intuition
2. Definition
3. Famous Actions
    1. Action on (x,t) events
    2. Isomorphism with Heisenberg Group



## 1. Key Properties and Intuition

The 1+1 Galilean group is chiefly defined by its generators and relations.
The generators correspond to movement you can do in that spacetime, they are:
1. Space translations: D (lie generator), $g(x,0,0)$
1. Time transaltions: H (lie generator), $g(0,s,0)$
1. Boosts: B (lie generator), $g(0,0,s)$

Those generators verify:

$$
[D,H] = e \\
[B,D] = e \\
[B,H] = D^{-1}
$$

We're first going to give some illustrations and intuition behind those relations.

### 1.1 Moving and Waiting $[D,H]=e$

Here we notice the fact that moving then waiting gets us to the same spot as waiting then moving!

The animation below shows the reasoning behind the equation $DH=HD$ which we can reformulate as $[D,H]=e$



<video src="{{ 'assets/galilean_1p1/MoveAndWait.mp4' | relative_url }}" controls autoplay loop style="max-width: 75%;"  >
      Your browser does not support the <code>video</code> element.
    </video>


### 1.2 Moving and Boosting $[B,D]=e$

Moving then accelerating is the same as accelerating then moving.

The animation below demonstrates the equality $$



<video src="{{ 'assets/galilean_1p1/MoveAndBoost.mp4' | relative_url }}" controls autoplay loop style="max-width: 75%;"  >
      Your browser does not support the <code>video</code> element.
    </video>


### 1.3. Boosting and Waiting $[B,H] = D^{-1}$

Accelerating then waiting gets you further away than waiting then accelerating.

Below we can see a proof that $HB=DBH$.
Equivalently, we can rephrase it as: $BHB^{-1} = D^{-1}H$ or $$[B,H] = D^{-1}$$

   
<video src="{{ 'assets/galilean_1p1/WaitAndBoost.mp4' | relative_url }}" controls autoplay loop style="max-width: 75%;"  >
      Your browser does not support the <code>video</code> element.
    </video>



## 2. Definition of the Galilean Group

Now we turn to giving an explicit construction of the group.

We consider a point particle moving on a line.
Its coordinates can be described with 3 numbers (x,t,v), where:
* x is the position on the line
* t is the time of the observation
* v is the current speed

We introduce the following 3 operators:
* D moves the particle observation to the right by one unit:  
   $d \cdot (x,t,v) \mapsto (x+1,t,v)$
* H moves the particle observation one unit in time.  
   $h \cdot (x,t,v) \mapsto (x+v, t+1, v)$
* B accelerates the particle by one unit:  
   $b \cdot (x,t,v) \mapsto (x,t,v+1)$
  
 Considering now a random element $d^yh^sb^a \in \mathcal{Gal}(1+1)$.
 We can define its action by extending the relations given above:
 $$d^yh^sb^a \cdot (x,t,v) = (x+y+s(v+a),t+s,v+a)$$
 
 In the method `Galilean1p1.act_point` below, we rewrite this equation as a function that takes as input 2 tuples of 3 elements: (y,s,a) et (x,t,v).
 
 Considering now two random elements: $d^yh^sb^a, d^zh^rb^w \in \mathcal{Gal}(1+1)$,
 we can compute the "double action": 
 $$d^zh^rb^w \cdot (d^yh^sb^a \cdot (x,t,v)) = (x+y+z+r(v+a+w)+s(v+a),t+s+r, v+a+w)$$
 We have:
 $$d^zh^rb^w \cdot (d^yh^sb^a \cdot (x,t,v)) = d^{y+z-sw}h^{s+r}b^{a+w} \cdot (x,t,v)$$
 And so we define:
 $$d^zh^rb^w \times d^yh^sb^a = d^{y+z-sw}h^{s+r}b^{a+w}$$
 In order to get the relation:
 $$(d^zh^rb^w \times d^yh^sb^a) \cdot (x,t,v) = d^zh^rb^w \cdot (d^yh^sb^a \cdot (x,t,v)) $$
 This multiplication is defined in the method `Galilean1p1.__mul__`.

First we verify that the `*` operation verifies all the group axioms:
1. $(0,0,0)$ is an identity
1. Existence of inverses
1. Associativity

    Testing identity...
    Ok.
    Testing inverses...
    Ok.
    Testing associativity...
    Ok.


🏆 Now we check that it verifies the commutation relationships, which proves that it is indeed our Galilean group! 

    Testing commutation relationships...
    Ok.


Here we check that the group action axioms are respected, makes sense that they work out since they are what we used to build the group operation to begin with.

    Testing group action axioms...
    Ok.


Here we check that the action behaves as we expect intuitively!

    Testing group action sanity...
    Ok.


## 2. Famous Group Actions

### 2.1 Action on (x,t) events

* D . (x,t) = (x-1,t)
* H . (x,t) = (x,t-1)
* B . (x,t) = (x-t,t)

    Testing group action axioms...
    Ok.


### 2.2 Isomorphism with the Heisenberg Group

We can use this group action to give a representation $\rho : \mathcal{Gal} \rightarrow \mathrm{GL}(3)$.

    Testing group representation axioms...
    Ok.


The representation is such that $g \cdot (x,t) = \rho(g) \begin{pmatrix} x \\ t \\ 1 \end{pmatrix}$

We check this formula below.

    Testing group action axioms...
    Ok.


We can give an explicit formula for the group representation:

$$\rho(D^xH^tB^v) = \begin{pmatrix}
1 && -v && -x \\
0 && 1 && -t \\
0 && 0 && 1
\end{pmatrix}
$$

This shows that $\mathcal{Gal}(1+1)$ is isomorphic to the _Heisenberg group_.

### 2.3 Action On Solutions of Schrödinger's Equation



#### 2.3.1 Definition and Physical Interpretation

Here we consider solutions $\phi$ of the free Schrödinger's equation in 1+1 spacetime:
$$i{\delta \phi \over \delta t} = -{1 \over 2}{\delta^2 \phi \over \delta x^2}$$

We have an action of $\mathcal{Gal}(1+1)$ on functions $\mathbb{R}^2 \rightarrow \mathbb{C}$ given by: $(g \cdot \phi)(x,t) = \phi(g^{-1} \cdot (x,t))$.

Expanding this formula for the generators:
* $\exp(yD) \cdot \phi (x,t) = \phi(x+y,t)$
* $\exp(sH) \cdot \phi (x,t) = \phi(x,t+s)$
* $\exp(vB) \cdot \phi (x,t) = \phi(x+t,t)$

However, we can see that this action does not leave the free Schrödinger's equation invariant.
Letting $\phi' = \exp(vB) \cdot \phi$, we obtain that $\phi'$ is solution to:
$i{\delta \phi' \over \delta t} = iv{\delta \phi' \over \delta t} - {\delta^2 \phi' \over \delta x^2}$

Instead we modify the above action for B, replacing it by:
* $(\exp(vB) \cdot \phi)(x,t) = e^{i(vx-j{v^2t \over 2})}\phi(x+vt, t) = \phi'(x,t)$

Note that this action gives the same result as the previous one _up to a phase factor_.
This is very important because two wave-functions which differ only from a phase factor are __physically identical__, i.e. they represent the same thing practical situation.

The action is taken a 2016 
[article](https://lbt.usal.es/wp-content/uploads/2016/06/PAPER.pdf):
> Cerveró, Jose M. and Pablo P Polo. “The one dimensional Schrödinger equation: symmetries, solutions and Feynman propagators.” European Journal of Physics 37 (2016): 055401.


We have:
* $ {\delta \phi' \over \delta t} = -i{v^2 \over 2}e^{-i(vx+{v^2t \over 2})}\phi(x+vt, t) + v e^{-i(vx+{v^2t \over 2})} {\delta\phi\over \delta x}(x+vt, t) + e^{-i(vx+{v^2t \over 2})}{\delta\phi\over\delta t}(x+vt, t)$
* $ {\delta \phi' \over \delta x} = -ive^{-i(vx+{v^2t \over 2})}\phi(x+vt, t) + e^{-i(vx+{v^2t \over 2})}{\delta\phi\over\delta x}(x+vt, t)$ 
* $ {\delta^2 \phi' \over \delta x^2} = -v^2 e^{-i(vx+{v^2t \over 2})}\phi(x+vt, t) - 2ive^{-i(vx+{v^2t \over 2})}{\delta\phi\over\delta x}(x+vt, t) + e^{-i(vx+{v^2t \over 2})}{\delta^2\phi\over\delta x^2}(x+vt, t)$

Using the expressions for the derivatives above, we have: $i{\delta\phi'\over\delta t} + {1\over2}{\delta^2\phi'\over\delta x^2} = ... = 0$

And so $\phi'$ verifies the free Schrodinger's equation too!

However there is an issue!  
We have $e^{yD} \cdot (e^{aB} \cdot \phi) = e^{-i(v(x+y)+{v^2t\over2})}\phi(x+y+at,t)$ with $e^{aB} \cdot (e^{yD} \cdot \phi) = e^{-i(vx+{v^2t\over2})}\phi(x+y+at,t)$

These two expressions are not equal for $y\neq0$, but $e^{aB}e^{yD}=e^{yD}e^{aB}$ because D commutes with everyone.
This means that our action is __not valid__ as a $\mathcal{Gal}(1+1)$ action.
However, it is possible to make it work, by introducing a new parameter that commutes with every other, this is called a central extension.
There is a suitable central extension that make this action work.

__TODO(Manu)__: Explicit this

#### 2.3.2 Computation I might need later

* $(e^{yD}e^{sH}e^{vB}\cdot\phi)(x,t) = e^{i\xi_v(x+y,t+s)}\phi(x+y+vt+vs, t+s)$
* $(e^{zD}e^{rH}e^{aB}\cdot (e^{yD}e^{sH}e^{vB}\cdot\phi))(x,t) = \\
e^{i\xi_a(x+z,t+r)}e^{i\xi_v(x+y+z+a(t+r), t+s+r)}\phi(x+z+y+(t+r)(a+v)+vs, t+s+r)$
* $e^{zD}e^{rH}e^{aB}\times e^{yD}e^{sH}e^{vB} = e^{(z+y-sa)D}e^{(r+s)H}e^{v+a}$
* $e^{(z+y-sa)D}e^{(r+s)H}e^{(v+a)B} \cdot \phi (x,t) = e^{i\xi_{v+a}(x+z+y-sa, t+r+s)}\phi(x+z+y-sa+(v+a)t+(v+a)(r+s), t+r+s)$
* $\xi_{v+a}(x+z+y-sa, t+r+s) = \xi_a(x+z,t+r) + \xi_v(x+y+z+a(t+r), t+s+r)$

This gives:
* $\xi_0(x,t)=0$

### 2.4 Action on Particle's Trajectories

#### 2.4.1 General Setting

The bread and butter of beginner's classical mechanics is the trajectory of a single particle, modeled as a function $f: \mathbb{R} \rightarrow \mathbb{R}$, since we are still restricting ourselves in 1 dimension of space.

The first $\mathbb{R}$ is time and the second is space, so that $f(t)$ gives the position of the particle at time t.

We define an action via the following:
* $(e^{xD} \cdot f)(t) = f(t) - x$
* $(e^{sH} \cdot f)(t) = f(t+s)$
* $(e^{vB} \cdot f)(t) = f(t)-vt$

Which gives for a generic element:
$$e^{xD}e^{sH}e^{vB} \cdot f (t) = f(t+s)-vt-vs-x$$

We can check via direct computation that it makes a valid action.



We can compute the best linear approximation to each of the generators, which gives us:
* $D(t) = -1$
* $H(t) = {df\over dt}(t)$
* $B(t) = -t$

We can interpret them as, the unit of space (1 meter), the speed, and time respectively.
It's probably possible to verify the commutation relationships using a lie bracket, but I'm not sure how to define it here, since it is an infinite-dimensional action.

#### 2.4.2 Restriction of the Action to Uniform Speed Particles

The particles moving at constant speed have trajectories of the form $f(t)=at+b$.
We can represent such a particle as a vector $\begin{pmatrix} b \\ a \\ 1 \end{pmatrix}$.

The action defined above gives:

$$e^{xD}e^{sH}e^{vB} \cdot \begin{pmatrix} b \\ a \\ 1 \end{pmatrix} = 
\begin{pmatrix} b+s(a-v)-x \\ a-v \\ 1 \end{pmatrix}$$

And so

$$e^{xD}e^{sH}e^{vB} \mapsto \begin{pmatrix} 1 & s & -vs-x\\ 0 & 1 & -v\\ 0 & 0 & 1 \end{pmatrix}$$

This formula is implemented in `Galilean1p1.mat_linear_motion`.

    Testing action axioms...
    Ok.


The infinitesimal generators are:
* $D = \begin{pmatrix} 0 & 0 & -1 \\ 0 & 0 & 0 \\ 0 & 0 & 0 \end{pmatrix}$
* $H = \begin{pmatrix} 0 & 1 & 0 \\ 0 & 0 & 0 \\ 0 & 0 & 0 \end{pmatrix}$
* $B = \begin{pmatrix} 0 & 0 & 0 \\ 0 & 0 & -1 \\ 0 & 0 & 0 \end{pmatrix}$

    Testing infinitesimal commutations...
    Ok.


### 2.5 Unique Unitary Representation

For any $\hbar \in \mathbb{R}$, we have a unitary representation on functions $\mathbb{R} \rightarrow \mathbb{C}$, given by:
$$\left [ \Pi(d^xh^tb^v)\varphi\right ](s) = e^{-i\hbar x}e^{-its}\varphi(s-\hbar v)$$

## Conclusion

Although spacetime in 1+1 dimensions is much simpler than our reality, it is useful in demonstrating phenomenon relating to speed and the passing of time.

    [NbConvertApp] Converting notebook Galilean 1+1.ipynb to markdown
    [NbConvertApp] Writing 12741 bytes to markdown/Galilean 1+1.md

