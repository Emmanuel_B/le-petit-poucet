---
layout: article
title:  "Finite Galilean Group 1+1"
date: 2022-09-20 16:50:47 +0000
key: "FINITE_GAL_1P1_20220920"
comment: false
visible: false
hidden: true
mermaid: true
mathjax: true
mathjax_autonumber: true
categories: mathematics 
license: true
cover: 'assets/galilean_1p1/one-line-girl.svg'
aside:
    toc: true
tags: ["group theory", mathematics, "galilean group"]
---


# Finite Galilean Group in 1+1 dimensions


             _
         __ ( }
       '---. _`---,
       ___/ /
     /,---'\\
           //
          '==
_Art by Joan Stark_

__Table of Content__:
1. Definition of the Group Action
    1. $\mathcal{Gal}_3(1+1)$
    2. Info about Elements
    3. The Group Action
2. Representation Theory
    1. Irreps and Character Table
    2. Projection on $\mathcal{U}_{-1,0} \oplus \mathcal{U}_{0,0} \oplus \mathcal{U}_{1,0}$
    3. Projection on $\mathcal{V} \oplus \mathcal{V'}$
    4. Interpretation of the Decomposition
3. Harmonic Analysis
    1. Definition
    2. Interpretation of the Coefficients
    3. Interpretation of the Product
4. Real Representations


## 1. Definition of the Group and Action

### 1.1 Definition of $\mathcal{Gal}_3(1+1)$

The 1+1 Galilean group is chiefly defined by its generators and relations.
The generators correspond to movement you can do in that spacetime, they are:
1. Space translations: D (lie generator), $g(x,0,0)$
1. Time transaltions: H (lie generator), $g(0,s,0)$
1. Boosts: B (lie generator), $g(0,0,s)$

Those generators verify:

$$
[D,H] = e \\
[B,D] = e \\
[B,H] = D^{-1}
$$

Any element can be uniquely written $D^xH^tB^v$, or in code `Galilean1p1(x,t,v)`.
The group definition is detailed in 'galilean.py' in the class `Galilean1p1`.

__Truncation of the group__: The group $\mathcal{Gal}(1+1)$ is infinite. We turn it into a finite group by imposing the additional relations:  
$$D^3 = H^3 = B^3 = e$$
We'll note the group thus obtained $\mathcal{Gal}_3(1+1)$.

### 1.2 Element Information about the Group

#### Group Structure

__Exercise__: Show that the sequence $0 \rightarrow \langle D, H\rangle \rightarrow G \rightarrow \langle B\rangle \rightarrow 0$ is exact and right-split.
Alternatively if you don't like the language of exact sequences, show that G is a semi-direct product of $\langle D, H\rangle$ and $\langle B\rangle$.


Since D and H commute, we now know that $\lt D, H \gt \sim \mathbb{Z}/3 \times \mathbb{Z}/3$, and together with the previous exercise this gives us that __G has 27 elements__.


__Exercise__: Show that $Z(G) = \langle D \rangle$ and deduce the normal series: 
$0 \lhd \langle D \rangle \lhd \langle D, H \rangle \lhd G$


__Exercise__: Prove that $G/{Z(G)} \sim \mathbb{Z}/3\times \mathbb{Z}/3$

> _Hint_: There are only 2 groups of order 9: $Z/9$ and $Z/3 \times Z/3$.

#### Conjugacy Classes

__Exercise__: Find all the __conjugacy classes__ of G.

> _Answer_: There are 11 classes:
> * 3 classes of size 1, given by representants $e, D, D^{-1}$
> * 8 classes of size 3, given by representants $H, H^{-1}, B, HB, H^{-1}B, B^{-1}, HB^{-1}, H^{-1}B^{-1}$


### 1.3 Definition of the group action on observations
Let's start by defining our observations.
They are fields, we have one (complex-valued) observation for each point (x,t) in space and time.
To make it more accessible to computations, we limit both time and space to the range [-1, 0, 1].

    Testing obs_xt_basis...
    Ok.


We also define a hermitian product between observations, which is given by multiplying coordinate wise and then summing.


![png]({{ 'assets/Finite_gal_1%2B1_files/Finite_gal_1%2B1_11_0.png' | relative_url }} )


Now we compute how the observation change when the observer changes.
We account for 3 basic types of displacements between observers:
1. Change in position
1. Change in clock
1. Change in speed

The action of D corresponds to moving an observer 1 step to the right (positive x direction).
The observation $D\cdot f$ represents intuitively the observation f as observed by the translated observer.  
In particular, assume that we observe f(0,0), then our observer will observe the same value at (-1,0) because he is now standing at (1,0), so (0,0) looks like behind from him.
From this kinds of reasoning we derive the following tests:

    Testing basic sanity of action on observations...
    Ok.


    This verifies that the scalar product is invariant...
    Ok.


    Testing commutation relationships at action level...
    Ok.


    Testing group action axioms...
    Ok.


Our observations clearly represent functions $\mathrm{obs}: [-1,0,1]^2 \rightarrow \mathbb{C}$.
The action we have defined is given by the equation:
$$g \cdot \mathrm{obs}(x,t) = \mathrm{obs}(g^{-1} * (x,t))$$
Where $*$ is the `Galilean1p1.act_event` action on two dimensions (x,t)

## 2. Decomposition into irreps

Here we have the basis functions to decompose the observation space into irreps.
We have the decomposition:

$$\mathcal{F} = \mathcal{U}_{-1,0} \oplus \mathcal{U}_{0,0} \oplus \mathcal{U}_{1,0} \oplus \mathcal{V} \oplus \mathcal{V'}$$

In order to find a decomposition basis, we use the __projection formula__:

$$\pi_W = {\mathrm{dim} W \over |G|} \sum_{g \in G} \overline{\chi_W(g)} \cdot g :
 \mathcal{F} \rightarrow \mathcal{F}$$

Where by g we mean $\rho(g) : \mathcal{F} \rightarrow \mathcal{F}$

This formula will give us a basis for each irrep in $\mathcal{F}$, but before that, we'll justify the decomposition.

### 2.1 Characters and Irreps

__Character table of G__:


| Conj class (size)               	| $e$ (1) 	| $D$ (1)         	| $D^{-1}$ (1)         	| H (3)        	| $H^{-1}$ (3)        	| B (3)        	| BH (3)        	| $BH^{-1}$ (3)         	| $B^{-1}$ (3)        	| $HB^{-1}$ (3)         	| $H^{-1}B^{-1}$ (3)              	|
| :----------------:	|---	|-----------	|-----------	|----------	|----------	|----------	|----------	|-----------	|----------	|-----------	|----------------	|
| $\chi_{0,0}$   	| 1 	| 1         	| 1         	| 1        	| 1        	| 1        	| 1        	| 1         	| 1        	| 1         	| 1              	|
| $\chi_{0,1}$   	| 1 	| 1         	| 1         	| 1        	| 1        	| w        	| w        	| w         	| $w^{-1}$ 	| $w^{-1}$  	| $w^{-1}$       	|
| $\chi_{0,-1}$  	| 1 	| 1         	| 1         	| 1        	| 1        	| $w^{-1}$ 	| $w^{-1}$ 	| $w^{-1}$  	| w        	| w         	| w              	|
| $\chi_{1,0}$   	| 1 	| 1         	| 1         	| w        	| $w^{-1}$ 	| 1        	| w        	| $w^{-1}$  	| 1        	| w         	| $w^{-1}$       	|
| $\chi_{1,1}$   	| 1 	| 1         	| 1         	| w        	| $w^{-1}$ 	| w        	| $w^{-1}$ 	| 1         	| $w^{-1}$ 	| 1         	| w              	|
| $\chi_{1,-1}$  	| 1 	| 1         	| 1         	| w        	| $w^{-1}$ 	| $w^{-1}$ 	| 1        	| w         	| w        	| $w^{-1}$  	| 1              	|
| $\chi_{-1,0}$  	| 1 	| 1         	| 1         	| $w^{-1}$ 	| w        	| 1        	| $w^{-1}$ 	| w         	| 1        	| $w^{-1}$  	| w              	|
| $\chi_{-1,1}$  	| 1 	| 1         	| 1         	| $w^{-1}$ 	| w        	| w        	| 1        	| $w^{-1}$  	| $w^{-1}$ 	| w         	| 1              	|
| $\chi_{-1,-1}$ 	| 1 	| 1         	| 1         	| $w^{-1}$ 	| w        	| $w^{-1}$ 	| w        	| 1         	| w        	| 1         	| $w^{-1}$       	|
| $\chi_V$       	| 3 	| 3w        	| $3w^{-1}$ 	| 0        	| 0        	| 0        	| 0        	| 0         	| 0        	| 0         	| 0              	|
| $\chi_V'$       	| 3 	| $3w^{-1}$ 	| 3w        	| 0        	| 0        	| 0        	| 0        	| 0         	| 0        	| 0         	| 0              	|



#### Derivation of the Character Table

__Exercise__: Compute the full character table of G.

__Hints__: G has 11 conjugacy classes so we know it has 11 irreducible representations.
9 of them can be obtained by computing the representations of $G/{Z(G)} \sim \mathbb{Z}/3\times \mathbb{Z}/3$ and pulling them back to G.
Using the orthogonality relationships, we can deduce that the 2 remaining irreps have both dimension 3.
Let's call them V and V', let's note $\rho: G \rightarrow \text{GL}(V)$ the representation.
D acts on V and has 3 eigenvectors. Considering the actions of V and V' on these, conclude that they must all have equal eigenvalues.
So $\rho(D) =  \lambda I_3$, we must also have $\lambda ^ 3 = 1$ so we can pick $\rho(D) = \omega I_3$.
We can now consider the 3 eigenvector of H on V and how B must act on them,
from which we can shows that all the eigenvalues of H must be distincts and that B permutes H's eigenvectors.
Accordingly, we can choose:

$$\rho(H) = 
\begin{pmatrix} 
1 & 0 & 0 \\
0 & \omega & 0 \\
0 & 0 & \omega^{-1} \end{pmatrix}$$ 

and

$$
\rho(B) = \begin{pmatrix}
0 & 1 & 0\\
0 & 0 & 1\\
1 & 0 & 0 \end{pmatrix}
$$

We can check that the above matrices verify the relations of G, which makes it a valid representation.
Computing the trace of every conjugacy class, we verify that $\langle \chi_V, \chi_V \rangle = 1$ which shows that it is irreducible.

Finally, using the orthogonality relationships, we can find the last row.

__Representation matrices__

The actual operators of the representations are given by the trace itself for the 9 unidimensional irreps.
For V, we can take the matrices given in the hints of the previous exercise.

For V' we can use the conjugate of the matrices for V!


### 2.2 Projection on $\mathcal{U}_{-1,0} \oplus \mathcal{U}_{0,0} \oplus \mathcal{U}_{1,0}$

    Testing projections on U(i,0) factors...
    OK.



![png]({{ 'assets/Finite_gal_1%2B1_files/Finite_gal_1%2B1_27_0.png' | relative_url }})


    Testing U(s,0) basis vectors behave as expected under G-action...
    Ok.


### 2.3 Projection on $\mathcal{V} \oplus \mathcal{V'}$

Projection on the 2 tridimensional irreps.

    Obtaining the basis v(t) from projection of certain observation...
    Ok.


We observe that V admits a basis made of 3 eigenvector.
We can interpret $\langle v(i)|$ as the operator giving the position of the center of mass at time i.
* Space translations just multiply all of them by $\omega$.
    This means that it just rotates the position of the center of mass on the unit circle.
* Time translations swaps them in a circle: $v(1) \mapsto v(0) \mapsto v(-1) \mapsto v(1)$
* Boosts act like twists in a way

    Test actions of D,H,B on basis of V...
    Ok.


Visualization of the basis v(-1), v(0), v(1) of V.


![png]({{ 'assets/Finite_gal_1%2B1_files/Finite_gal_1%2B1_35_0.png' | relative_url }})


We give the matrices for the element of G when they act on the v(-1)..v(1) basis of $\mathcal{V}$.

    Testing that the matrices are correct for V action...
    Ok.


We can also present V by a basis of eigenvectors H:

Here we give a similar decomposition for V'.
We see that the basis is just given by conjugation of basis for V.
* Time translation still shifts the basis vector in the same direction
* Space translations multiplies by $\bar \omega$ (which we write m)
* Boosts twists again but in the opposite direction as for V

    Test actions of D,H,B on basis of V'...
    Ok.


Matrices for the action of G on v'(-1)..v(1).

    Checks that the basis of the irreps is orthonormal...
    Ok.


Now that we have a basis for the 5 irreps of $\mathcal{F}$, we can make a function that decompose an arbitrary irrep in the xt basis into the irrep basis.

We create an observation of a particle at rest at 0.
We also compute its decomposition, we observe:
* A total non-null "mass" on `u(0)`
* Constant center-of-mass evolution on both V and V'


    
![png](Finite_gal_1%2B1_files/Finite_gal_1%2B1_50_0.png)
    


    Testing that decomposing and recomposing are inverse operations...
    Ok.


### 2.4 Interpretation of the Decomposition

Here we give an interpretation of the basis vectors, for $o$ a real-valued observation.
For example, we can suppose that $o(x,t)$ represents the mass measured at position x and time t.
* The component on u(0) is given by ${1 \over 9} \sum_{i,j} o(i,j)$.  
    It can be interpreted as the total mass of the observation.
    It is an invariant of the observation.
* The components on u(1) and u(-1) are conjugates => we have 
    $\overline{\langle o | u(-1)\rangle} = \langle o | u(1) \rangle$
    * An explicit formula for the component on u(1):
        $\langle o | u(1) \rangle= {1 \over 9} \sum_{i,j \in [-1,0,1]} o(i,j)\omega^i$
* The condition "component on u(1) is null" is equivalent to the condition "mass is conserved in time".  
    Since we will often consider observations of mass evolving according to the law of classical mechanics, in which mass is conserved, we will often use this result.
* The coefficent on $v(i)$ is the conjugate of the one on $v'(i)$
* The coefficient of the irrep $\mathcal{V}$ can be interpreted as the trajectory of the center of mass.  
    The coefficient at time i is given by: $\langle o | v(i) \rangle= {\sqrt{3} \over 9} \left [ o(i,-1) \omega^{-1} + o(i,0) + o(i,1) \omega \right ]$  
    We can interpret this formula by putting particles of mass o(-1), 0(0), 0(1), respectively at positions $\omega^{-1}$, 1, $\omega$ on the unit circle, and averaging to get the center of mass.
    Since the action of D "wraps around" the space axis back to itself, it makes sense to consider the space axis as a circle rather than as a line.

## 3. Harmonic Analysis

The formula for the Fourier transform that I have, uses functions defined on the group G itself.
Our observations are defined on the space time torus... but we can extend them to a function on the whole of G.
If f is an observation, we define its extension $f'$ by:
$f': g \mapsto (g \cdot f)(0,0)$

We can compute f back from f' using the formula: $f(x,t) = f'(D^{x}H^{t})$

Letting $\rho$ denote an irrep and g an element of G.
The formula for Fourier inversion are given by:
* $\hat{f'}(\rho) = \sum_g f'(g) \rho(g)$
* $f'(g)={1 \over G} \sum_{\rho} \dim V_{\rho} \mathrm{tr}(\rho(g^{-1}) \cdot \hat{f'}(\rho))$

So we get the following equations for f:
* $\hat{f}(\rho) = \sum_g (g \cdot f)(0,0) \rho(g)$
* $f(x,t) = {1 \over G} \sum_{\rho} \dim V_{\rho} \mathrm{tr}(\rho(D^{-x}H^{-t}) \cdot \hat f(\rho))$

### 3.1 Definition

In order to help debugging, we notice through a simple computation that the Fourier components on V and V' are conjugates to each other when the input is real.

    Testing that V and V' comp. are conjugates for real obs...
    Ok.


Proof of the Fourier inversion formula, see this 
[math.stackexchange question](https://math.stackexchange.com/questions/3258833/fourier-inversion-theorem-and-plancherels-identity-in-the-context-of-representa)
for more details.

We have:  
$${1 \over G} \sum_{\rho} \dim V_{\rho} \mathrm{tr}(\rho(g^{-1}) \cdot \hat{f'}(\rho)) \\
= {1 \over G} \sum_{\rho} \sum_h \mathrm{tr}(\rho(e)) \mathrm{tr}(\rho(g^{-1}h)) f(h) \\
= \sum_h \left [ {1 \over G} \sum_{\rho} \overline{\mathrm{tr}(\rho(e))} \mathrm{tr}(\rho(g^{-1}h)) \right ] f(h) \\
= f(g)
$$

Where the first equality is given by the Fourier transform formula and the last one by the orthogonality of characters.

    Testing that Fourier and Fourier inv are inverses...
    Ok.


We can explicit the construction of f' from f, using the action of the group on events (x,t), as defined in `Galilean1p1.act_event`.
We have:  
$$f'(g) = (g \cdot f)(0,0) = f(g^{-1} \cdot (0,0))$$
Writing $g = D^xH^tB^v$ we get:
$$f'(D^xH^tB^v) = f(x+tv,t)$$

### 3.2 Interpretation of the Fourier Coefficients


Let's start with computing the Fourier transform for a random obs.

    Original observation
    [[1.65 1.03 0.6 ]
     [1.5  1.76 1.18]
     [0.89 1.62 0.82]]
    U comp
    [[ 0.    0.    0.  ]
     [ 2.79  5.21 12.16]
     [ 0.    0.    0.  ]]
    V comp
    [[ 0.   -0.   -0.  ]
     [ 4.4   1.41  5.05]
     [-0.    0.    0.  ]]
    [[ 0.    0.    0.  ]
     [-7.44 10.28  2.05]
     [ 0.   -0.    0.  ]]
    Rank of V comp: 1


__Interpretation of Fourier Coefficients__: The Fourier coefficients corresponds to the observation's decomposition in an orthogonal basis made from the irreducible representations basis vectors.
Let O be an observation.
The components of $\hat O$ are given by:
* $\hat O (\mathcal{U}(s,0)) = 27 \langle O, u(-s,0) \rangle$
* $\hat O (\mathcal{V}) = 9 \sqrt{3} \begin{pmatrix}
    0 & 0 & 0 \\
    \langle O, v'(-1) \rangle & \langle O, v'(0) \rangle & \langle O, v'(1) \rangle \\
    0 & 0 & 0
    \end{pmatrix}$
* $\hat O (\mathcal{V'}) = 9 \sqrt{3} \begin{pmatrix}
    0 & 0 & 0 \\
    \langle O, v(-1) \rangle & \langle O, v(0) \rangle & \langle O, v(1) \rangle \\
    0 & 0 & 0
    \end{pmatrix}$
    
This means that the Fourier decomposition does not bring more information than the irreducible decomposition.
Indeed, we can read the components of the irreducible decomposition of $O$ in its Fourier transform.
Do note that the Fourier component of $\mathcal{U}(1,0)$ is proportional to the irreducible component on $\mathcal{U}(-1,0)$ (note the minus sign).
In the same vein, the Fourier component on $\mathcal{V}$ has to be read from the irreducible component on $\mathcal{V'}$.

In the end, we can give the same physical interpretation to the Fourier components as we did for the irreducible components, since they are equal up to swapping and a multiplicative factor.

    Test of the formula above...
    Ok.


### 3.3 Interpretation of the Product

The operation of convolution is closely linked to Fourier analysis.
For finite groups, the convolution is given by:
$$\phi \star \psi (g) = \sum_{h\in G} \phi(h)\psi(h^{-1}g)$$

Let $O,P$ be two obervations, we can define the convolution $O' \star P'$ and pull it back to obtain an observation $O \star P$:
$$O \star P (x,t) = \sum_{h\in G} (h \cdot O)(0,0) \times (h^{-1}D^xH^t \cdot P)(0,0)$$

Which we can rewrite as:
$$O \star P (x,t) = \sum_{y,s,a\in [-1,0,1]^3} O(y+sa,s) P(x-y,t-s)$$

This formula is implemented in the function `conv` below.

For fun, we try convolving the observation of a particle at rest (mass of 1 at x=0 for all t, 0 everywhere else).
We observe that the convolution spreads the observation until the observation looks uniform.

    Testing convergence to uniform observation...
    Ok.


The Fourier transform exhibits the ring-isomorphism between the group ring $\mathbb{C}G$ (with multiplication given by convolution) and a direct product of matrix rings.

In our case, the Fourier ring is a direct product of:
* The $\mathcal{U}$ Fourier component, isomorphic to $\mathbb{C}^3$
* The $\mathcal{V}$ Fourier component, this matrix ring is made of all matrices of this type:
$$ \begin{pmatrix}
0 & 0 & 0 \\
a & b & c \\
0 & 0 & 0
\end{pmatrix}
$$
* The $\mathcal{V'}$ Fourier component, which is isomorphic to the previous one.

The Fourier isomorphism is given by the convolution product formula:
$$\widehat{O \star P} = \hat O \times \hat P$$

    Testing the Fourier product formula...
    Ok.


The product formula helps give further intuition behind the convolution operator, giving it a filter meaning.


To help understand this, let's focus on an example, convolution by $v(0)$.
Since this observation only has non-zero Fourier coefficients on $\mathcal{V'}$, the result of the convolution $v(0) \star O$ for any observation O, will also have null zero-coefficients everywhere but on $\mathcal{V'}$.
This corresponds to an observation that only have an irred. component on $\mathcal{V}$.
In that sense, the operation $v(0) \star \_$ projects an observation down to the $v(0)$ component.

We can do the same for any other irrep. basis vector!

## 4. Real Representations

We have a decompositions into irreps over $\mathbb{C}$:
$$\mathcal{F} = \mathcal{U}_{-1,0} \oplus \mathcal{U}_{0,0} \oplus \mathcal{U}_{1,0} \oplus \mathcal{V} \oplus \mathcal{V'}$$

We have $\mathcal{F}_\mathbb{R}$ the space of real valued observations and $\mathcal{F}_\mathbb{R} \otimes \mathbb{C} = \mathcal{F}$
$\mathcal{F}_\mathbb{R} \otimes \mathbb{C} = \mathcal{U}_{-1,0} \oplus \mathcal{U}_{0,0} \oplus \mathcal{U}_{1,0} \oplus \mathcal{V} \oplus \mathcal{V'}$

$\mathcal{F}_\mathbb{R} \otimes \mathbb{C} = \mathcal{U}_{0,0} \oplus (\mathcal{U}_{-1,0} \oplus \mathcal{U}_{1,0}) \oplus (\mathcal{V} \oplus \mathcal{V'})$

We use the following definition from "A First Course in Representation Theory".

__Definition__ Let V be a complex representation.
We call V _real_, if there is $V_0$ a representation on a real vector space,
such that $V = V_0 \oplus \mathbb{C}$.

__Properties__
* If $V_0$ is irreducible over $\mathbb{R}$, then $V$ is either irreducible over $\mathbb{C}$ or the direct sum of two irreducible and conjugate representations.

* $\Re(\mathcal{U_{0,0}}) = \mathbb{R} \cdot \begin{pmatrix} 1 & 1 & 1 \\ 1 & 1 & 1 \\ 1 & 1 & 1 \end{pmatrix}$
* $\Re(\mathcal{U}_{-1,0} \oplus \mathcal{U}_{1,0}) = 
\mathbb{R} \cdot \begin{pmatrix} -1/2 & -1/2 & -1/2 \\ 1 & 1 & 1 \\ -1/2 & -1/2 & -1/2\end{pmatrix} + 
\mathbb{R} \cdot \begin{pmatrix} \sqrt{3}\over2 & \sqrt{3}\over2 & \sqrt{3}\over2 \\ 1 & 1 & 1 \\ \sqrt{3}\over2 & \sqrt{3}\over2 & \sqrt{3}\over2 \end{pmatrix}
$
* $\Re(\mathcal{V} \oplus \mathcal{V'}) = 
\mathbb{R} \cdot \begin{pmatrix} 0 & 0 & 0 \\ 0 & 0 & 0 \\ -1/2 & 1 & -1/2\end{pmatrix} +
\mathbb{R} \cdot \begin{pmatrix} 0 & 0 & 0 \\ 0 & 0 & 0 \\ -\sqrt{3}\over2 & 0 & \sqrt{3}\over2\end{pmatrix} + \\
\mathbb{R} \cdot \begin{pmatrix} 0 & 0 & 0 \\ -1/2 & 1 & -1/2 \\ 0 & 0 & 0 \end{pmatrix} +
\mathbb{R} \cdot \begin{pmatrix} 0 & 0 & 0 \\ -\sqrt{3}\over2 & 0 & \sqrt{3}\over2 \\ 0 & 0 & 0\end{pmatrix} + \\
\mathbb{R} \cdot \begin{pmatrix} -1/2 & 1 & -1/2 \\ 0 & 0 & 0 \\ 0 & 0 & 0 \end{pmatrix} +
\mathbb{R} \cdot \begin{pmatrix} -\sqrt{3}\over2 & 0 & \sqrt{3}\over2 \\ 0 & 0 & 0 \\ 0 & 0 & 0 \end{pmatrix}$

For the irreducible (real) representations of the type $V_0 \otimes \mathbb{C} = W \oplus \overline W$, if we have a basis $e_1, ..., e_n$ for W, then we can denote $e_1, .., e_n, \overline{e_1}, ..., \overline{e_n}$ a basis for the direct sum.

Since the matrices of the actions for any $g\in G$ for $W$ and $\overline W$ are conjugate, we can check by direct computation that in the basis $e_1+\overline e_1, ..., e_n+\overline e_n, i\overline e_1 - ie_1, ..., i\overline e_n - ie_n$, this matrix has only real entries.

In turn this gives the real representation we were looking for!
Below, we give a function to compute the matrices for any g.
