---
layout: article
title:  "Daily Life Topology"
date: 2022-12-26 16:50:47 +0000
key: "TOPO_GENDER_20221226"
comment: false
mermaid: true
mathjax: true
mathjax_autonumber: true
categories:  language 
license: true
cover: 'assets/topology_to_gender/thames-cropped.jpg'
published: false
aside:
    toc: true
tags: ["language", "continuity", "topology"]
---

TODO (Manu):
- Add Poincarre's points in this article
- How do I organize this?
    - My main selling point is the intuitive stuff and I don't want this to be a topology course
    - So I could just write all the definitions and properties in the start and then start talking
    - It would make the structure much clearer, but maybe less appealing to readers

The article assumes basic knowledge of topology.
My angle to to view topology as a language and compare it with English.
We'll see the differences and some applications to gender identity at the end.
My goal would be to give some intuition/motivation for a new student that just started learning topology.

I'm currently reading John Lee's "Introduction to Topological Manifolds".
It's mostly a book about the mathematical language of topology.

Mathematicians say that Topology is the language of continuity.
Another way to say it would be to say it's the language of oneness,
of things that cannot be broken up cleanly into pieces.

<div style="width:80%; margin:0 auto;" align="center" markdown="1">
![Still water image]({{ '/assets/topology_to_gender/thames.jpg' | relative_url }})
_Copyright: (c) Roni Horn / Photo (c) Tate_
</div>


Can you describe this photo with words?
It's difficult because words are goods when situations are easy to break into pieces.
When you can say "there was that guy there, a chair here, a big table in the middle".
This wave picture is not easy to break into parts, that's why we use a different language althogether.

__Definition__ 
A __topological space__ is a space W and a collection $\tau$ of subsets of W that we call "open",
that verify a couple of conditions:
* $\emptyset, X \in \tau$
* Let $U,V \in \tau$, then $U \cap V \in \tau$
* Let $(U\_i)\_{i \in I}$ be an arbitrary collection of sets in $\tau$, then $\bigcup_{i\in I}U_i \in \tau$


You can think of open sets as "valid ways to cut up the space".

The axioms of topology are all about how you can break a space up in pieces.

## Being One in Mathematics: Connectedness

For example, we have this definition of what it means to be "in one piece".

A space is __connected__ if whenever $W = U \cup V$,
with U, V open and non-empty, then $U \cap V$ is not empty.
Said differently, it's impossible to cut a connected space into two pieces without having some form of overlap.

A space W is __disconnected__ if you can find 2 open and non-empty sets U, V, 
that do not intersect, such that $W = U \cup V$.

TODO (Manu):
- Probably need a proof that the line and/or the circle are connected -> Wait for chapter 4

Note that with these definitions we can give a couple of interesting examples:
- Any space X with the _indiscrete topology_ (the topology that only contains $\emptyset$ and X) is connected
- The space $\\{0, 1, 2\\}$, with the topology $\\{\emptyset, \\{0\\}, \\{0, 1\\}, \\{0,1,2\\}\\}$ is connected.

The last example seems a bit weird.
How can a set made of 3 distinct parts be connected?
As we can see, it is related to the fact that the topology is really coarse.
All the open-sets are included inside each other.

We can see that this topology is not so interesting from the point of view of 
"breaking up the space into small pieces".
Because the only way we can have a collection of open sets that cover the space
is to include the whole set.

We need to introduce another condition to make sure that our topology actually
is able to break up the space.

__Definition__ A topological space W is said to be __Hausdorff__,
if whenever we take two distinct points
we can find an open set that contains one but not the other.

__Exercise__: Show that a finite Hausdorff space has the discrete topology (every subset is open).
Conclude that a finite Hausforff space that has more than 1 point is disconnected.

So we reach the conclusion that a connected Hausdorff space has to be infinite,
which does match the intuition.

## Why Can We Take Arbitrary Unions But Only Finite Intersections of Open Sets?

The answer appears clearer when looking at Hausdorff spaces.
Suppose X is an Hausdorff space with a topology such that any intersection of open sets is open.
Then it is easy to show that every singleton set is open, and so that the space is disconnected.
This is obviously a bad thing since our goal is to describe connected things.


## Boundaries

Let me take the WORLD ITSELF as an example of the topological space.
Whenever you have a concept, or a word, it works as a kind of cut in the world.
For example, this cat called Pedro splits life into things that are Pedro and things that are not Pedro.
In that sense, Pedro, or any concept, is an open set.

<div style="width:80%; margin:0 auto;" align="center" markdown="1">
![Pedro]({{ '/assets/topology_to_gender/blackcat.jpg' | relative_url }})

_Pedro the cat_
</div>

The question is now, is life connected or disconnected?
When we talk casually, it seems that things are either inside Pedro or outside Pedro,
and that this union covers everything.
If that was the case, it would make our world disconnected according to the definition above.

However, more careful investigation shows that there is actually a boundary between Pedro and "outside Pedro" which is neither.
For example, if Pedro is drinking, at which instant do you consider that the water is part of Pedro?
When the water is in the bowl, it is definitely "not Pedro", after he's done drinking, it is definitely "part of Pedro".
At which instant exactly does it switch?
When it is inside the mouth, the throat or the stomach?
It doesn't matter exactly where you place that boundary, the point is that there is one.

Let's imagine you define the moment where the water becomes part of Pedro as the instant the water molecule touches his tongue.
Note that atoms never really touch, but they do get close enough.
You could choose a small threshold, say $\epsilon$, and say that the water is in Pedro when it gets within that distance of a previously declared "Pedro atom".
That's weird and annoying to do but it also create this annoying phenomenon, what about when the molecule is exactly at epsilon from the closest atom?
Then it is neither Pedro nor "not Pedro", it is on the boundary.

Of course, this boundary is infinitely thin, that's why we usually don't think about it.

You also exists as a concept.
Think about when you're sweating, at which instant exactly does the water molecule passes from inside to outside?
Same thing, you can define that how you want, there will always be a boundary where it's either both you and not you, or neither.
You CAN make the boundary as thin as you want, but you cannot make it disappear.
This is just the expression that this world is connected.


You might think that the boundary is not important because it is so thin.
Important life activities such as birth, death, eating, drinking, breathing, shitting, pissing, sweating, sex all include a modification of the boundary.
We cannot live without the connection to the outside world, without the boundary.

In the next few lines, we give the mathematical definition of a boundary.
We'll need to define the interior first.

__Definition__ Interior.  
Let A be an arbitrary subset of W (not necessarily open).
We define $\text{Int}A = \bigcup_{U\in\tau, U \subset A}U$

The _interior_ of A is the biggest open subset contained in A.

We are now ready to define the boundary:

__Definition__ Boundary.  
Let A be an arbitrary subset of W.
We define $\delta A = W \setminus (\text{Int}A \cup \text{Int}(A^c))$

__Exercise__: Show that if W is connected, then every non-empty set except W itself has a non-empty boundary.

Finally, we have the following decomposition:
$A = \text{Int}A \cup \text{Int}(A^c) \cup \delta A$

Where, for a connected set, the boundary is never empty and is never open.

I believe this shows that English (or French or other languages) provides a discrete model of the world
in which boundaries are forgotten.
This corresponds to approximating the world with a totally disconnected space.
It works relatively well in practice but we do have issues that I'll discuss in the last part.

But first let's talk more about how we could describe something that is one (connected) in a better way.

## Breaking Up Observations


Let's say we want to describe a topological space representing the wave, we'll call W.
We can do that using a function $f: W \rightarrow S$
Where the set S can be whaterver you want,
we can say shades of color or water agitation if we're still trying to describe the wave example.
The set S will also required to be a topological space.

We say that the __function is continuous__ if $f^{-1}(U)$ is open in W for all U open in S.

Allright so the key thing that these definitions allow is this:

Suppose U, V are open and $U \cup V = W$, suppose we have $f_U: U \rightarrow S$ a continuous function on U,
and $f_V$ a continuous function on V.
Suppose also that $f_U$ and $f_V$ agree on $U \cap V$, then there is a unique continuous function f on W that restricts to $f_U$ on U and to $f_V$ on V.

__Exercise__ Prove this.

Let's say "f" is your description, what this theorem allows you to do is construct it from 2 smaller descriptions, one on U and one on V.

This is interesting because when we think naively of what is oneness, it seems impossible to describe it.
How could we describe something that is not splittable?
It seems to escape description, to be transcental almost.

This is where the language of topology actually teaches us something.
It is possible to talk about something that is one.
You CAN break up your observation of oneness into multiple smaller observations.
However your cuts of the oneness will have to overlap, and your smaller observations will have to agree on the overlaps.

This is a much more subtle vision of what it means to be "one".
Unity doesn't mean unbreakable, it means everytime you break it you have an overlap or a boundary.

Note that English, doesn't have this subtetly.
Words in English have an outside and an inside and the boundary is forgotten.
In the next section we'll see the issues with this feature.

## What Is A Woman ?

[What is a woman, Shapiro](https://www.youtube.com/watch?v=QZ9MM8CSZD8)

Recently there has been debate over whether trans-women should be allowed in women competition.
This was linked to the question of "what is a woman?"

The first thing to notice is that the word woman is a concept,
we may change what we define is inside or outside this concept,
but it does little in itself to alter the life of the real human being that are being tagged by it, and are beyond it.

The concept of "a woman" like all concepts splits life into two parts: "woman" and "not woman".
As with everything else, this split cannot be clean, there must be a boundary or some overlap.
There must be people which are neither, or both.
This has nothing to do with the people in question, it has way more to do with how we choose our concept.
Whichever label is assigned or not to someone, all should be respected of course!

Taking a step back, and looking at the identity and gender debate.
A lot of importance as been taken to identity definitions, as can be seen from the LGBTQIA+ acronym.
It IS very important to give reconaissance to people from minority groups and adding more precise labels do help.

But at a fundamental level, it doesn't matter how many labels you have,
there will always be people at the boundary, because this world is one.

Taking examples from topological open sets, why not allow labels to intersect?
Why couldn't one be BOTH a man and a woman for example?
