---
layout: article
title:  "1D Spacetime: Living On a Line"
date: 2021-11-02 16:50:47 +0000
key: "1D_SPACETIME_20220906"
comment: false
mermaid: true
mathjax: true
mathjax_autonumber: true
categories: mathematics 
license: true
cover: 'assets/galilean_1p1/one-line-girl.svg'
aside:
    toc: true
tags: ["group theory", mathematics, "galilean group"]
---

We explore a simplified version of spacetime with only 1 space dimension.


## Mathematics of Spacetime in 1+1 Dimensions

In 1+1 spacetime we consider only the following 3 motions:
* d -> displacement one unit in space (instantly)
* h -> displacement one unit in time
* b -> acceleration of one unit of speed (instantly)

These motions respect the following 3 identites:
* Moving then waiting is the same as waiting then moving: $$dh=hd$$
    <video src="{{ 'assets/galilean_1p1/MoveAndWait.mp4' | relative_url }}" controls autoplay loop style="max-width: 75%;"  >
      Your browser does not support the <code>video</code> element.
    </video>
* Moving then accelerating is the same as accelerating then moving: $$db=bd$$
    <video src="{{ 'assets/galilean_1p1/MoveAndBoost.mp4' | relative_url }}" controls autoplay loop style="max-width: 75%;"  >
      Your browser does not support the <code>video</code> element.
    </video>
* Accelerating then waiting gets you further than waiting then accelerating: $$hb=dbh$$
    <video src="{{ 'assets/galilean_1p1/WaitAndBoost.mp4' | relative_url }}" controls autoplay loop style="max-width: 75%;"  >
      Your browser does not support the <code>video</code> element.
    </video>

These 3 motions with their 3 "commutation relations" are enough to fully describe the group.
The details are developped in the 
[Galilean 1+1 notebook]({% post_url 2022-09-16-galilean-1p1-notebook %}).

This
[notebook]({% post_url 2022-09-21-galilean-2D-lie-algebra %})
dwelves into the infinitesimal aspect of the group with a study of its lie algebra.

## Living on A Finite Line

Here we make the Galilean group (artificially) finite by imposing the relations $B^3=D^3=H^3=e$.
It gives a 27-element finite group which has the same commutation relationships as the Galilean group.


In the 
[Finite Galilean notebook]({% post_url 2022-09-20-finite-gal-1p1-notebook %})
we explore topics of representation theory, non-commutative harmonic analysis, character theory,
complex and real irreps, all in the finite context.


