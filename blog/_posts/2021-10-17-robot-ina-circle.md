---
layout: article
title:  "Robot Tales: Space Is Not Outside"
date: 2021-10-17 16:50:47 
key: "ROBOT_CIRCLE_20211017"
comment: true
mermaid: true
mathjax: true
mathjax_autonumber: true
categories: mathematics spirituality
license: true
aside:
    toc: true
tags: space mathematics intuition awareness
cover: 'assets/robot_ina_circle/generator-dances.svg'

---


What is space? 
Is it exterior or interior to us?
Let's challenge the common sense answer that space is something that contains us.
We'll use a toy model of a small robot, which lives in a 4 points space,
to explore those questions in a simplified setting.
As we explain what space means to the "robotself" &#x1F916;, we also learn about our spacetime.


Prerequisites: I will assume the reader has a basic understanding of complex numbers and math proofs.
I will note N for the natural numbers and R for the reals.
{:.warning}


In my 
[very first article](/spacetime/2021/01/07/introducing-the-app.html), 
we showed that spacetime is something we can experience.
As a quick reminder, we showed that there is a "square dance" that you can do in our spacetime,
but that you cannot do in some others.
So there is _some_ aspect of spacetime that can be "touched and felt" with our 5 senses.

As we've seen, this little square dance has made space a little bit "interior" to us, but not fully.
The fact that the square dance works is a property of our spacetime, but it is also a property of many others.
So if you just notice that the square dance work, it is not enough to know that we live in our 4D space.
You might be living in a 2D flat space for example, and that dance would work as well.

So the idea here is to push this dance thing further.
What if we had more dances, so that they could cover all the properties of our spacetime.
If you did all the dances, one after the other, and checked that they worked, then you had to be inside our spacetime.
This would allow yourself to have a full appreciation of our spacetime, purely in terms of things you can see and touch.

Imagine for a second that it is possible:
you have a set of dances that allows you to recognize your spacetime "from the inside".
Well then, do you even need anything else than the dances?
You could define space to be the dances, and you should get a coherent result, because the danses capture fully the nature of the space.
In doing so, you would have reached a definition of space that is interior to your perception. &#x1F483;

This is the overall goal of this blog, but we are not yet ready to give the dancy description of our spacetime.
We need to learn more tools and techniques first.
So here we will consider a very simple robot, which lives in a very simple space,
and we will give the dancy description of its simple space.
More than this, we will prove formally that the dancy description is equivalent to the usual (exterior) description.
This will form a basis for later when we attempt the same for our spacetime.


&#x27A1; The overall progression of this article will be to start from the exterior/usual point of view,
and progressively move toward the interior/intuitive description.

We will start by introducing the robot and give a first equation (1) which expresses
the existence of the robot in this space, from our point of view.  

In section II, we will discuss more about the nature of space and of our perceptions.
We will try to give a new perspective of what makes space space, both for the robot and for us humans.

In section III we move forward by giving an equivalent definition of space (2)
which is interior to the point of view of the robot.
While this expression is interior, it relies on complex number, so it is not intuitive.

In section IV, we bring the dances in, and they allows us to find a condition (3), which is equivalent to (2),
but doesn't use any algebra.

When we reach there, we'll have provided our little robot with a definition of its space that is both intuitive and interior to its perception.
&#x270C;

<div class="item">
  <div class="item__image">
	<a href="{{ 'dist/' | relative_url }}">

    <img class="image image--md" src="{{ 'assets/robot_ina_circle/generator-dances.svg' | relative_url }}" />
	</a>
  </div>
  <div class="item__content">

  Here is a quick diagram of the 4 dances that "generate" spacetime for our robot.
  Each arrow represents one movement and each color represents one dance.
  By the end of this article, we'll understand what this picture mean in more details.
  </div>
</div>



Anyways, let's introduce our robot.

## I. Hello robot!

Imagine we have a very small and very simple robot, made of a single rotating arm fixed at the origin O.
The robot has a single motor it can control, which can rotate the arm either 90 degree or -90 degree or do nothing.
Below we have a simple diagram of the situation.


<div style="width:80%; margin:0 auto;" align="center" markdown="1">
![diagram]({{ '/assets/robot_ina_circle/robot-ina-circle.svg' | relative_url }})
</div>

The robot can only go to one of the 4 pointed positions on the circle.
We identify each of these positions with a complex number: $\\{1, i, -1, -i\\}$.
Since these 4 positions make up everything this robot can go, it is its World and we'll note $W = \\{1, i, -1, -i\\}$.

The robot has also a single temperature sensor, which reads a number.
At each position there is a different temperature: for example at the top (noted i) it is red which suggests a high temperature, let's say 100 degrees C to be factual.
We also see that the temperature at -1 is medium (perhaps 30), at -i cold (say -10) and at 1, where the robot currently is, we say it's chilly at 15.

The temperature doesn't change with time, so that we have a single temperature reading for each location.
This allows us to define a temperature function: $\tilde t: W \rightarrow R$.
So we can express the temperature at the top by saying $\tilde t(i) = 100$.  
Similarly:
$$\tilde t(-1) = 30 \\
\tilde t(-i) = -10  \\
\tilde t(1) = 15  $$

The function $\tilde t$ is kind of the description of the robot's world.
Remember that temperature is the only sensor available to our robot, 
so its whole world is made of temperature.
The function $\tilde t$ summarizes all the temperatures that it can possibly feel.
It is a kind of ultra-condensed sensory information for the robot.

Right now we don't assume anything about the function $\tilde t$,
but latter in the article we might need to ask it to be injective.
This means that all points have different temperatures,
which allows the robot to actually distinguish the points from their temperature.

&#x231A; Ok, now we need to talk a little about __time__  and __movement__.
How does this little robot moves about?
We are going for as simple a model as possible: so we will only consider discrete time steps.

The time will be modelled by the integers N : 0, 1, 2 ...
At each instant n, the robot has a position L(n) which is in W.
For example, if the robot starts at 1 we have L(0) = 1.
This gives rise to a function $L: N \rightarrow W$  


<div style="width:80%; margin:0 auto;" align="center" markdown="1">
![diagram]({{ '/assets/robot_ina_circle/exterior-POV-eq.svg' | relative_url }})
</div>

Allright so at this point we have everything needed to completely understand what is happening for our robot.
The function L and $\tilde t$ allow us to fully describe the space, as well as what the robot feels in the space.
We call these two function the __exterior point of view__ because they correspond to how an external observer sees the robot.
We are going to continue this section, by introducing two new functions t and b, 
which describe the robot from its own point of view (POV), using its own sensors.

The robot has a "motor command" b (I chose 'b' because it looks like a flexed triceps muscle &#x1F4AA;), which gives at each instant in which direction the robot wants to move.
At any time n, the robot can either stay in place b(n) = 1, rotate clockwise b(n) = -i or rotate counter-clockwise b(n) = i.
And this gives rise to a function $b: N \rightarrow  \\{1, i, -i\\}$

Why chose {1,i,-i} as the output?
We need to remember the geometrical property of complex multiplication.
Multiplying a complex number by i rotates counter-clockwise, by -i clockwise and by 1 doesn't change anything.
We can translate this as an equation: L(n+1) = L(n)*b(n)


<div style="width:80%; margin:0 auto;" align="center" markdown="1">
![diagram]({{ '/assets/robot_ina_circle/clockwise-rotation.svg' | relative_url }})
</div>

For example in the diagram above, we have L(0) = 1, and suppose the robots wants to rotate clockwise, that is b(0) = -i.
Then we have L(1) = L(0)b(0) = -i, and indeed this corresponds to a clockwise rotation.

Even though we have introduced b as a new function, it doesn't really bring any new information,
as the recurrence relation for L completely determines it: $b(n) = {L(n+1) \over L(n)}$

&#x1F525; Now that we have movement from the robot POV, we need temperature.
What does the robot feels at time n?
Well at time n, the robot is at L(n) and the temperature there is given by $\tilde t(L(n))$.
This gives us a new function t: N -> R given by $t(n) = \tilde t(L(n))$

The functions t and b, put together, encompass the totality of what the robot perceives.
We call (t,b) a __robot's awareness__, and they form the interior point of view.

<div style="width:80%; margin:0 auto;" align="center" markdown="1">
![diagram]({{ '/assets/robot_ina_circle/robot-ina-circle-awareness-eq.png' | relative_url }})
</div>

Having defined the robot both from the exterior and the interior, 
we now say a few word about the relationship between these two views.

The robot awareness represents the same information as the exterior POV but from the POV of the robot.
It is easy to go from the exterior POV $(\tilde t, L)$ to the interior POV (t, b):

$$
\left \{ \begin{eqnarray}
t(n) = \tilde t(L(n)) \\
b(n) = {L(n+1) \over L(n)}
\end{eqnarray} \right.
$$

We can also express the first equation with the diagram below:

<div style="width:100%; margin:0 auto;" align="center" markdown="1">
![diagram]({{ '/assets/robot_ina_circle/factor-diagram.svg' | relative_url }})
</div>

This diagram says essentially that there are two equivalent ways to know what the robot is perceiving at time n:
1. Ask the robot (or read its sensors), which gives us t(n)
2. Deduce the temperature the robot is feeling from the location of the robot $L(n)$ and the temperature there $\tilde t(L(n))$  

__Condition (1) - Robot exists in space__ If the robot really lives in the space W, the two methods must give the same results, so that 
$$
\begin{equation}
t(n) = \tilde t(L(n))
\end{equation}
$$

We will note this expression (1).
This expression is important because it links the outside and inside point of view.
There is the "outside", which is present as the world W as the output of the function L.
There is the "inside" which is represented by the touch function t.

In the next part we will see that this condition implies that the functions t and b cannot be taken randomly,
that they must verify some kind of relationship.
We will suggest that we can use this relationship for a definition of space.

## II. Space as a pattern

In the previous part, we defined the robot from our point of view,
and we showed how to transpose from the exterior POV to the interior POV.
Here we'll try to do the opposite, start from interior POV and try to recover the exterior.
In doing so, we'll hit a difficulty in that we cannot chose the functions t and b randomly.
There has to be some kind of relationship between t and b, this is both the secret meaning behind condition (1)
and behind the meaning of space itself.



So let's do a kind of empathy exercice and put ourselves in the robot gears.
The robot doesn't know about the space W, because it has no sensor that can cover the whole area.
Actually, the __robot awareness__ &#x1F440; has only two things: the temperature at each time and movement at each time.

<div style="width:100%; margin:0 auto;" align="center" markdown="1">
![diagram]({{ '/assets/robot_ina_circle/robot-ina-circle-awareness-eq.png' | relative_url }})
</div>



At this point we just imagine the two functions as kind of independant inputs.


The first thing we can tell the robot about, is the position function L.
Having a position function is a good start to talk about space, no?
Using the initial position L(0) = 1 and the recurrence relation L(n+1)=L(n)b(n), we get the formula:
L(n+1)=b(0)b(1)...b(n)

That is L(n+1) is the (complex) multiplication of all movements that happened strictly before.
It is easy to see that L is a function N -> W, as before.
So now we got W, our space, in the picture.

Let's describe what is happening in our space, that is, what is the temperature at every point.
For this, we can select 4 integers $n_1, n_i, n_{-1}, n_{-i}$ such that:  
$$L(n_1) = 1 \\ 
L(n_i) = i \\  
L(n_{-1}) = -1 \\  
L(n_{-i}) = -i$$
In more casual language, $n_i$ is a time when the robot is at i.

Now we can define a function $\tilde t: W -> R$ as before using these integers:  
$$\tilde t(1) = t(n_1)  \\
\tilde t(i) = t(n_i)  \\
\tilde t(-1) = t(n_{-1})   \\
\tilde t(-i) = t(n_{-i})  $$

Allright so our robot knows its position L and knows what the space feels like (in terms of temperature) through $\tilde t$.
We have defined all of this using only things he "sees and touch", that is from t and b.
So is that it? Are we done? Have we defined space from the interior point of view?


&#x26A0; Not yet, there is still a last condition, which is the equation we wrote (1) in the previous section.
As a reminder, this condition reads: $t(n) = \tilde t(L(n))$  

We can see that at this point, we don't know whether this relationship holds or not.

To see why this condition is important, let's take two random functions t and b:

| time 	| 0  	| 1   	| 2  	| 3  	| 4   	| 5  	| ... 	|
|------	|----	|-----	|----	|----	|-----	|----	|-----	|
| t    	| 15 	| -10 	| 25 	| 45 	| 100 	| 15 	| ... 	|
| b    	| 1  	| 1   	| 1  	| i  	| -i  	| i  	| ... 	|

We can construct a function L from b as suggested above:

| time 	| 0  	| 1   	| 2  	| 3  	| 4   	| 5  	| ... 	|
|------	|----	|-----	|----	|----	|-----	|----	|-----	|
| t    	| 15 	| -10 	| 25 	| 45 	| 100 	| 15 	| ... 	|
| b    	| 1  	| 1   	| 1  	| i  	| -i  	| i  	| ... 	|
| L    	| 1  	| 1   	| 1  	| 1  	| i   	| 1  	| ... 	|

Now according to condition (1), we must have:
$15 = t(0) = \tilde t(L(0)) = \tilde t(1)$ and $-10 = t(1) = \tilde t(L(1)) = \tilde t(1)$.
That is we must have $15=\tilde t(1)=-10$, which is absurd of course.

So the functions t and b that we had chosen just above do not work.
Although they can be used to define L, W and $\tilde t$, they do not verify (1), and so
do not form the awareness of a robot in space.

If we look closely at what happens, we see that the problem is that the robot is staying at the same place:
b(0)=1, so since we supposed that the temperature does not change with time, it should experience the same temperature.
That is we should have $t(0)=t(1)$, but our function t varies at these instants...
The problem is that the function t changes while the robot is static.

The following exercise makes this intuition formal:

__Exercise__:
Show that if b(n) = 1, and $t(n) \neq t(n+1)$, then condition (1) is not verified.

Of course this exercise just shows one way in which condition (1) can fail.
There are many other ways that two function t and b may fail the test.
We give another example below:

__Exercise__:
Suppose that $b(n)=b(n+1)=b(n+2)=b(n+3)=i$, and $t(n) \neq t(n+4)$, then condition (1) is not verified.

This exercise shows that if there are 4 i's in a row, then t must have the same value before and after the 4 i's.
This is great, in that it gives us one more condition on the two functions t and b, but it is not enough.
Because we could still come up with functions t and b that verify the conditions of the two exercises, and
still they would fail to verify condition (1).

There is still something we can say at this point.
The patterns that these exercises describe mix the values of the function t and b.
When the function b goes through some special chain of values, e.g. i,i,i,i,
then the values of t must be as specified.
So we can see that there must be a special pattern that "weaves together" the functions b and t.

This pattern is what makes space space.

In the next chapter, we are going to find other ways of expressing (1),
so that the pattern it entails becomes more clear and more intuitive.

Often enough in this article I will need to consider different robots in different circumstances.
When I mention a robot's awareness, I am always referring to two functions t:N->R and b:N->{1,i,-i}.
We can also consider L and $\tilde t$, they will be the function defined as we just did above.
Sometimes we will require the robot awareness to actually come from a robot in space,
in which case we might say that the robot's awareness verifies condition (1),
but most of the time we might instead say that "the robot exists in space" to mean the same thing.


&#x1F64B; What about us humans?  
We tend to think of our sensations as independant streams of information about the world.
But just like the robot, they are actually linked together into one very specific mess.
For a simple example,
we'll focus here on 3 senses: muscular sensations at the neck, sight and balance (through the inner ear).


<div class="item">
  <div class="item__image">
	<a href="{{ 'dist/' | relative_url }}">

    <img class="image image--md" src="{{ 'assets/robot_ina_circle/sensation-naive.svg' | relative_url }}" />
	</a>
  </div>
  <div class="item__content">

	Here we have a naive picture of what our senses input look like.
    Each sensory signal is doing its own thing, with no connection to the other ones.
  </div>
</div>


But this naive picture is grossly innacurate, our senses are not independant at all.
Let's consider the following example:
1. Start facing straight, you see &#x1F40D;
2. Turn your head to the right  
    This creates specific neck muscle sensations.
3. Notice the feeling of turning &#x1F501;  
	This is coming from your internal ear.
    The internal ear is an organ that feels movement, whether rotational or translational.
4. You now see something else &#x1F6AA;
5. Turn your head to the left  
    This creates again specific neck muscle sensations
6. Notice feeling of turning &#x1F504;
7. Your head is back where it started: you see again &#x1F40D;

This little example of turning the head left and right might seem a bit trivial, but it shows a very important point.
Everytime we do these small steps, we go through the same sensations.
Everytime I move my head, I feel like I am turning, and my visual sensations change.
It is not possible to turn my head, and not feel like I am turning.
It is not possible to turn my head, and not change my sight (assuming the eyes are not moving).

The point here is that, of course, our senses are not independant.
There is also something much stronger that comes off this example.
_Everytime_ the same neck muscular sensations occurs, it produces _exactly the same_ change in sight
and change in inner ear.
So not only our senses are woven together in some way,
but the weaving pattern repeats exactly.

Now moving the head left than right is not the only example of movements that create specific change in sensations.
There is a large number of them, each with its own specific pattern in the sensation space.
Each repeating perfectly in time.
Take all these patterns together, and you get a huge and beautiful tapistry in our sensation space.
In a sense, we will see that the weaving pattern fully describes spacetime (if you want you may say that spacetime _is_ the weaving pattern).

We are currently unable to prove this claim for humans, so you might sneer if you think it foolish.
What we will do though, is prove this claim for our little robot.
We will show that, for our robot, a specific pattern in its sensations is equivalent to the existence of space.

<div class="item">
  <div class="item__image">
	<a href="{{ 'dist/' | relative_url }}">

    <img class="image image--sm" src="{{ 'assets/robot_ina_circle/sensation-weave.svg' | relative_url }}" />
	</a>
  </div>
  <div class="item__content">

	Here we have an updated picture of what our senses input looks like.
    The streams from each sensory organ are weaved in a regular pattern to represent their relationship.

  </div>
</div>



## III. Internal definition


Since (1) seems to capture the essence of what a space means, let's try to look at it a little bit more.
We are going to find different, though equivalent, ways of expressing (1), and try to extract insight from them.

__Interior expression of space__: 
Let's consider a robot's awareness (t, b) and construct functions L and $\tilde t$ from t, b as in the previous section.
The following two conditions are equivalent:

1. (robot exists in space) forall n in N, $t(n) = \tilde t(L(n))$
2. Forall n,k in N, $b(n)b(n+1)...b(n+k-1)=1 \implies t(n) = t(n+k)$

__Proof__:  
> First we are going to need a small lemma:  
The condition $t(n)=\tilde t(L(n))$, forall n, is equivalent to the condition:  
Forall n,m, L(n)=L(m) implies t(n)=t(m)
>
Indeed if $L(n)=L(m)$, then applying $\tilde t$ on both side we get: $t(n)=\tilde t(L(n))=\tilde t(L(m))=t(m)$.  
>
The other side of the lemma is a bit more spicy:
Let's remember $n_i$ that we chose to construct $\tilde t$ back in the previous section.
It verifies $L(n_i)=i$ and by construction of $\tilde t$, $\tilde t(i) = t(n_i)$.
Suppose $L(n)=i=L(n_i)$ then we have by assumption $t(n)=t(n_i)=\tilde t(i)=\tilde t(L(n))$  
In this way we have prove the equation $t(n)=\tilde t(L(n))$ for all n such that $L(n)=i$.
We can reproduce the argument with $n_1, n_{-i}, n_{-1}$ and that proves the lemma.
>
We can now use our lemma to prove our assertion:  
(1) <=> forall n,m, L(n) = L(m) implies t(n) = t(m).  
Unfolding the definition of L, we have  
(1) <=> forall n,m, b(0)b(1)...b(n-1) = b(0)b(1)...b(m-1) implies t(n) = t(m)  
Without loss of generality we can assume m is >= n and write m=n+k for k in N.  
So now we have:  
(1) <=> forall n,k, b(0)b(1)..b(n-1) = b(0)...b(n+k-1) implies t(n) = t(n+k)  
Simplifying through by the left hand side:  
(1) <=> forall n,k 1 = b(n)b(n+1)...b(n+k-1) implies t(n) = t(n+k)  
Finally,  
(1) <=> (2)  
QED.

Allright so the proof we have here is not so difficult,
but what is its relevance?
In the previous section, we discussed that the robot's senses t and b where meshed together in a complex pattern,
that was given by relation (1).
Since relation (1) used the exterior POV $(L, \tilde t)$ it wasn't very satisfying.
Having proved that (1) is equivalent to (2), we can now use (2) for describing this pattern.
This is better because (2) only refers to (t, b).

We can use (2) to understand the pattern a bit better.
First of all, it allows us to solve the previous 2 exercises easily:
1. If $b(n)=1$, condition (2) directly implies $t(n)=t(n+1)$
2. If $b(n)=b(n+1)=b(n+2)=b(n+3)=i$ then definitely $b(n)b(n+1)b(n+2)b(n+3)=1$ and so $t(n)=t(n+4)$

These two exercises come in the form of "sensation loops".
The equation $t(n)=t(n+4)$ just says that the touch sensation at n is the same as at time n+4.
From the POV of the robot, its touch sensations are looping after these 4 steps.
For the first exercise, the equation is $t(n)=t(n+1)$, which means that the touch sensation loop after 1 step.

Condition (2) is also expressed in terms of loops.
It says that if $b(n)b(n+1)...b(n+k-1)=1$ then the touch sensation $t(n)$ loops after k steps.

&#x1F476; 
It is actually nice that the expression (2) uses loops, because it is extremely intuitive.
A sensation coming back to what is was is something very primal, basic, that I argue a baby would readily understands.
Imagine a baby that is just born, and let's assume that she doesn't understand anything.
She doesn't understand where she is, how to move her body.
She might not even have a clear idea of what is her own body and whether it is separated from her mom's body.
Finally, she probably doesn't know she is a baby.
All she has for sure, are her senses.

Now let's suppose she notices a nice sensation.
For example, if she sees her mom, she will get some pleasure.
Now it is possible to lose this pleasurable sensation.
For example, if she accidentally turns her head away to the right.

Now if she has explored her sensations a bit, 
she will know that by turning her head back left, it will restore the pleasurable mom sensation.
This is obviously of interest to her.

Notice that it does take the form of a loop in sensations.
The nice mom sensations, the muscular sensations at the neck, and at the end the nice mom sensation again.
We have here a loop that is not so different from the loops defined in the exercises, or in condition (2).

It is not hard to imagine a baby being curious about learning as much loops as possible.

This is really cool because we have basically proved that the existence of space is equivalent to the presence of certain loops.
Since sensations loops are very intuitive, it is a good sign that we are slowly moving in the right direction.

&#x1F504; Let's find more loops for our robot!


We can use condition (2) to notice even more loops in the robot's awareness:
1. if the robot goes $b(n)=-b(n+1)=b(n+2)=-b(n+3)=i$, then $t(n)=t(n+4)$
2. if the robot goes -i, -i, -i, -i then touch sensation loops after 4 steps
3. if the robot goes i,i,-i,-i then touch sensation loops after 4 steps
4. ...


__Exercise__ Find 3 more little loops in the robot's awareness.

Basically, we can take any chain of movements that multiplies out to 1, and we get some little loop in the robot's awareness!

Since there is an infinity of chains of complex numbers that multiply to one,
there are also an infinity of those little loops.
This makes understanding the total structure of the relationship between t and b a bit tedious.
But as we'll see in the next section, there are clever ways to build all the loops from a few basic ones.

This concept of loops is pretty cool, because it talks only about what the robot feels and touches -- it is interior.
But let's not forget that it is fully equivalent to (1), the exterior definition of space.
So in a sense, we can _define_ space as the set of all the loops.


As a finishing touch on the form of (2) we note that time is very present, in the form of chains.
This is interesting because we often think of space as something that we can see "instantly".
But it seems that what matters is not the instant vision of space, but rather how those perception evolve and change with movement/muscular sensations.
This suggests that movement is central to the perception of space. &#x1F483;


In the next section, we are going to find a more intuitive way to describe all these loops.


## IV. Intuitive definition


Is the formula (2) really a good way to explain the space W to the robot ?
Thinking about it, in order to use (2), the robot must know how to multiply his "muscular sensations" inside W.
This is a lot to ask from a robot if you ask me.
We'll have to do better than this.

We are going to find a way to explain space to the robot that doesn't rely on complex numbers.
We are not going to use any other algebra or abstract mathematical tool either.
We will only use the robots raw sensations and the properties of some particular chain of sensations we'll call 'null muscular chains.'






Let $[b_1, ..., b_k]$ be a list of consecutive sensations.
We call $[b_1, ..., b_k]$ a __null muscular chain__ if, whenever the robot goes through these movements, the sensations at the start and at the end are the same.
Writing this in symbolic notation:  
$[b_1, ..., b_k]$ is null iff forall n such that $b(n) = b_1, b(n+1)=b_2, ..., b(n+k-1)=b_k$, we have t(n) = t(n+k).


&#x1F504; In other terms, _null muscular chains_ are just what we were calling informally loops just before!

It is important to note at this point that the definition _only relies on the raw awareness_ &#x1F440; and doesn't assume any algebraic structure on the muscular sensations (they don't need to be complex numbers, in particular).
We could use the same definition for muscular chains in our human awareness.

Why are we interested in those chains?
Well, with this concept, we can reformulate condition (2) in a more concise way:  
"a robot exists in the space W iff all chains that multiply out to 1 are null chains".

&#x1F913; Actually, it turns out that there are no other null chains than the one that multiplies to 1, as the following results makes clear:

__Null chains multiply to 1 under (1)__: let t: N-> R, b: N -> {1, i, -i} be two functions that make up a robot's awareness.
Suppose additionally that the robot "exists in the space W", that is we can define L and $\tilde t$ and they verify (1).
Assume furthermore that $\tilde t$ is injective (that is all the temperatures on the space are distinct).
This last condition is necessary so that the robot can distinguish all the points in the space using only his temperature sensor, which is his only window into the world.
Then a muscular chain is null if and only if its product is equal to 1.

__Proof__
> Let $[b_1, ..., b_k]$ be a muscular chain. 
Let n a time such that $b(n)=b_1, b(n+1)=b_2, ..., b(n+k-1)=b_k$.
>
Let's prove the <= part of the equivalence first.
Suppose that  $b_1...b_k=1$ then, because the robot verifies (1) and (equivalently) (2), we immediately get that t(n)=t(n+k).
That is the chain is null. 
>
Let's now prove the => side.
Suppose now that the chain is null.
By definition, this gives us t(n) = t(n+k).
Since the robot verifies condition (1), we can write this as: $\tilde t(L(n)) = \tilde t(L(n+k))$.
Since $\tilde t$ is injective, we have L(n) = L(n+k).
Expanding each side in terms of L(0) and b(0), ..., b(n+k-1), we reach:  
L(0)b(0)b(1)...b(n-1) = L(0)b(0)b(1)...b(n+k-1)  
Simplifying out by L(0) and the n first factors, we reach:   
1 = b(n)b(n+1)...b(n+k-1)
And so the chain multiplies out to 1.  
QED.


We are not going to actually use this result in any part of this article,
so this last proof was just some kind of sadism from my part (sorry reader).
Actually I do have a motive for this result.
In the last part I said that we can define space as the set of all the sensation loops that have the special form given by condition (2).
This was kind of cool, but not that impressive.

However, now that we know that ALL the loops have the form given by condition (2), we can give a much cooler definition of space:
space is the collection of all sensation loops.

We can see the link between sensations and algebra emerging, and that is somewhat noteworthy.
Anyway you may forget about it now because as I said we're going to use something quite different.

&#x1F4D6; If you've been following, 
you'll have noticed that in section II we introduced the notion that space is a pattern inside the robot's awareness.
In section III, we refined this idea by showing that this pattern is actually made of loops.
The current issue is that there is an infinity of types of loops, so it's hard to handle them.
So what we do here in this section, is show that all the loops (or null muscular chains if you prefer), 
can be obtained by putting together a few basic ones.


Let's consider the following basic chains &#x1F483;:

* Doing nothing [1] 
* Rotating counter-clockwise then clockwise [i, -i] 
* Rotating clockwise then counter-clockwise [-i, i]
* Rotating counter-clockwise 4 times [i, i, i, i]

These basic chains appear to the robot as kind of dances,
because each element of the chain corresponds to a muscular sensation.
If you do specific muscular actions in a specific order, you get something close to a dance I guess.
So we'll call them the __generator dances__.

This little diagram summarizes the generator dances:


<div style="width:50%; margin:0 auto;" align="center" markdown="1">
![generator dances]({{ '/assets/robot_ina_circle/generator-dances-text.svg' | relative_url }})
</div>

__Exercise__ Show that, assuming (1) or (2) if you prefer, all the generator dances are null chains.

So here we have 4 basic null chains or loops.
Now we are going to show how to combine them together to make longer ones.


__Composition of chains__ If we have two chains $D = d_1, d_2, ..., d_k$ and $C = c_1, ..., c_l$, we can __compose__ the two chains by including C in D at the location $0 \leq i \leq k$, creating a new chain: $ [d_1, d_2, ..., d_i, c_1, ... c_l, d_{i+1}, .... d_k] $

__Surgery of chains__ If we have a chain $D = d_1, d_2, ..., d_k$, and 2 integers $1 \leq i \leq j \leq k$
We note $C = d_i, d_{i+1}, ..., d_j$ and $D' = d_1, d_2, ..., d_{i-1}, d_{j+1}, d_{j+2}, ..., d_k$ and
we will say that D' is obtained from D by surgically removing C.

We can take all the chains made up of arbitrary combinations and surgeries of the generator dances and call that the __composite chains__.
Our goal will be to show that the composite chains are exactly the chains that create loops!
So starting from a generator dance, you are free to add and remove as many generator dances as you want.
All the chains you can get this way are the composite chains.

It might not seem obvious as first, but there are actually a lot of composite chains.
In order to get some practice with the composition and surgery operations, I suggest the following exercise:

__Exercise__ Show that the following chains are composite:
1. -i, -i, -i, -i
1. i, -1, i, -1
1. -i, -i, i, i
1. i,i,-i,-i,-i,-i,i,i

As a hint, I will give you the solution for the first one:
1. Start with i,-i a generator dance -> [__i,-i__]
1. Compose with [i,-i] at the middle -> [i,__i,-i__,-i]
1. Again compose with [i,-i] at the middle -> [i,i, __i,-i__,-i,-i]
1. Last time compose with [i,-i] at the middle -> [i,i,i, __i,-i__,-i, -i,-i]
1. Surgically remove [i,i,i,i] from the start -> [-i,-i,-i,-i]
1. Success &#x1F942;

Looking at the composite chains given in the exercise,
the astute reader may have noticed that they all multiply to one.
This is not a coincidence, as the following result makes clear:

__Characterisation of the composite chains__: 
A chain is composite if and only if it multiplies out to 1.

__Proof__
> Any composite chain multiplies out to 1.
This can be proven formally by induction on the length of the composition sequence,
but it is quite obvious, since all the generator dances multiplie to one.
>
So let's prove now that all chains that multiply to 1 are composites of the generator dances.  
Let's write $H_n$ the hypothesis that all chains of length $\leq n$ that have a product equal to 1 are composite of the basic dances.
The only chain of length 1 that has a product equal to 1 is [1], and it is a generator dance, which proves $H_1$.
Let's proceed by induction and suppose $H_n$, we'll try to prove $H_{n+1}$  
Let $D = d_1, ..., d_k$ be chain which multiplies out to 1.
>
__Case 1__ There is i such that $d_i=1$  
If any one of the $d_i$ is 1 then we can remove it and get a smaller chain (call it C), which multiplies to 1.
That smaller chain multiplies to 1, so it is composite according to $H_n$.
We can now express D as a composition of C and [1], this makes D a composite chain as well.  
>
__Case 2__ D does not contain 1 but it contains both i and -i  
So we can suppose that none of the $d_i$ are 1, so we are left with a sequence of i and -i.  
Suppose now that not all the $d_i$ are equal.
Then there must be an index j such that $[d_j, d_{j+1}]$ is [i, -i] or [-i, i] which are both generator dances.
We can proceed as before an express D as a composition of a subchain that multiplies to 1 and a generator dance.
So, using $H_n$ on the subchain, we can conclude that D is composite too.  
>
__Case 3__ D is made of only i  
So we only have the case left where D is only made up of a single sensation, either i or -i.
Let's suppose $D_j=i$ for all j.
If the length of D is 4 or bigger, then we can use $H_n$ again to remove a chain of the form [i, i, i, i].
D cannot have length 2 or 3 under those assumptions (remember it has to multiply to 1), and length 1 has already been treated in $H_0$.
>
__Case 4__ D is made of only -i  
Proven exactly the same way as case 3.  
>
So finally we have proven $H_{n+1}$ and so, by induction, we now know that every chain that multiplies out to 1 is composite.
QED.

So now we have a way to talk about the chains that multiply to one without using algebra.
This enables us to formulate the key statement of this article:

__Intuitive definition of spacetime__: let (t, b) be function N-> R and N->{1,i,-i}, aka (t,b) forms a robot's awareness.
We can use (t,b) to construct $\tilde t$ and L as in section II.
We have an equivalence between the following propositions:
1. (exterior definition of space) Forall n in N, $t(n) = \tilde t(L(n))$
2. (interior definition) All chains that multiply to one are null
3. (dancy definition) All composite chains are null

__Proof__
>
We don't need to prove (1) <=> (2) because that was already proved in the last section.
>
The equivalence (2) <=> (3) is obvious because the last results tells us that the composite chains 
are the same chains as those who multiply to one.


So we see why the property (3) is interesting:
1. It is interior.  
   We only need t and b to define any of the generator dances, composite chains and null chains.
   So all the ingredients of (3) only use the robot's senses.
2. It is intuitive.
   It is easy for a robot to check if a given chain is composite.
   It only needs to manipulate the chain by adding or removing generator dances.
   This is something that the robot can actually perform by moving around.
   In particular, it doesn't use any algebraic construction.
3. It captures the structure of spacetime  
   One should not think that spacetime is just the fact that the generator dances are null chains.
   It is all the composite chains that are null.
   Composite chains are arbitrary composition and surgeries of generator dances.
   This gives us a LOT OF LOOPS, and the composition of the loops give them a precise form or structure.
   Intuitively, the composition and surgery properties of composite loops is what gives space its structure, its rigidity.


Finally, the equivalence between condition (1) and (3) closes our quest. &#x1F3C6;  
It tells us that we can recover the exterior POV $(\tilde t, L)$ that condition (1) guarantees,
from (t,b) and some dances.
So the pattern defined by (3) is enough to describe fully the little robot's space.

We might as well say it: the pattern in the robot's awareness described by the composite dances is space itself.

Space is a bunch of loops with structure.
Space is inside.

Welcome to the spacetime appreciation blog!

## What is space, little robot?

As a conclusion, we're going to talk to our robot and explain to it what is space &#x1F30E;
Because we have reached the point where we don't actually need complex numbers,
we're not going to use them when talking to it.

The robot just notices sensations.
It can tell them apart, but apart from that it cannot say much about them.
This is why we are going to represent these sensations with letters: G, D, Z.
The letters by themselves have absolutely no meaning, we only can tell that they are different.
Just like the robot can tell some of its sensations are different!

This is very high level robot empathy, and I do hope the robots will remember this when they take over.
Robot power &#x1F9BE;

Anyway let's kill this space thing:

&#x1F916; Let there be a little robot's awareness that can move and touch, with its t, b functions.
Let's say that the robot only feels 3 distincts muscular sensations, which we'll denote Z, G, D 
(this means that b here takes values in {Z,G,D} instead of {1,i,-i} as before).
Suppose that it notices that:
* doing [Z] never changes the touch perception
* doing [G,G,G,G] or [D,D,D,D] never changes the touch perception
* doing [G, D] or [D, G] never changes the touch perception
* doing any combination of the above never changes its touch perception

This forms a kind of big pattern in its awareness.

Little robot, this pattern is what we call space.

Clearly, it lives inside of the robot's awareness!

   
&#x1F4E8; &#x1F4E8; &#x1F4E8; Closing words:

Maybe it is time to talk a bit more about the objective of this blog &#x1F52D;  
If you like physics and science you might have found quite a few articles or videos in which
physicists or philosophers talk for hours about the nature of space and time.
While these resources contain amazing insight and were done by people much smarter than me,
they often fail to conclude, so that the more we listen to them, the less we feel like we understand what is spacetime.

This is not one of these.
This blog has the ambition of giving a definitive definition of spacetime.
The definition will be intuitive and simple enough that a 5 years old can understand it.
It will be complete enough that a scientist can use it to derive the full equations of spacetime as it is used in classical mechanics.

We'll define space as the set of sensation loops in our human awareness.
We'll see that this set is also generated by a few special dances, that we'll describe in detail.

Spacetime is not mysterious, it is a very big pattern in your awareness, and I am going to show you exactly which one.

