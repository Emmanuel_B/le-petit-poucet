---
layout: article
title:  "Robot Returns: Space from Sensations"
date: 2021-11-02 16:50:47 +0000
key: "ROBOT_RETURNS_20211102"
comment: true
mermaid: true
mathjax: true
mathjax_autonumber: true
categories: mathematics 
license: true
cover: 'assets/robot_returns/monkey_quaternion.gif'
aside:
    toc: true
tags: space mathematics awareness algebra language
---




In the 
[last article]({{ '/mathematics/spirituality/2021/10/17/robot-ina-circle.html' | relative_url }}) 
we started from the outside point of view of what it means for the robot to exist in space, and then transposed it to the interior point of view of the robot.
Here we go the opposite way, start with the raw sensations and the dances and build a space out of it.


[Last time]( {{ '/mathematics/spirituality/2021/10/17/robot-ina-circle.html' | relative_url }})
the key was a rough modelisation of a simple robot
that uses complex numbers to model the space and complex multiplication to model rotation in that space.
We then showed how we could translate this exterior point of view to an internal point of view
which uses special dances to describe space.

I will reuse some of the notation from the
[previous article]({{ '/mathematics/spirituality/2021/10/17/robot-ina-circle.html' | relative_url }}).
{:.warning}

Here we'll go the opposite way, we'll start with the intuitive dances and we'll see how we can reconstruct a notion of space from that.
We'll start by showing how we can use 4 special muscular chains &#x1F4AA; to recreate the points of the world,
having a definition of the outside world that uses only interior perceptions.
Then we'll use the most natural operation there is on muscular chains: concatenation.
It simply consists in doing 2 muscular chains, one after the other.
We'll see that we can recover complex multiplication as a "shadow" of concatenation,
and it establishes a surprising link between algebra and movement.
It will also allow us to finish the modelisation and come back to the exterior point of view of the first article.
In the last section, we'll talk more about the notion of displacement and its algebraic counter-part.

For the whole of this article, we'll assume the following setup:

Let there be a little robot awareness that can move and touch, with its t, b functions.
Let's say that the robot only feels 3 distincts muscular sensations, which we'll denote Z, G, D.
Suppose that it notices that:
* (P1) doing [Z] never changes the touch perception
* (P2) doing [G,G,G,G] or [D,D,D,D] never changes the touch perception
* (P3) doing [G, D] or [D, G] never changes the touch perception
* (P4) doing any combination of the above never changes its touch perception
* (P5) at least once, doing [G] changed the touch sensations
* (P6) at least once, doing [G, G] changed the touch sensations

We can rewrite these assertions using our new vocabulary:
 P1, P2, P3, P4 say that [Z], [G,G,G,G], [D, D, D, D], [G, D], [D, G] and all composites are null chains.
While P5 and P6 say that [G] and [G, G] are __not__ null chains.

Believe it or not, this is enough to rebuild the full outside picture of the robot in its circle.

<div class="item">
  <div class="item__image">
	<a href="{{ 'dist/' | relative_url }}">

    <img class="image image--md" src="{{ 'assets/robot_returns/monkey_quaternion.gif' | relative_url }}" />
	</a>
  </div>
  <div class="item__content">

    From sensations to space, let's start the journey.  
  </div>
</div>

[_Image source_](https://blogs.scientificamerican.com/roots-of-unity/nothing-is-more-fun-than-a-hypercube-of-monkeys/)

## Repetitions, repetitions, repetitions

Here we want to start gently, instead of jumping straight into giving properties and proofs,
we're going to introduce some vocabulary and intuition.
We will talk about the awareness context and the role of repetition in our assumptions.

The first thing I would like to draw your attention to is that we are really talking about a single robot's awareness.
That is two functions t,b defined like below:


![diagram]({{ '/assets/robot_ina_circle/robot-ina-circle-awareness-eq.png' | relative_url }})


As a quick way to concretise what it means, here is a sample of a "robot awareness":

| time 	| 0  	| 1  	| 2  	| 3  	| 4   	| 5  	| 6   	| 7  	| 8   	| 9  	| 10  	| 11 	| 12  	| ... 	|
|------	|----	|----	|----	|----	|-----	|----	|-----	|----	|-----	|----	|-----	|----	|-----	|-----	|
| t    	| 15 	| 15 	| 15 	| 15 	| 100 	| 15 	| 100 	| 30 	| -10 	| 15 	| -10 	| 15 	| -10 	| ... 	|
| b    	| Z  	| Z  	| Z  	| G  	| D   	| G  	| G   	| G  	| G   	| D  	| G   	| D  	| D   	| ... 	|

Quick test, can you spot a few composite chains here? are they null?

We are really tied down to the actual content of these t,b functions.
In particular, the robot is free to do what it wants! 
So for example, the chain [G,G,G] might never appear.
For another robot it might appear, but only once.
For another, it might appear an infinity of times.

This is very different from the exterior point of view.
In the previous article, when we considered the robot from our point of view, with the localisation function,
we could have made sense of the sentence "[G,G,G] is a not null chain" _even if a given robot never actually did it_.
This is because we could use the function L to simulate what would have happened if the robot did do it.

Remember that the definition of a null chain says that any chains that never changes preceptions when it is done, is null.
According to this definition, any chain that never happens is null.
This is kind of boring, because those are not actually special chains, it's just that they don't happen enough to be understood.
Fortunately that is not a big problem to solve, we can just limit our attention to the chains that happen enough.

&#x27A1; So first we're going to give a few definition that allow us to conveniently talk about when and if the chains appear.

A muscular chain $A=[A_1, ..., A_k]$ is said to __happen at time n__ if
there exists a time n such that: 
$b(n)=A_1,b(n+1)=A_2, ..., b(n+k-1)=A_k$.

We'll note \|A\|, the __length__ of the muscular chain A.

We'll say that A __produces sensation__ $s \in R$ at time $n \in N$ when:
* A happens at n
* t(n+\|A\|) = s

Note that this last definition just says that the robot observes s after performing A at n.

__Example__: Let's go back to the sample of robot awareness that we gave above and note that:
* The chain [G,D] has length 2
* It occurs at 3, and produces sensation 15 at time 3.

&#x1F4A1; Now it is also a good time to introduce a key element of this article: repetitions.
Do note that a key element of the properties P1...P6 are different kinds of repetitions.
A null chain is a _loop_: after you do it, you feel again the same sensation as when you started.
So there is a first kind of repetition here, a repetition of sensation,
but there is also another kind of repetition lurking behind:
no matter at which time the null chain appears, it works the same.
This second kind of repetition gives structure in time, and allows us to make predictions.

I am drawing your attention to this because I want to emphasize that, apart from properties (P1) ... (P6),
we are really taking random functions t,b.
Anything could potentially happen!
For example, property P5 says that [G], at least once, did not loop sensations.
Let's take a concrete example, let's say at time n=5 the robot is feeling 15 degrees, and decides to go for muscular sensation G.
That is:  
$$t(5) = 15 \\
b(5) = G$$
Now, after that, the robot observes 100 degress, that is $t(6)=100$.
So indeed, at this time, [G] did not loop sensations.

But suppose later, say time n=100, the robot again feels a temperature of 15 degrees, and also goes for [G]:
$$t(100) = 15 \\
b(100) = G$$

Then can we predict that t(101) will give 100?
Nope! None of our assertions can say that.
So although we have a repetition for null chains, we don't know much about all the other chains.

Well actually, here is something we can say.
Take the muscular chains between time 5 and 100; $[b(5), b(6), ..., b(99)]$, and call it M.
Suppose M is composite, then we'll have $t(101) = 100$! (We'll prove this later in the article).
So we do have some repetition for arbitrary chains too!
They produce the same sensations change when they are repeated after a composite chain.

That is cool, but you might think that M is rarely a composite chain, so that in practice it doesn't repeat often.
Surprisingly, we'll be able to prove that for any chain M, either M is composite, or one of M.[G], M.[D], or M.[G,G] is composite.
So there are A LOT of composite chains, and so there are a lot of repetitions in the robot awareness (t,b).

To conclude on this section, we'll say that not only there are a lot of repetitions, but those repetitions have a structure.
The exact structure is given by the composition of the generator dances.
There are so much repetitions actually, that we can summarize the robot's perceptions with a few bits of information: the localization function and the function t' giving the temperature at each point.
(L, t') forms the external point of view, it is a summary of the internal point of view, 
once all the repetitions have been taken into account.
You can think of space as the set of all the repetitions in the robots awareness, as well as their structure.

Now we're ready to go.
We're going to rebuild step by step the exterior point of view from the robot's awareness (t, b) and its repetitions described by P1..P6.


## Space from sensations

The idea to reconstruct space is to equate space with the set of all the places you can go.
Now you do you find that?
Well you can start with the set of all muscular chains, because they correspond with movements.
That's a nice start, but there is the issue that many different muscular chains create the same displacement.
For example: both [G, G] and [D, D] gets the robot to the other side of the circle.

What makes [G, G] and [D, D] similar in terms of displacements?
From the exterior point of view it is obvious, GG goes counter-clockwise 180 degrees while DD does the same clockwise,
so they end up in the same location.
But &#x1F92B;, that is cheating, the robot doesn't know about that, it only knows about its special dances,
so we need to find a way to express the relationship between [G,G] and [D,D] using the dances:
* [D, D] -> [G, D, D, D] since [G, D] is a generator dance
* [G, D, D, D] -> [G, G, D, D, D, D] here I am again inserting [G, D] but this time just after the existing G.
* [G, G, D, D, D, D] -> [G, G] here we remove the generator dance [D, D, D, D]

So we've successfully build a connection between [G, G] and [D, D] using a composition of generator dances.
As we'll see later, this turns out to work all the time.
Everytime two muscular chains produce the same displacement, they can be related by generator dances.
We'll call that property: 'equality up to null chains'.


__Equality up to null chains__ Let B, C be two muscular chains.
We'll say that B and C are equal up to null chains, and write B ~ C, 
if either:
* B is obtained from C by composing with generator dances
* C is obtained from B by composing with generator dances

Note that this relationship is symmetric: $B \sim C \iff C \sim B$ and transitive: $B \sim C \text{ and } C \sim M$ implies $B \sim M$.

__Exercise:__ Are the following chains equal up to null chains?
* [G] and [G,G,G,G,G]
* [G] and [D,D,D]
* [G,G,Z,D,G,D,D,D,D] and [D, Z, D]
* [G] and [G,G]

I think here we need to stop and think a little bit about this property.
We have defined equality up to null chains purely in terms of algebra of chains,
so it is natural to ask the question: how does it translate intuitively?
Later in the article, when we reach the level of the exterior point of view,
we'll see that the chains are equal up to null chains when they produce the same displacement.

That is interesting from our exterior point of view,
but what about the robot?
Do chains that are equal up to null chains _feel the same_ in some way?
Indeed that is exactly the case.
Doing two chains that are equal up to null chains produce the same change in sensations (between the start and the end).
And if two chains always produce the same change in sensations they are equal up to nullity.
However, we won't be able to prove this until the last section.

In the property below, we recover the four spots of our square, using the trick of removing generator dances from muscular chains.

__classification of muscular chains up to null chains__ Let C be an arbitrary muscular chain, then one and only one of the following holds:
* C ~ [Z]
* C ~ [G]
* C ~ [G, G] 
* C ~ [G,G,G]

That is there are only 4 __reduced__ muscular chains up to null chains.

__Proof__:
>
The key observation here is that, up to null chains, we can express all muscular chains with only the sensation G. 
* [Z] ~ [G, G, G, G]
* [D] ~ [G, G, G]. Indeed, we have $[D] \sim [D, D, G]$ since $[D, G]$ is null.
  Adding 2 more [D, G] we get [D] ~ [D, D, D, D, G, G, G] ~ [G, G, G] since [D, D, D, D] is a composite chain.
>
Let C be an arbitrary chain, we can use the result above to obtain a chain C' obtained from C 
by replacing every Z by [G, G, G, G] and every D by [G, G, G].
We have C ~ C' and C' is only made up of G's.
Since [G, G, G, G] is null, we can remove it from C' as many time as possible.
We obtain a chain C''~ C,  which is only composed of strictly less than 4 G's.
If C'' = [] we have C'' ~ [Z] because [Z] is a generator dance.
Otherwise, C'' ~ [G] or C'' ~ [G, G] or C'' ~ [G, G, G], and, since C ~ C'', this proves that C is equal to 1 one the 4 reduced chains -- up to null chains.
>
Let's show now that the reduced chain is unique.
We only need to show that none of the 4 reduced muscular chains are equal to each other up to null chains.
Suppose for example that [G] ~ [G, G], then we can replace each G with [G, G] giving: [G] ~ [G, G, G, G] ~ [].
This means that [G] is a composite of generator dances, which implies (by supposition P4) that [G] is null.
This is not possible, since we have supposed in P5 that [G] is not a null chain.
So we can't have [G] ~ [G, G].
>
We can use a similar argument to show that the other reduced chains are pairwise distinct -- up to null muscular chains.



We have made some nice progress already, because we have recovered the 4 positions the robot can live in.
In a sense we now know that the space has 4 spots.
Let's write W'={[Z], [G], [G,G], [G, G, G]}


&#x1F30F; __creation of outter world__ We have a function $\pi: \\{\text{all muscular chains}\\} \rightarrow W'$ that takes any muscular chain to its unique reduced representation in W'.

Note: we have 
* $A \sim B$ iff $\pi(A) = \pi(B)$
* For any muscular chain A, $\pi(\pi(A)) = A$
* The chain D is a composite of generator dances iff $\pi(D) = [Z]$


What can we say about this construction?
Well some of it is kind of intuitive.
By building our space out of [Z], [G], [G, G], [G, G, G],
we effectively identify a point of space with a special movement.
This makes sense, since we can attain each point of space by some movement (ignoring technical issues).

But it is also surprising, in that it blurrs the distinction of what is interior and what is exterior.
We have litteraly defined space, which most people would say is _outside_, with chains that appear _inside_ the robot.
In this article, we hope the reader is able to reflect on the notion of interior, exterior and that they are not absolute.
In a sense, everything we know appears in our awareness, so that one can say that everything is inside.
Perhaps the reader can also make sense of the blog's motto: "I do not appear in space, but space appears in I".


We can already apply this concept to get _real benefits_ in our _daily life_.
For example, if you are still a kid, and your mom asks you to go play outside,
you can just point her to this article, and explain to her that everything is inside anyway so you might I well stay here.
She might not be convinced at first, but if you explain the algebraic construction with enough patience, she might just leave you alone.

Allright, so now we have our space W', but we still need to recover how to multiply its elements,
this will be the topic of the next section.



## Algebra from sensations


If you have two muscular chains &#x1F4AA; &#x1F4AA;, you can do one after the other and get a longer one &#x1F9B5;:

__concatenation of muscular chains__ Let A and B be two muscular chains,
 we can concatenate them together to create a longer chain by simply putting all the sensations together.
We'll note $A \cdot B$ for the concatenation.

Example: $[Z, Z, G] \cdot [G, G, D] = [Z, Z, G, G, G, D]$

In mathematical terms, this equips the muscular chains with a monoid structure.

In casual language, the concatenation of A and B is just the sensation chain you would perceive if you would do A first then B.
It is very natural, and we see that time plays an implicit role in this definition.
Doing A then B is the most basic thing you can do with time.

Our goal now is to use this concatenation operation to induce a multiplication on W'.
Our first step is to show that concatenation is compatible with the relation ~.

__concatenation respects equality up to null chains__ For any four muscular chains A,B, X, Y, such that A ~ B and X ~ Y.  
We have $A \cdot X \sim B \cdot Y$.

__Proof__:
> A ~ B so there is a procedure to transform A into B using only generator dances.
X ~ Y so there is a procedure to transform X into Y using only generator dances.
By applying the first procedure on the left of $A \cdot X$ and the second on the right side of $A \cdot X$,
we obtain a procedure that takes us to $B \cdot Y$, thus $A \cdot X \sim B \cdot Y$.  
QED.

__Exercice: all chains are close to null chains__ Let A be any muscular chain.
Then one of A, A.[G], A.[G,G], A.[D] is null.

__Multiplication in W'__ We can use the concatenation to create a multiplication in W':
Let $g, h \in W'$, we define $g \ast h = \pi(g \cdot h) \in W'$.
This operation is associative: $(g \ast h) \ast k = g \ast (h \ast k)$,
which means we don't have to worry about where to put the parentheses.

For n in N, we'll note $g^n = g \ast g ... \ast g$ (n times).

__Proof__:
> 
Let's show that the operation is associative.
$(g \ast h) \ast k = \pi(\pi(g \cdot h) \cdot k)$  
Note that $\pi(g \cdot h) \sim g \cdot h$, so that  
$\pi(g \cdot h) \cdot k \sim g \cdot h \cdot k$ and so
$(g \ast h) \ast k = \pi(\pi(g \cdot h) \cdot k) = \pi(g \cdot h \cdot k)$  
Using the same type of reasoning we can show that:  
$g \ast (h \ast k) = \pi(g\cdot h \cdot k)$  
Which proves the statement.

At this point, our robot has it's very own multiplication.
Since there are only 4 reduced chains, we can carry out all the possible multiplications,
and put them in a table.

__Multiplication table for W'__:  

| $\ast$       	|   [Z]   	|   [G]   	|  [G,G]  	| [G,G,G] 	|
|:-----------:	|:-------:	|:-------:	|:-------:	|:-------:	|
|   __[Z]__   	| [Z]     	| [G]     	| [G,G]   	| [G,G,G] 	|
|   __[G]__   	| [G]     	| [G,G]   	| [G,G,G] 	| [Z]     	|
|  __[G,G]__  	| [G,G]   	| [G,G,G] 	| [Z]     	| [G]     	|
| __[G,G,G]__ 	| [G,G,G] 	| [Z]     	| [G]     	| [G,G]   	|

__Proof__:
>
We use chain concatenation and then we simplify using the generator dances.
For example: $[G,G] \ast [G,G] \sim [G,G,G,G] \sim [Z]$.
Which in turns implies $[G,G] \ast [G,G] = [Z]$.
>
The other multiplications can be done in the same fashion.

__$\ast$ is commutative:__ You can swap the orders of the products:  
For all $g,h \in W'$, we have $g \ast h = h \ast g$.

__Proof:__  
> Done by checking one by one in the table.


&#x1F4A1; So now we start to have a good understanding of our multiplication in terms of its algebra.
But algebra is never very far from geometry:
in the following diagram we'll see how the multiplication structure suggests space already.

Let start from [Z] and multiply by [G] again and again.
We get [Z] then [G] then [G,G] then [G,G,G] then... back to [Z].
This looping behavior is pictured below:

<div class="item">
  <div class="item__image">
	<a href="{{ 'dist/' | relative_url }}">
    <img class="image image--md" src="{{ '/assets/robot_returns/circle-from-chains.svg' | relative_url }}" />
	</a>
  </div>
  <div class="item__content">

	In this diagram each red arrow represents multiplication by [G].

	We can see how space is starting to emerge from the reduced chains and $\ast$.
  </div>
</div>


As we discussed, but not yet proved, equality up to null chains means that 2 muscular chains 
produce the same displacement.
The operation $\pi$ takes any chain to a single canonical representative chain that performs the same movement.
The question here is what happens to the representative of a concatenation $A.B$?
It turns out that it is $\pi(A) \ast \pi(B)$, which is why we say that $\ast$ is a shadow of concatenation.


__concatenation is multiplication up to null chains__ Let A and B be two muscular chains, then:
$\pi(A \cdot B) = \pi(A) \ast \pi(B)$

__Proof__:
> 
$A \cdot B \sim \pi(A) \cdot \pi(B)$  
Applying $\pi$ on both sides, we get: 
$\pi(A \cdot B) = \pi(\pi(A) \cdot \pi(B)) = \pi(A) \ast \pi(B)$  
QED

In one sentence: $\pi(A) \ast \pi(B)$ is a reduced movement that performs the same displacement as $A\cdot B$.

At this point we have this cool new multiplication.
Is it really new though?
Have we stumbled on a totally new multiplication that is going to change the face of mathematics forever?
Not really, as it turns out $\ast$ is just complex multiplication, but wearing a disguise.
In order to make it clear, we can put the multiplication table for W and W' next to each other:


__Multiplication table for W'__:  

| $\ast$       	|   [Z]   	|   [G]   	|  [G,G]  	| [G,G,G] 	|
|:-----------:	|:-------:	|:-------:	|:-------:	|:-------:	|
|   __[Z]__   	| [Z]     	| [G]     	| [G,G]   	| [G,G,G] 	|
|   __[G]__   	| [G]     	| [G,G]   	| [G,G,G] 	| [Z]     	|
|  __[G,G]__  	| [G,G]   	| [G,G,G] 	| [Z]     	| [G]     	|
| __[G,G,G]__ 	| [G,G,G] 	| [Z]     	| [G]     	| [G,G]   	|


__Multiplication table for W__:  

|  $\times$    	|   1   	|   i   	|  -1   	| -i   	|
|:-----------:	|:-------:	|:-------:	|:-------:	|:-------:	|
|   __1__   	| 1     	| i     	| -1    	| -i  	|
|   __i__   	| i     	| -1    	| -i    	| 1     	|
|  __-1__   	| -1    	| -i    	| 1     	| i     	|
| __-i__    	| -i    	| 1     	| i     	| -1    	|


If you look closely at the tables, you will notice that they are pretty much the same.
You can get from one to the other through the substitution [Z] -> 1, [G] -> i, [G, G] -> -1, [G, G, G] -> -i.

In mathematical terms, we would say that W and W' are isomorphic as groups.

&#x27A1; At this point there are a few things to take notes of.
First of all, we have managed to find again the equivalent of W.
Our W' is not made of complex numbers, but of 4 chains of sensations.
Though the type of the points of W and W' are completely different,
in both cases there are only 4 of them, and that is what matters in the end.

The space W comes with a multiplication, which is abstract in nature, called complex multiplication.
The space W' also comes with a multiplication $\ast$, which is a remnant of chain concatenation.
Don't be fooled that $\ast$ is abstract, it is just a shadow of one of the most basic intuitive operation "doing A then B".
So both W and W' have very different multiplication in spirit, yet when we look at their table, it works the same way.

In the end we have succeeded in reconstructing our space.
We have two alternative constructions one by nature abstract, the other by nature concrete,
and they are both equivalent.
What is then the difference between abstract and concrete?

I will argue that the abstract/concrete dichotomy is not really relevant,
and one is better off not looking at things that way.
I suggest instead looking at algebra as a _language_ that allows us to express
things that perhaps, English cannot.

Let's say that the robot meets another robot like itself,
and wants to share about its experiences. &#x1F4AC; 

Well there are two ways it could proceed.
If it knows about its space, it could just tell the other robot about the nice temperature at one point of the space.
For example, it could say "temperature is nice at position [G]".
If it didn't know about space, it could tell "starting from [Z], when I do [G], or [D,D,D] or [G,G,D] I feel a nice temperature".

Of course, it is stupid to say "when I do [G], [D,D,D], [G,G,D] " because those are muscular chains _that always produce the same change in sensations_ (they are all equal to [G] up to null chains).
Because the other robot is wired the same way, it already knows that, so it is much faster to just say "temperature is nice at [G]".
The message here is that robot's sensations are far from random, they are extremely structured,
and so it is possible to talk about them in a more concise fashion by taking advantage of this structure.

This also applies to our human sensations as well.
They are even more structured than the robot's although the pattern is more complex.
A single sentence in English is able to describe what a given human will feel and see and touch,
without directly talking about muscular chains, because it is not necessary.

As an example, when you say: "I see bread", you don't need to precise from which point of view you see it.
Because it is understood that if you can see the bread from one point of view,
you can move about (= perform a muscular chain) and see it in another point of view.
This is true for all humans awarenesses, so we can just skip it altogether, and everybody understands fine.

When we say "I see bread" or even "there is bread" &#x1F35E;, the lack of point of view gives it its exterior feeling.
Anyone can see the bread, from any point of view, so it is exterior to me.
But note that the sentence "I see bread" has no meaning if there is not a specific angle from which you actually see it.
And, imagine moving about, and the bread disappearing...
That would not be bread then, maybe an illusion or....?
In any case, IF the bread didn't behave as expected when I moved about, the sentence "I see bread" would be meaningless.
The exterior point of view in "I see bread" or "there is bread" encompasses both the actual sensation
and the intuitive understanding of how these perceptions change when I move about.
So it is built from the interior point of view too.

Since we build the exterior point of view by cutting out obvious repetitions from the interior point of view,
it could be better to name it the "compressed point of view".

In any case, we see that English (or other exterior point of view languages like (L, t') for our robot)
are faster and more convenient to exchange information.

Why then use another language like algebra?
Note that by their construction itself, compressed languages skip the repetitions in our awareness.
They are _totally unable to express_ this huge part of our life:
this repetition pattern that imprints each instant of our awareness, from seconds to years.
Probably the largest scale pattern in our awareness: spacetime.
Why should we not talk about it?
It might be redundant information, but it still exists, and so deserves to be described.

Algebra is able to express the structure of our sensations, it is the language of spacetime.
It is useful for example if you want to understand what happens under a heavy psychedelics,
where the structure of the awareness is going to be affected.
It is also useful if you do a lot of meditation and have a detailed look at your own sensations.

I also believe that it is a good tool if you want to understand who you are.
The idea of you as a person performing "things in the world" is a highly compressed version of
everything that you see and feel every second.
How was this concept built?
English cannot express the answer, but algebra might.

Who built this concept?
No language can express this answer.


English and algebra are two different languages, two different tools.
Depending on the task, it might be wiser to use one or another.
In the end, both languages aim at the fundamental goal of communication:
describe what human see, touch and feel -- paint a picture of our sensations.

So I guess algebra can be close to our sensations too...
One has to admit: abstract algebra... is not really abstract.


We'll need to notice a final property about W' that'll come in handy later:

__inverses__: For any $g \in W'$, there exists a unique $h \in W'$,
such that $g \ast h = [Z]$.
Furthermore, it also verifies $h \ast g = [Z]$.
We call h the inverse of g, and we note it $g^{-1}$.

__Proof__:
>
The proof can easily be done by going through all the elements of W' and using the multiplication table.
For example, we can read from the table that $[G][G,G,G] = [G,G,G][G] = [Z]$, and that there are no other elements h such that $[G]h=[Z]$.

In order to conform with standard mathematical practice, we can call [Z] the __identity__.
Mathematicians also have a word for sets like W', with a single operation, an identity and inverses,
they are called __groups__.

Roughly speaking, group theory is the language of spaces and geometries.
The modern definition of space time (not taking into account general relativity) in physics is a group,
it is called the Poincare group.
Pretty much everything in this blog uses group theory at some level,
so if you learn/know a bit of it, everything will be _much less painful_.
You won't need any for this article though (but it will hurt a little).


<div class="item">
  <div class="item__image">
	<a href="{{ 'dist/' | relative_url }}">

    <img class="image image--md" src="{{ 'assets/robot_returns/insight-overload.jpg' | relative_url }}" />
	</a>
  </div>
  <div class="item__content">

Every good blogger needs to know when is the good time to stop, in order for the reader to relax a little.
Unfortunately, I totally lack any kind of dosage skills, so I cannot resist giving you <i>more insight</i>.

<br>

Indeed, I showed you how to reconstruct the complex multiplication on 4 numbers from a robot.
But that might not convince a lot of people.
What about the addition of the real numbers, multiplication of all complex numbers, multiplication of matrices?
Can we retrieve these algebraic operations from sensations and movements?


  </div>
</div>

Here I make a very bold assertion, that I cannot prove here (this article is long enough believe me),
that we can recover the algebra of any group from a suitable robot with suitable generator dances.

In particular, we can recover matrix multiplication, addition and multiplication of reals, and a lot of other abstract things.

What happens if we consider our human awareness and our own repetitions, can we get an algebra from our movements?
It turns out that yes, and we get the venerated Galilean group.
A 10 dimensional, infinite group, a cool beast.
The proof of this assertion, and the study of this group, is basically the whole point of this blog.
You can view this whole website as a fanboy blog for the Galilean group.

Anyway that is it for the insight for this section.
The article is far from finished though, and there might be more insight in the next sections (sorry).

## External point of view

Now that we have our W', we can complete our description of the robot from the exterior point of view.
We'll recreate a localization function L, describe the temperature at all points in the space using t',
and then check that together they verify relation (1).

All these terms have been defined in the 
[previous article]({{ '/mathematics/spirituality/2021/10/17/robot-ina-circle.html' | relative_url }}) 
, do have a look if you need.

Let's randomly decide that $L(0) = [Z] \in W'$.
At time n, we note $b(n) \in \\{Z, G, D\\}$ the muscular sensation the robot currently feels.
We write $L(n+1) = L(n) \ast \pi([b(n)])$.
This allows us to define L for all times.

__Exercice:__ If A is a muscular chains that happens at n, show that:
$$L(n+|A|) = L(n) \ast \pi(A)$$


Now we have to define what our space looks like in term of temperature at every point.

For this, we can select 4 integers $n_Z, n_G, n_{GG}, n_{GGG}$ such that:  
$$L(n_Z) = [Z] \\ 
L(n_G) = [G] \\  
L(n_{GG}) = [G,G] \\  
L(n_{GGG}) = [G,G,G]$$

In more casual language, $n_G$ is a time when the robot is at [G].


&#x1F64B; Pedantic note: how do we know that those times actually exists?
We don't actually.
But for 99.999999% of t,b functions they will exist.
For example, if we know the chain [G,G,G,G] happens at least once for a robot, then for sure these integers exist.
I don't want to get bogged down in technicalities, so let's just suppose that they do exist (add it as an axiom), and leave it at that.


Reminding ourselves that we note t(n) the temperature that the robot feels at time n,
we can define a function t': W' -> R as before using these integers:  
$$t'([Z]) = t(n_Z)  \\
t'([G]) = t(n_G)  \\
t'([G,G]) = t(n_{GG})   \\
t'([G,G,G]) = t(n_{GGG})  $$

At this point we have all the key ingredients for our exterior description of this robot in its circle.
We just need to verify the condition (1) that is: $t = t` \circ L$.

&#x1F3C6; __The robot exists in the space W'__ The functions we just defined verifies $t = t` \circ L$.

__Proof__:
>
Since (1) is equivalent to (2), let's just prove (2) instead.
Let n, k, such that $b(n) \ast b(n+1) \ast ... \ast b(n+k) = [Z]$.
We want to show that t(n) = t(n+k+1).
$$b(n) \ast b(n+1) \ast ... \ast b(n+k) = [Z] \\
\iff \pi(b(n)) \ast \pi(b(n+1)) \ast ... \ast \pi(b(n+k)) = [Z] \\
\iff \pi(b(n) \cdot b(n+1) \cdot ... \cdot b(n+k)) = [Z] \\
\iff   b(n) \cdot b(n+1) \cdot ... \cdot b(n+k) \sim [Z] \\
\iff   b(n) \cdot b(n+1) \cdot ... \cdot b(n+k) \text { is composite} \\
\implies b(n) \cdot b(n+1) \cdot ... \cdot b(n+k) \text{ is null}$$   
So $b(n) \cdot b(n+1) \cdot ... \cdot b(n+k)$ is null, a.k.a it doesn't change sensations,
a.k.a. t(n) = t(n+k+1).  
QED

Note in the previous demonstration:
Since all the b(j) are singular sensations, the chain concatenation $b(n) \cdot b(n+1) \cdot ... \cdot b(n+k)$ is just  [b(n), ..., b(n+k+1)].



<div class="item">
  <div class="item__image">
	<a href="{{ 'dist/' | relative_url }}">

    <img class="image image--lg" src="{{ 'assets/robot_returns/robot-returns-ina-circle.svg' | relative_url }}" />
	</a>
  </div>
  <div class="item__content">

That is it, we are back to the outside point of view.  <br>
That was the goal of this article, so I guess, congrats. <br>
Unfortunately for the reader, there is more insight coming...
  </div>
</div>

The main difference is that instead of having a space W made of complex numbers equipped with multiplication,
we have W' made of 4 muscular chains, equipped with a remnant of chain concatenation.
Both are fully equivalent.
You can think as space as something that is really exterior but can be experienced from the inside.
Or you can think of space as a pattern in awareness built out of generator dances.
There is no difference in the actuality of what space is, it is just a difference in how we express it.

The difference between the exterior and the interior point of view is syntactic.


Perhaps I can add a bit more intuition here.
Something what was perhaps weird in the last article was that muscular sensations
were taking values in W.
Why would muscular sensations have the same nature as points in space?

This is not just an artifact of my very random modelisation of the robot.
This is actually quite common in physics.
You can describe a simple space, for example a line as $R$.
Now movements in that space are also reals, elements of $R$.
This is easily explained if one believes that all spaces are internal at the core.
If you take a look at W', it's made of muscular sensations.
And of course movements are also made of muscular sensations.
So it makes sense that movement and space are built of the same thing.
In the end, it is muscular sensations all the way down.



## What is movement?

This article has been quite heavy with algebra and maybe light on intuition.
We have introduced a formal relation called "equality up to null chains", and used that as our basis to build the exterior point of view.
Now it is only natural to ask: do chains that are equal up to null chains feels special from the robot point of view &#x2753;
Could the robot have come up with this idea by itself?

Spoiler: chains are equal up to nullity when they produce the same change in sensations.
But before we can prove this result, we need to talk a bit more about movement.	


&#x1F3C3; &#x1F3C3; &#x1F3C3; What is our intuition of movement?
Movement is dynamic, and produced by (robotic) muscles, so we can use a muscular chain to describe it.
But at the same time, several muscular chains can produce the same displacement.
So we need to consider a way to tell when two different muscular chains actually represent the
same displacement.

The most straightforward way is to use the exterior point of view to define movement.
Suppose a chain A happens at time n.
The original position is given by $L(n)$, the position after performing the muscular chain is given by $L(n+|A|)=\pi(A)\ast L(n)$.
So $\pi(A)$ represent the change in position that chains A does.
We use this intuition for the next definition:

__Exterior definition of displacement__:
Two muscular chains A and B produce the same displacement iff:
1. They happen at every location
2. There exists a unique $g \in W'$ such that  
	Whenever A happens at n, $L(n+|A|)=g \ast L(n)$,
    and whenever B happens at n' $L(n'+|B|)=g \ast L(n')$

This is nice and very clear from the exterior point of view,
but this is not satisfying at all from the point of view of the robot's awareness.
Let's try another way to look at it.
We can tell that two muscular chains are alike if doing either of it creates the same change in sensations.
For example, starting from sensation s1, and doing A, the robot feels s2.
Later, starting again from sensation s1 and doing B, the robot also feels s2.
This is something extremely intuitive that even a baby could notice.
Our intuition tells us that if two muscular chains _always_ create the same change in sensation,
then they probably describe the same change in position, the same displacement.

Allright so at this point we have 3 ways to talk about displacement:
1. (Exterior) muscular chains that produce the same change in positions
2. (Interior) muscular chains that always produce the same change in sensations
3. (Algebraic) muscular chains that are equal up to null chains

In this part we'll show that they are equivalent.



Let's start by defining what it means for muscular chains to always create the same change in sensations.
Note that at this stage, there is nothing that guarantees even that 
doing the same chain A twice from s1 should create the same change in sensations.
If you look at what we supposed at the start of this article: (P1)...(P6),
there is nothing that explicitely says that doing the same chain twice should create the same change in sensations.
But as we're going to see below, sensations do repeat.
 
&#x1F504; __repetition of sensations__:
Suppose A produces sensations s at n.
Suppose also that A happens again at another time n' such that the muscular chain between n and n' is composite.
Then A also produce sensations s at n'.

Note: saying that the muscular chain between n and n' is composite is equivalent to saying that L(n)=L(n').
So this property just says that doing the same thing again from the same location will produce the same sensations.

__Proof__:
>
Let's assume n' > n.
The chain $[b(n), b(n+1), ..., b(n'-1)] = M$ is composite by assumption.
Since A happens at n, M must start by A, so that we can write $M = A \cdot M'$.
We have $[Z] = \pi(M) = \pi(A) \ast \pi(M')$, so that $\pi(M') = \pi(A)^{-1}$.  
The chain that goes from t(n+|A|) to t(n'+|A|) is $M'.A$,
 we have $\pi(M'.A) = \pi(M')\ast\pi(A) = [Z]$ according to the properties of inverses.
So $M'.A$ is composite, hence null by (P4), so it must loop sensations:
$t(n+|A|) = t(n'+|A|)$  
QED

![diagram for the proof]({{ '/assets/robot_returns/diagram-chain-repetition.svg' | relative_url }})


Two muscular chains A and B are said to __behave the same__ at $g \in W'$
* if there exists $t_1, t_2$ such that $L(t_1) = L(t_2) = g$
* if A happens at $t_1$ and B happens at $t_2$
* and if there exist a single sensation s such that A produces s at $t_1$ while B also produces s at $t_2$


&#x1F64B; Note that there is something a bit bothering here.
In order to show that A and B behave the same at g, we need to find some particular times $t_1$ and $t_2$.
Our definition seems to depend on the particular times where it happens.
We'll show that it is not the case, that if they happen at two other times then they will also produce the same sensation.


Suppose A and B behave the same at g, then for any time $t_3, t_4$,
such that $L(t_3) = L(t_4) = g$, where A produce s at $t_3$ and B happens at $t_4$.
Then B also produces s at $t_4$.

__Proof__
>
Since $L(t_3)=L(t_1)=g$, A produces the same sensation s at $t_1$ and at $t_3$ (according to the repetition of sensation lemma).
Since $L(t_4)=L(t_2)=g$, B produces the same sensation s at $t_2$ and at $t_4$.  
QED



We are now equipped to prove the main result of this section,
which shows that the different ways to define movement are equivalent.

__muscular chains that produce the same movement__: 
Let A, B be two muscular chains.
The following properties are equivalent:
1. (Interior) A and B behave the same at all $g \in W'$ 
2. (Algebraic) A~B and both A and B happens at all locations
3. (Exterior) A and B happens at all locations and there exists $g \in W'$ such that:    
	Whenever A happens at n, $L(n+|A|)=g \ast L(n)$,  
    and whenever B happens at n' $L(n'+|B|)=g \ast L(n')$
    


Note: we need to stipulate that A and B happen at all locations because otherwise we might face some issues.
For example, it is possible that A or B never happens _at all_ in a particular robot's awareness,
in which case of course it is not possible to give meaning to A or B happening at the same location.

__Proof:__
>
Let's prove (2) => (1)  
Suppose A ~ B, let $g \in W'$, we'll show that A and B behave the same at g.
Since both A and B happen at all locations, we can chose $t_1, t_2 \in N$ 
such that A happens at $t_1$, B happens at $t_2$ and $L(t_1) = L(t_2) = g$.
Without loss of generality, we'll assume $t_1 \leq t_2$.  
Let M be the chain from $t_1$ to $t_2$.
Since M has to start by A, we can write $M = A \cdot M_\Delta$
Since $L(t_1) = L(t_2)$, M must be composite so:
$[Z] = \pi(M) = \pi(A \cdot M_\Delta) = \pi(A) \ast \pi(M_\Delta)$ and so
$\pi(M_\Delta) = \pi(A)^{-1}$  
Now we have $\pi(M_\Delta \cdot B) = \pi(A)^{-1} \ast \pi(B) = \pi(A)^{-1} \ast \pi(A)$.
The last equality is due to A~B.
So we have $\pi(M_\Delta \cdot B) = [Z]$ and the chain $M_\Delta \cdot B$ is composite, hence null, hence loops sensations.
But this chains goes from the end of A to the end of B, which shows that the sensations are the same at both ends.  
>
>
Now we'll prove (1) => (2)  
let's suppose that A and B behave the same at all $g \in W'$.
Clearly both A and B happen at all locations, so we just need to show that A ~ B.
Let $g \in W'$, let $t_1, t_2 \in N$ be times when A and B act the same at g.
We have $t'(L(t_1+|A|)) = t'(L(t_2+|A|))$  
$$ \iff t'(L(t_1)\ast\pi(A)) = t'(L(t_2)\ast\pi(B)) \\
\iff t'(g\ast\pi(A)) = t'(g\ast\pi(B))$$  
Here we can use the change of variable $g = h\ast \pi(A)^{-1}$:  
$$ \iff t'(h) = t'(h \ast pi(A)^{-1}\ast\pi(B)) $$  
* if $\pi(A)^{-1}\pi(B) = [G]$  
    Then we have, for all h: $t'(h) = t'(h\ast[G])$.  
    Clearly that implies that [G] is a null chain which is absurd according to our assumptions.
* if $\pi(A)^{-1}\pi(B) = [G,G]$  
    Similarly, this would imply that [G,G] is a null chain, which is also absurd.
* if $\pi(A)^{-1}\pi(B) = [G,G,G]$  
    This gives $t'(h) = t'(h \ast [G,G,G])$.
    In particular, $t'([Z]) = t'([G,G,G]) = t'([G,G]) = t'([G])$ and so t' is constant.
    Again this means that [G] is null, which is absurd.
>
So we must have $\pi(A)^{-1}\pi(B)=[Z]$, by the unicity of inverses, we must have $\pi(A) = \pi(B)$ and thus
$A \sim B$.  
>
> (2) => (3)  
> Since A ~ B, we have $\pi(A) = \pi(B)$.
This in turns implies, forall n where A happens at n, $L(n+|A|) = \pi(A) \ast L(n)$.
The same equation is true for B, and so we have (3).
>
> (3) => (2)  
Let g such that:  
Whenever A happens at n, $L(n+|A|)=g \ast L(n)$,  
and whenever B happens at n' $L(n'+|B|)=g \ast L(n')$  
We have necessarily $\pi(A) = g = \pi(B)$, and so $A \sim B$.  
QED.


What is the point of this computation?
Again this result is best understood in terms of language:

1. &#x1F440; The interior point of view is the most basic: it tells what the robot feels at which points.
    It is the most _concrete_ or sensorial in a way.
    The advantage of this language is that it doesn't need "big brain".
    You see, you touch, you observe "same": that is it.
    However, the drawback is that there is a lot of verbiage in this description.
    You need to show a lot of particular chains to transmit any message (actually an infinity), so it is not efficient.
    This is due to the fact that there are so many repetitions in the robot's awareness, and that we could "factor them out".
2. &#x1F30E; The exterior points of view is the most efficient.
    It tells exactly what is happening in the awareness of the robot in the most concise way possible.
	It does so because it completely abstract out the repetitions from its representation.
3. &#x27BF; The algebraic point of view is the glue between the two.
    The group theory that rules the algebra of muscular chains and friends (composite, null chains),
    allows one to transfer between the interior and exterior point of view.


The interior point of view is the language of raw awareness, 
the exterior point of view is the language of raw awareness without all repetitions,
the algebraic point of view is the language of the repetitions.

## Conclusion

This article was a bit painful to write, and I guess a bit painful to read as well.
A lot was said about the nature of space and language, at least for a little robot in a circle. &#x1F916;

The plan of course will be to extend this claims to our human awareness.
In particular, the spacetime appreciation blog will aim to give:

1. A definition of spacetime from what we human see, touch and feel, based on generator dances.
    These will be dances that you can actually perform, in the comfort of your own home.
2. Obtaining the algebra of the Galilean group from the repetitions in our human awareness.
    If you thought the algebra in this article was annoying, wait till you see this one, 
    it is _noticably spicier_ &#x1F336;&#x1F336;&#x1F336;.
3. Presenting group theory as a language instead of an abstract weird thing,
    and using it to answer identity questions.


In the end, we hope we can convince the reader that space is not interior or exterior.
Everything happens inside your awareness, and all languages try to describe the content of our awareness in the end.
Our awarenesses have A LOT of repetitions.
When we omit them, we get the exterior point of view.
When we include them, we get the interior point of view.
When we describe the repetitions, we get algebra.

Don't mistake the tool used to describe reality with reality itself.
There is no inside or outside.
In the end it's all patterns in your awareness.

## References

1. Cover for the article is taken from [blog.scientificamerican.com](https://blogs.scientificamerican.com/roots-of-unity/nothing-is-more-fun-than-a-hypercube-of-monkeys/)
