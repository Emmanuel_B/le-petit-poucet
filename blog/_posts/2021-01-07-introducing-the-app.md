---
layout: article
title:  "A Web Application to Move Around Hyperbolic Space"
key: "APP_INTRO_20210107"
date: 2021-01-07 16:50:47 +0000
categories: spacetime
tags: ["petit poucet app", "hyperbolic", "spherical", "geometry", "expository", "square dance"]
comment: true
license: CC-BY-4.0
mathjax: true
cover: '/assets/petit-poucet-screenshot.png'
---

What does it feel like to live in hyperbolic space?  
Here we have a simple app where you can move in different spaces (including hyperbolic, spherical, and flat geometry).
The application is minimalistic as it only shows your trace or trajectory as you move around.
Hopefully, this is enough to give you a feel of that space.



<div class="item">
  <div class="item__image">
	<a href="{{ 'dist/' | relative_url }}">
    <img class="image image--xl" src="{{ '/assets/petit-poucet-screenshot.png' | relative_url }}" />
	</a>
  </div>
  <div class="item__content">
    <div class="item__header">
      <h4>Try the app!</h4>
    </div>
    <div class="item__description">
	  <a class="button button--primary button--pill" href="{{ 'dist/' | relative_url }}">Go to app</a>
    </div>
  </div>
</div>


First we will present the application and explain how to use it.
Next we will introduce a small movement or "dance" and use it as a tool to test the different spaces, both inside and outside of the app.
In the final words, I argue that spacetime might not be as abstract as it  seem, as we do observe it.

A note about the name: "le Petit Poucet" or the "Little Thumbling" is a children story by Charles Perrault where a young kid uses small rocks to find his way into the forest, much like we use the colorful thread of the spider in the app.

## I - How to use the web app? 

At the beginning the space is empty and everything is black.
But as we move around, we trace a mark on the space that we can see.
The idea is to roam around and see how the trace changes as we move in different spaces.

### I.1 - Controls

The app main action is to __move around__, using:
* the `arrow keys`{:.info} for moving up/down and left/right
* the `a`{:.info} and `d`{:.info}  keys to turn around ourselves


Additionnally, we have some __controls__ on the left hand side menu:
-   The `Reset`{:.warning} button allows to clear the trace.
-   The `Change`{:.warning} button allows to change the space we are currently on.

### I.2 - Available spaces

#### Sphere 


<div class="item">
  <div class="item__image">
	<a href="https://en.wikipedia.org/wiki/Spherical_geometry">
    <img class="image image--sm" src="{{ '/assets/spherical.webp' | relative_url }}" />
	</a>
  </div>
  <div class="item__content">
    <div class="item__description">
        <ul>
<li>As the name suggests, this is just the geometry you would encounter if you were actually living on a ball</li>
<li>It is "bounded": going very far in any direction for about 30 seconds, you should come back where you came from.</li>
<li>Try to make parallel lines, is it possible?</li>
        </ul>
    </div>
  </div>
</div>

#### Flat Plane



<div class="item">
  <div class="item__image">
	<a href="https://en.wikipedia.org/wiki/Euclidean_geometry">
    <img class="image image--sm" src="{{ '/assets/flat.webp' | relative_url }}" />
	</a>
  </div>
  <div class="item__content">
    <div class="item__description">
        <ul>
<li> This is the usual geometry of a flat plane, as we experience for example on flat soccer field.</li>
<li> It is unbounded: we can go forward infinitely without coming back on our tracks.</li>
<li>
Try to make parallel lines, observe that it is possible and that there is a single parallel to line passing by a given point.
	
	This is also called Euclid parallel's postulate. 
</li>
        </ul>
    </div>
  </div>
</div>


#### Hyperbolic Plane



<div class="item">
  <div class="item__image">
	<a href="https://en.wikipedia.org/wiki/Euclidean_geometry">
    <img class="image image--sm" src="{{ '/assets/hyperbolic.webp' | relative_url }}" />
	</a>
  </div>
  <div class="item__content">
    <div class="item__description">
        <ul>
<li>
 Hyperbolic plane looks everywhere like the middle of a saddle (pictured left).
	However it does not globally look like a saddle, actually there is no surface that we can picture in our geometry that has the shape of hyperbolic space.
</li>
<li>
	It is unbounded: going forward, you never come back where you started no matter how long you go.</li>
<li>
 Try to make parallel lines 
</li>
<li>
 Make two different parallel lines going through the same point, showing that Euclid parallel's postulate fails in this geometry
</li>
        </ul>
    </div>
  </div>
</div>
## II - Application: Making a square

A question which might be of interest, would be to determine the type of space we are in (spherical, flat or hyperbolic) without looking at the menu.
There are of course many ways to do that, as the spaces are quite different, but here we will propose a simple solution, based on a little square "dance".

### II.1 - The square in the app

You can make a square in any of the proposed spaces in the following way:

<div class="item">
  <div class="item__image">
	<a href="{{ 'dist/' | relative_url }}">
    <img class="image image--l" src="{{ '/assets/spherical-square.gif' | relative_url }}" />
	</a>
  </div>
  <div class="item__content">
    <div class="item__header">
      <h4>How to make a square</h4>
    </div>
    <div class="item__description">
		<ol>
<li> Go forward (up arrow)</li>
<li> Go right (right arrow)</li>
<li> Go backward (down arrow)</li>
<li> Go left (left arrow)</li>
		</ol>
Each movement can be relatively small, like a few seconds each.
    </div>
  </div>
</div>



What do you observe? It depends on the space you are in:

*  _spherical geometry_ -> you end up facing _inside the square_
*  _flat geometry_ -> you end up right where you started
*  _hyperbolic geometry_ -> you end up facing _outside the square_

<div class="grid-container">
  <div class="grid grid--p-3">
    <div class="cell cell--4"><div>
    	<img class="image image--l" src="{{ '/assets/spherical-square.png' | relative_url }}" />
		<p><i>Spherical</i></p>
	</div></div>
    <div class="cell cell--4"><div>
    	<img class="image image--l" src="{{ '/assets/flat-square.png' | relative_url }}" />
		<p><i>Flat</i></p>
	</div></div>
    <div class="cell cell--4"><div>
    	<img class="image image--l" src="{{ '/assets/hyperbolic-square.png' | relative_url }}" />
		<p><i>Hyperbolic</i></p>
	</div></div>
  </div>
</div>

The flat geometry example is just what you would expect intuitively.
After finishing the square, we are facing exactly in the same direction that we started with.  
In spherical geometry, when we come back after the square, we have also rotated towards the inside of the square.
Correspondingly, hyperbolic geometry rotates in the other direction.

A __technical note__ for mathematicians (feel free to skip this):  
The "square dance" is related to the __commutators__ of the underlying Lie group of each geometry.
Writing $$T_x$$ for the translation in the x-axis, and $T_y$ for the y-axis translation, the orientation at the end of the square dance is given by $T_xT_yT_x^{-1}T_y^{-1}$.  
Considering smaller and smaller translations, then passing to the limit, we can also show that this information is  contained in the _lie bracket_ $\[D_x, D_y\]$, where $D_x$ and $D_y$ are the Lie vectors corresponding to translation in the x and y direction respectively.  
So there is a direct link between this "square dance", and the structure coefficients of the underlying Lie algebra, which define the geometry (up to a few details), according to the [Lie group-Lie algebra correspondence](https://en.wikipedia.org/wiki/Lie_group%E2%80%93Lie_algebra_correspondence#Proof_of_Lie's_third_theorem`) and the [Erlangen program](https://en.wikipedia.org/wiki/Klein_geometry).
{:.info}

So this simple square dance is a convenient tool to determine in which geometry we are in.
But we can also do it in our space time, what does it reveal then?

### II.2  - The square in real life

I invite you to get up of your chair, finding a suitable area, for example facing straight at a wall, and then doing the four steps of the square outlined above.
Of course, when you are finished, you observe that you are facing straight at the wall again.
This observation has nothing stupid, as we've seen, in different geometries, we would have rotated at the end of the square, so that we would be facing the wall with an angle.

In other words, by doing this simple square dance, we have observed something of our spacetime.
Mathematicians call this property we have juste observed "flatness".  
A big part of science is putting complicated names on simple phenomena.
Now you can impress your friends too; instead of saying "moving in a square" you can say "performing an experience of the flatness of spacetime".


## Final words: can we look at spacetime? 

Of course when we look around, we can see events or objects _in spacetime_, but we can we see spacetime _itself_?  
When you do the "square dance", you observe coming back exactly where you started, which is something very special of our geometry.
We can argue that this is indeed "looking at spacetime".  
Perhaps it would be better to say "perceiving spacetime" rather than looking because the "square dance" experiment involves moving and looking, so that the perception of spacetime requires both visual as well as muscular sensations.


Do let me know your thoughts and if any cool things/issues happenned with the app!

Hyperbolic bye!!
![Hyperbolic bye]({{ '/assets/hyperbolic-bye.png' | relative_url }})

