---
layout: article
title:  "On the Beach of the Acid Shore"
date: 2021-03-17 12:00:00 +0000
categories: time
tags: identity expository objects poetry time
license: CC-BY-4.0
cover: 'assets/on-the-beach/great-wave.jpg'
key: "ACID_BEACH_20210317"
comment: true
---


About geometry and identity :)

<div style="width:80%; margin:0 auto;" align="center" markdown="1">
![wave]({{ '/assets/on-the-beach/great-wave.jpg' | relative_url }})
</div>

&#x1F4D0; In school we learn that the two fundamental elements of geometry are the point and the line.
The point is the smallest conceivable entity, it cannot be divided.
The line is made of points and repeats infinitely in a single direction.


This is true in 2 dimensions, yet we live in a 4 dimensional space-time.
In our space-time, the equivalent of points are events, they are small and indivisible elements in both the space and time dimensions.
An event is “something” that happens at a specific place and at a specific time.  
In our space-time, the equivalent of the line is the object.
We can observe the object at many different times, each such observation is an event.  
The object is made up of those events just like the line is made of points.  
The object repeats itself infinitely in the time direction.  
The object is the line of time.

&#x1F30A; I went to the beach on the acid shore.
I saw time and space deconstruct, decompose into shapes, colours and sounds.
I lost the sense of who I was, lost in an endless present, cut from any history and timeline.
Each instant was like an absurd and infinite island.

The incredible intensity of life.

People say that living in the present moment is the best feeling in the world, but that is only half true.
It is also terrifying, because when you are lost in time it is not clear anymore that you are safe.
There is no pause in life, the body needs to be protected at each instant.

The body is the first object.

I had to bring back something from that beach, otherwise nobody would believe me, even the future “me”.
So I picked up a seashell and I looked at it in full detail.
It is mostly beige, but it almost has a black line, barely visible.
It makes it unique, recognizable, it makes me feel safe.
It reminds me of my promise to protect my body.
 
Each time I looked at the seashell, I was surprised to see the same details.
Surprised by the incredible precision in that repetition.  
So something existed.  
And if something existed, I, as the one who recognizes it, must exist too?

I feel like I exist in time because I recognize objects.



&#x1F913; Here is what I wrote on this beach:
“You got it, your precious.
You touch it and it makes you feel safe.
One day you will return it, like the sea retreats at low tide.
You will give away your totem.
You will give up this easy comfort, and you will be cold and you will be scared.
And then you won’t be scared, because in this chaos of lights and shadows, of shapes and colors you will recognize your home, you.
You are always home, it is always now.
You do not exist.”

There are no good and bad moments, good or bad thoughts.
Each instant is perfect and intense.
It is impossible to be away from life.
Each instant is life.

I made my promise.
One day, I will return my seashell.
I will come back to the place I have never left, the place where I belong.
I will come back on the beach of the acid shore.


##### _Update history_

*   Edited 06/2022: removed fear paragraph + minor edits
*   _Edited 29/03_: minor edits

