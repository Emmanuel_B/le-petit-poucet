---
layout: article
title:  "A Finite Group Imitating 1+1 Galilean Group"
date: 2021-11-02 16:50:47 +0000
key: "FINITE_GAL_20220118"
comment: false
mermaid: true
mathjax: true
mathjax_autonumber: true
categories: mathematics 
license: true
cover: 'assets/symmetric-abstract-pattern.png'
published: false
aside:
    toc: true
tags: ["group theory", mathematics, exercise, "galilean group"]
---

Freeman Dyson said that "particle physics emerge as the playground of group theory".
So if, like me, you have started studying groups with an eye to physics,
you might have found yourself lost in a pretty dry land of mathematics.
So here is an exercise about a 27 elements finite group that illustrate some of the properties of the 
[Galilean group](https://en.wikipedia.org/wiki/Galilean_transformation)
the group of our spacetime, limited here to 1 dimension of space and 1 of time.

The exercise comprises normal subgroups, extensions, semi-direct products, extensions and Fourier transform for finite groups.
I'll try to show the links with physics as much as I can, but the real goal here is just to get some practice in group theory.

## Part I: Definition

We're only going to be interested in 1 dimension of space and 1 dimension of time.
We will also have galilean boosts.

Let $\mathfrak{d, h, b}$ be the generators of spatial translations, time translations and boosts respectively, for the Galilean group.

We know that these elements verify:
$$
[\mathfrak{d}, \mathfrak{h}] = 0 \\
[\mathfrak{d}, \mathfrak{b}] = 0 \\
[\mathfrak{b}, \mathfrak{h}] = \mathfrak{d} 
$$

Translating these relationships to casual language:
1. First relation says that moving on x-axis then waiting is the same as waiting then moving on x-axis.
2. Second relation says that moving on x-axis then accelerating (x-axis) gets you to the same point as first accelerating then moving.
3. Third relation says that if you accelerate first then wait, you get further away than if you wait then accelerate

So ya let's cook up a finite group with the same relationships:


$$
[D, H] = 0 \\
[D, B] = 0 \\
[B, H] = D
$$

The 
[commutator](https://en.wikipedia.org/wiki/Commutator) for finite groups reads: $[D, H] = DHD^{-1}H^{-1}$.
So we get the following relations:


$$
DH = HD \\
DB = BD \\
BH = DHB
$$

Note that at this point our group contains $D^k$ for all k in $\mathbb{Z}$, so it is not finite.
In order to make it finite, let's add the condition $D^3=B^3=H^3=e$.

So here we have the definition of our toy group, given by a generators and relations: $G = <D,H,B / D^3, H^3, B^3, BH=DHB>$.

If you need a bit more intuition about this group, we can realize it as an action of the set $\mathbb{Z}/3 \times \mathbb{Z}/3$.
Elements of this set can be viewed as finite versions of 2D spacetime diagrams.
We'll put space in the first dimension and time in the second one, correspondingly we'll note $(x,t)$ for an element in $\mathbb{Z}/3 \times \mathbb{Z}/3$

$$D \cdot (x, t) = (x+1, t) \\
H \cdot (x, t) = (x, t+1) \\
B \cdot (x, t) = (x+t, t)
$$ 

We can check that these operators verify the relations given above.
Which makes it a valid set action of our group G.


We can interpret these equations physically by supposing that as an observer in spacetime you observe $f: \mathbb{Z}/3 \times \mathbb{Z}/3 \rightarrow \mathbb{R}$.
That is, at point x and at time t, you observe values $f(x,t)$.

Then we obtain that:
- An observer translated -1 unit away from you in x-axis will observe $f(D \cdot (x,t))$
- An observer with its clock 1 time unit before you will observe $f(H \cdot (x,t))$
- An observer going with a negative speed of 1, and passing by the point (0, 0) will observe $f(B \cdot (x,t))$

So the operators do correspond to change of observers!

We'll note $\mathcal{F}$ the complex vector space of complex valued functions on $\mathbb{Z}/3 \times \mathbb{Z}/3$.
$\mathcal{F}$ is a G-representation by $g \cdot f  (x,t) = f(g^{-1} \cdot (x,t))$.
Alternatively, $\mathcal{F}$ can de defined as the permutation representation on $\mathbb{Z}/3 \times \mathbb{Z}/3$.


Having defined our group, we turn to actually getting to know it a little.
How many elements does it have, what is its composition structure?

## Part II: Element information

__Exercise__: Show that the sequence $0 \rightarrow <D, H> \rightarrow G \rightarrow \<B\> \rightarrow 0$ is exact and right-split.
Alternatively if you don't like the language of exact sequences, show that G is a semi-direct product of $<D, H>$ and $\<B\>$.

Since D and H commute, we now know that $\lt D, H \gt \sim \mathbb{Z}/3 \times \mathbb{Z}/3$, and together with the previous exercise this gives us that G has 27 elements.

__Exercise__: Show that $Z(G) = \langle D \rangle$ and deduce the normal series: 
$0 \lhd \langle D \rangle \lhd \langle D, H \rangle \lhd G$

__Exercise__: Prove that $G/{Z(G)} \sim \mathbb{Z}/3\times \mathbb{Z}/3$

__Hint__: There are only 2 groups of order 9: $Z/9$ and $Z/3 \times Z/3$.

__Exercise__: Find all the conjugacy classes of G.

__Answer__: There are 11 classes:
* 3 classes of size 1, given by representants $e, D, D^{-1}$
* 8 classes of size 3, given by representants $H, H^{-1}, B, HB, H^{-1}B, B^{-1}, HB^{-1}, H^{-1}B^{-1}$

## Part 3 : Representation theory

__Exercise__: Compute the full character table of G.

__Hints__: G has 11 conjugacy classes so we know it has 11 irreducible representations.
9 of them can be obtained by computing the representations of $G/{Z(G)} \sim \mathbb{Z}/3\times \mathbb{Z}/3$ and pulling them back to G.
Using the orthogonality relationships, we can deduce that the 2 remaining irreps have both dimension 3.
Let's call them V and V', let's note $\rho: G \rightarrow \text{GL}(V)$ the representation.
D acts on V and has 3 eigenvectors. Considering the actions of V and V' on these, conclude that they must all have equal eigenvalues.
So $\rho(D) =  \lambda I_3$, we must also have $\lambda ^ 3 = 1$ so we can pick $\rho(D) = \omega I_3$.
We can now consider the 3 eigenvector of H on V and how B must act on them,
from which we can shows that all the eigenvalues of H must be distincts and that B permutes H's eigenvectors.
Accordingly, we can choose:

$$\rho(H) = 
\begin{pmatrix} 
1 & 0 & 0 \\
0 & \omega & 0 \\
0 & 0 & \omega^{-1} \end{pmatrix}$$ 

and

$$
\rho(B) = \begin{pmatrix}
0 & 1 & 0\\
0 & 0 & 1\\
1 & 0 & 0 \end{pmatrix}
$$

We can check that the above matrices verify the relations of G, which makes it a valid representation.
Computing the trace of every conjugacy class, we verify that $\langle \chi_V, \chi_V \rangle = 1$ which shows that it is irreducible.

Finally, using the orthogonality relationships, we can find the last row.

__Character table of G__:


|                	| 1 	| 1         	| 1         	| 3        	| 3        	| 3        	| 3        	| 3         	| 3        	| 3         	| 3              	|
|----------------	|---	|-----------	|-----------	|----------	|----------	|----------	|----------	|-----------	|----------	|-----------	|----------------	|
| G              	| e 	| D         	| $D^{-1}$  	| H        	| $H^{-1}$ 	| B        	| BH       	| $BH^{-1}$ 	| $B^{-1}$ 	| $HB^{-1}$ 	| $H^{-1}B^{-1}$ 	|
| $\chi_{0,0}$   	| 1 	| 1         	| 1         	| 1        	| 1        	| 1        	| 1        	| 1         	| 1        	| 1         	| 1              	|
| $\chi_{0,1}$   	| 1 	| 1         	| 1         	| 1        	| 1        	| w        	| w        	| w         	| $w^{-1}$ 	| $w^{-1}$  	| $w^{-1}$       	|
| $\chi_{0,-1}$  	| 1 	| 1         	| 1         	| 1        	| 1        	| $w^{-1}$ 	| $w^{-1}$ 	| $w^{-1}$  	| w        	| w         	| w              	|
| $\chi_{1,0}$   	| 1 	| 1         	| 1         	| w        	| $w^{-1}$ 	| 1        	| w        	| $w^{-1}$  	| 1        	| w         	| $w^{-1}$       	|
| $\chi_{1,1}$   	| 1 	| 1         	| 1         	| w        	| $w^{-1}$ 	| w        	| $w^{-1}$ 	| 1         	| $w^{-1}$ 	| 1         	| w              	|
| $\chi_{1,-1}$  	| 1 	| 1         	| 1         	| w        	| $w^{-1}$ 	| $w^{-1}$ 	| 1        	| w         	| w        	| $w^{-1}$  	| 1              	|
| $\chi_{-1,0}$  	| 1 	| 1         	| 1         	| $w^{-1}$ 	| w        	| 1        	| $w^{-1}$ 	| w         	| 1        	| $w^{-1}$  	| w              	|
| $\chi_{-1,1}$  	| 1 	| 1         	| 1         	| $w^{-1}$ 	| w        	| w        	| 1        	| $w^{-1}$  	| $w^{-1}$ 	| w         	| 1              	|
| $\chi_{-1,-1}$ 	| 1 	| 1         	| 1         	| $w^{-1}$ 	| w        	| $w^{-1}$ 	| w        	| 1         	| w        	| 1         	| $w^{-1}$       	|
| $\chi_V$       	| 3 	| 3w        	| $3w^{-1}$ 	| 0        	| 0        	| 0        	| 0        	| 0         	| 0        	| 0         	| 0              	|
| $\chi_V'$       	| 3 	| $3w^{-1}$ 	| 3w        	| 0        	| 0        	| 0        	| 0        	| 0         	| 0        	| 0         	| 0              	|


The actual operators of the representations are given by the trace itself for the 9 unidimensional irreps.
For V, we can take the matrices given in the hints of the previous exercise.

For V' we can use the following matrices:

$\rho_V'(D) = \omega^{-1} I_3$.

$$\rho_V'(H) = 
\begin{pmatrix} 
1 & 0 & 0 \\
0 & \omega & 0 \\
0 & 0 & \omega^{-1} \end{pmatrix}$$ 

and

$$
\rho_V'(B) = \begin{pmatrix}
0 & 0 & 1\\
1 & 0 & 0\\
0 & 1 & 0 \end{pmatrix}
$$


__Exercise__ Decompose $\mathcal{F}$ in irreps and give a basis compatible with the decomposition.

__Hints__
Using character theory we find that $\mathcal{F} = U_{0,0} \oplus U_{1,0} \oplus U_{-1, 0} \oplus V \oplus V'$.
Let $$(f_{ij})_{i,j \in \mathbb{Z}/3}$$ be a basis of $\mathcal{F}$ given by $f_{ij}(i, j) = 1$ and 0 everywhere else.
then $g \in G$ acts by $g \cdot f_{ij} = f_{g \cdot (i,j)}$

Let $f \in \mathcal{F}$, we can conveniently arrange its values in a 3x3 matrix:
$$
f = \begin{pmatrix}
f(-1,1) & f(0,1) & f(1,1) \\
f(-1,0) & f(0,0) & f(1,0) \\
f(-1,-1) & f(0,-1) & f(1,-1) 
\end{pmatrix}
$$

_Reminder: projection formula_  Let W be an irrep., we have:

$$\pi_W = {\mathrm{dim} W \over |G|} \sum_{g \in G} \overline{\chi_W(g)} \cdot g :
 \mathcal{F} \rightarrow \mathcal{F}$$

Where by g we mean $\rho(g) : \mathcal{F} \rightarrow \mathcal{F}$


Using the projection formulas, we obtain the following basis for the decomposition into irreps:
* $$u_{0,0}= {1 \over 9} \begin{pmatrix}
1 & 1 & 1 \\
1 & 1 & 1 \\
1 & 1 & 1
\end{pmatrix}$$

* $$ u_{-1,0} = {1 \over 9} \begin{pmatrix}
\omega & \omega & \omega \\
\omega^{-1} & \omega^{-1} & \omega^{-1} \\
1 & 1 & 1
\end{pmatrix}
$$

* $$ u_{1,0} = {1 \over 9} \begin{pmatrix}
\omega^{-1} & \omega^{-1} & \omega^{-1} \\
\omega & \omega & \omega \\
1 & 1 & 1
\end{pmatrix}
$$

* $$v_{-1} = {1 \over 3} \begin{pmatrix}
0 & 0 & 0 \\
0 & 0 & 0 \\
1 & \omega^{-1} & \omega	
\end{pmatrix}, 
v_0 = {1 \over 3} \begin{pmatrix}
0 & 0 & 0 \\
1 & \omega^{-1} & \omega \\
0 & 0 & 0 
\end{pmatrix},
v_1 = {1 \over 3} \begin{pmatrix}
1 & \omega^{-1} & \omega \\
0 & 0 & 0 \\
0 & 0 & 0 
\end{pmatrix} $$	


* $$v'_{-1} = {1 \over 3} \begin{pmatrix}
0 & 0 & 0 \\
0 & 0 & 0 \\
1 & \omega & \omega^{-1}
\end{pmatrix}, 
v'_0 = {1 \over 3} \begin{pmatrix}
0 & 0 & 0 \\
1 & \omega & \omega^{-1} \\
0 & 0 & 0 
\end{pmatrix},
v'_1 = {1 \over 3} \begin{pmatrix}
1 & \omega & \omega^{-1} \\
0 & 0 & 0 \\
0 & 0 & 0 
\end{pmatrix} $$	


We can use the projection formula again to decompose an arbitrary function into its symmetric components:
* $p_{0,0}(f) = \sum_{i,j} f(i,j) \cdot u_{0,0}$
* $p_{-1,0}(f) = \omega \sum_{i,j} \omega^{j}f(i,j) \cdot u_{-1,0}$
* $p_{1,0}(f) = \omega^{-1} \sum_{i,j} \omega^{-j}f(i,j) \cdot u_{1,0}$
* 
    $ p\_V(f) = \left \[f(-1,-1)+\omega f(0,-1) + \omega^{-1} f(1, -1) \right \] \cdot v_{-1} + \\
  \left \[f(-1, 0)+\omega f(0,0) + \omega^{-1} f(1, 0) \right \] \cdot v_0 + \\
  \left \[f(-1,1)+\omega f(0,1) + \omega^{-1} f(1, 1) \right \] \cdot v_1 $
* 
    $ p_V'(f) = \left \[f(-1,-1)+\omega^{-1} f(0,-1) + \omega f(1, -1) \right \] \cdot v'_{-1} + \\
  \left \[f(-1, 0)+\omega^{-1} f(0,0) + \omega f(1, 0) \right \] \cdot v'_0 + \\
  \left \[f(-1,1)+\omega^{-1} f(0,1) + \omega f(1, 1) \right \] \cdot v'_1 $

TODO: write some python code to verify those formula.
- Actually it would be cool to have some python code to do the projection numerically, so that I can quickly check what is looks like
    Then the rest is just recombining.
	
