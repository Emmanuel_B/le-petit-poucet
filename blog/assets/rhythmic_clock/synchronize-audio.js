let currentHour = null;
    let firstRunTesting = false; // set to true to test the hourly transitions

function fetchAudio(url) {
    return fetch(url)
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.blob();
        })
        .then(blob => {
            const blobUrl = URL.createObjectURL(blob);
            return blobUrl;
        })
        .catch(error => {
            console.error('Fetch error:', error);
        });
}

async function synchronizeAudio(baseUrl, initial = false) {
    const audio = document.getElementById('synchronizedAudio');
    const source = document.getElementById('audioSource');

    // Get the current time
    const now = new Date();
    let newHour = now.getHours() % 12; // Get the current hour in 12-hour format
    if (firstRunTesting) {
        newHour = 0;
    }
    const currentMinute = now.getMinutes();
    const currentSecond = now.getSeconds();

    // Calculate the current time in seconds within the hour
    const currentTimeInSeconds = (currentMinute * 60) + currentSecond;

    if (initial || newHour !== currentHour) {
        currentHour = newHour;

        // Construct the full URL for the current hour's audio file
        const audioUrl = `${baseUrl}clock-hour-${currentHour}.mp3`;
        console.log("Constructed audio URL:", audioUrl); // Debug statement

        // Fetch the audio file as a Blob and set it as the source
        const blobUrl = await fetchAudio(audioUrl);
        audio.src = blobUrl;

        // When the metadata is loaded, set the current time
        audio.onloadedmetadata = () => {
            audio.currentTime = currentTimeInSeconds;
            if (!initial) {
                audio.play();
            }
        };
    } else {
        // Only update the current time if the hour hasn't changed
        audio.currentTime = currentTimeInSeconds;
    }
};

function register(baseUrl) {
    document.addEventListener('DOMContentLoaded', (event) => {
        const playPauseButton = document.getElementById('playPauseButton');

        // Synchronize audio initially
        synchronizeAudio(baseUrl, true);

        // Handle play/pause button click
        playPauseButton.addEventListener('click', () => {
            const audio = document.getElementById('synchronizedAudio');
            if (audio.paused) {
                // Resynchronize before playing
                synchronizeAudio(baseUrl);
                audio.play();
                playPauseButton.textContent = '❚❚ Pause';
            } else {
                audio.pause();
                playPauseButton.textContent = '▶ Play';
            }
        });

        // Check the hour periodically and resynchronize if needed
        setInterval(() => {
            firstRunTesting = false;
            const now = new Date();
            const newHour = now.getHours() % 12;
            if (newHour !== currentHour) {
                const audio = document.getElementById('synchronizedAudio');
                if (!audio.paused) {
                    synchronizeAudio(baseUrl);
                    audio.play();
                }
            }
        }, 2000); // Check every 2 seconds
    });
}
