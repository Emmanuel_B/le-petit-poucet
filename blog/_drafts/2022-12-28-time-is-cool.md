---
layout: article
title:  "Time to Talk About Time"
date: 2022-12-28 16:50:47 +0000
categories: time
tags: expository philosophy spacetime nonduality
comment: true
published: true
---

Here I want to talk about the relation between natural selection and time.
Also about how time affects pretty much everything in our lives.

## Time and Natural Selection

Natural selection is "survival of the fittest".

What exactly means "the fittest"?
Well it depends on the situation, it could be the strongest, the fastest,
the tallest, or it could be the most energy efficient, the best in hiding,
or the most fertile.

Most probably it is a combination of all of those things.

In essence, the most fit means "the most likely to survive in the current situation."

So we have now rephrased the law of natural selection: "survival of the most likely to survive",
seems pretty much tautological, isn't it?

And survival basically means: to keep existing in time.

So we see that time has a very special role in natural selection.

What's more is that survival is not just survival of the individual,
but more survival of the genes, which include survival of the children.

Natural selection has defined our body.
The way it defined our body is through basically trying different things and pruning off the ones who don't work.
Why do we have 2 arms?
Well there might have beens humans with 3, but they all died.
That is time ended them.
We have 2 arms because that's the only thing that time allowed.

All our body is defined by the fact that it passed the test of time.

The same with our emotions, and our perceptions, there were all molded by time.

You feel fear when you are on top of a high bridge because that's what time allowed you to feel.
There were other humans who felt very happy and relaxed at the top, and they fell.
Time did not reproduce them.
At the origin there was no relation whatsoever between fear and heights.
Time created that relation.
Time made you feel that.

Why do you feel horny sometimes?
There were humans that never felt that, they didn't reproduce, and so they died without childrens.
Horniness exists because time deleted non-horniness.

So the body shape, our perception, fear, horniness, sex are all children of time.
We are children of time.

## Other Time Things

In the common perception of things, the world is made of a stage we call space,
and then there are objects/shapes/people who interact on that stage.

This is not the most advanced description that we have,
just perhaps the most common one.

What is an object?
An object is a shape that repeats in time.

Most of our language is constructed from concrete nouns,
which denotes objects.

Our language is linear,
it is made of words and they come one after the other.
The linearity of language is nothing else than the linearity of time.

What is a common society/personal goal?
Progress.
That is a positive evolution in time according to some criterium.

What is the biggest fear?
Death.
That is the end of your personal time.

What is the most forbidden act?
Killing someone.
That is limiting someone's living time.

## Conclusion

Time defined our body, our emotions, it is behind our laws, fears, hopes and goals.
Everything is done in time, by time and for time.

Time, what kind of God are you?

