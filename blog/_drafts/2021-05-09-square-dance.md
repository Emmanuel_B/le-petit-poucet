---
layout: article
title:  "Square Dance"
date: 2021-05-09 16:50:47 +0000
categories: space
comment: true
---

## Do the square

I suggest practicing the following simple "line dance":

1. Walk 1 step forward
2. Walk 1 step right
3. Walk 1 step back
4. Walk 1 step left

Surprisingly, this dance doesn't work in all spaces.
In some spaces doing this dance will make you come back rotated, or make you come back at a different point than you started.

Feel free to check on the app that the square only works in the flat space.

In our spacetime we can also do the square (by actually moving our body) and you will notice that you do come back exactly where you started.
It might seem obvious, but it is actually an experiment that you are doing.
Everytime you do a square and come back the same way you start, you are observing the flatness of our spacetime.

When you do a square, you observe spacetime.

There are many other "dances" that allows you to characterize our space time, and we will talk about them in future articles.

### What is spacetime?

The [Merriam-Webster dictionary](https://www.merriam-webster.com/dictionary/space-time) gives us two alternative definitions:

1. A system of one temporal and three spatial coordinates by which any physical object or event can be located

2. The whole or a portion of physical reality determinable by a usually four-dimensional coordinate system 

The first definition is abstract, referring to a system that describes reality.
It is not related directly to what we see, hear and touch, it's more like a bookkeeping system that allows us to organize our perceptions.

The second definition identifies spacetime with "a piece of reality", a kind of container, in which different events, objects, and even ourselves, appear.

I would like to introduce a different way of seeing spacetime:

3. A specific pattern within our perception

In order to justify this point of view, we will revisit the square dance example one more time, reformulating it in terms of perceptions.

At the start point, we have both a given visual sensation and a muscle sensation.
The visual sensation is the one of the wall (assuming you are doing it facing a wall), while the muscle sensation is the one of standing upright.
Let's write W for the "looking at the wall" sensation and S for the "standing upright" sensation.
When we do the first step, our muscle sensations change, we have the feeling of our leg moving for example.
Our visual sensations also change, sure we are still facing the wall, but from a different angle, and from a different view point, so that the exact visual sensations are noticably different.

What about at the end of the square?
Let's write W' for the visual sensations then, and S' for the muscular sensations.
Of course, at the end of the square, we are back where we came from, looking at the wall from the same place, so that W'=W.
We are also standing so that S'=S.
Finally at the end of the square, all our physical sensations are back to where they were.
Looking at our sensations, this little square stands out as a pattern, linking muscular and visual sensations.
We can reformulate the flatness of space, as, "when the muscle sensations of going in a square occurs, the visual sensations return to their initial state".

As we have seen playing with the "petit poucet app" this pattern would not appear if we were living, say, in hyperbolic geometry.
So this pattern gives us information about our spacetime.

Now it is easy to imagine other dances, and to imagine that those dances also convey information about our spacetime.
What I propose then, is to collect all those patterns together, and call that "spacetime".
Much of this blog will be dedicated to describing precisely those patterns, and how they relate to the more familiar notions of spacetime we have.
