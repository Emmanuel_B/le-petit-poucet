.PHONY: help new_post develop build clean

OUT_DIR=./public

env-%:
	@ if [ "${${*}}" = "" ]; then \
		echo "Environment variable $* not set"; \
		exit 1; \
	fi

help: 		## Show this help.
	@echo "Usage: make [target] ..."
	@echo ""
	@echo "Available targets:"
	@egrep -h '\s##\s' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m  %-30s\033[0m %s\n", $$1, $$2}'

new_post: 	## Create a new blog post with the provided title.
	@echo "Enter the post title:"; \
	read TITLE; \
	FILENAME=`date +%Y-%m-%d`-`echo $$TITLE | tr " " "-"`.md; \
	echo "Creating new post in _posts/$$FILENAME"; \
	echo "---" > blog/_posts/$$FILENAME; \
	echo "layout: article" >> blog/_posts/$$FILENAME; \
	echo "title: $$TITLE" >> blog/_posts/$$FILENAME; \
	echo "date: `date +'%Y-%m-%d %H:%M:%S'`" >> blog/_posts/$$FILENAME; \
	echo "categories: " >> blog/_posts/$$FILENAME; \
	echo "tags: " >> blog/_posts/$$FILENAME; \
	echo "---" >> blog/_posts/$$FILENAME

develop:	## Start a livereload server for local development
	docker compose watch blogdev petit_poucet_dev

test:		## Run js tests
	docker compose run petit_poucet_dev npm run test:coverage

build: env-OUT_DIR	## Build both the app and the blog
	@echo "Building blog to ${OUT_DIR}"
	docker compose build build_blog build_petit_poucet petit_poucet_dev
	docker compose run build_blog
	@echo "Building app to ${OUT_DIR}/dist"
	OUT_DIR=${OUT_DIR}/dist docker compose run build_petit_poucet

clean: env-OUT_DIR
	rm -rf ${OUT_DIR}
