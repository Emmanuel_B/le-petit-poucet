# galilean.py
# Definition of Galilean group in 1+1 dimensions.

import numpy as np

class Galilean1p1:
    def __init__(self, x,t,v):
        self.x = x
        self.t = t
        self.v = v

    def __eq__(self, right):
        if self.x == right.x and self.t == right.t and self.v == right.v:
            return True
        else:
            return False

    @staticmethod
    def identity():
        return Galilean1p1(0,0,0)

    def inv(self):
        return Galilean1p1(
                -self.x-self.t*self.v,
                -self.t,
                -self.v
                )

    def copy(self):
        return Galilean1p1(self.x, self.t, self.v)

    def __mul__(self, right):
        """ Group multiplication """
        return Galilean1p1(
                self.x+right.x - right.t*self.v,
                self.t+right.t,
                self.v+right.v
                )

    def __power__(self, n):
        """ Elevate to nth power. """
        base_element = self
        if n == 0:
            return Galilean1p1.identity()
        elif n==1:
            return self.copy()
        elif n < 0:
            # for negative n we compute the -nth power of the inverse
            base_element = self.inv()
            return base_element**(-n)
        elif n>1:
            return self * (self**(n-1))

    def act_point(self, x,t,v):
        """ Action on a particle at pos x, time t speed v.
        Action is such that:
            D . (x,t,v) = (x+1,t,v)
            H . (x,t,v) = (x, t+1, v)
            B . (x,t,v) = (x,t,v+1)
        """
        return (self.x+x+self.t*(self.v+v), self.t+t, self.v+v)

    def act_event(self, x,t):
        """ Action on an event at pos x, time t.
        Action is such that:
            D . (x,t) = (x-1,t)
            H . (x,t) = (x, t-1)
            B . (x,t) = (x-t,t)
        """
        return (-self.x+x-self.v*t, -self.t+t)

    def mat_event(self):
        """ Explicits the group element as a 3x3 matrix.
        This representation as a subgroup of GL(3) makes the natural action of GL(3) on
        vectors (x t 1) act like `act_event`.

        Returns:
            A 3x3 matrix as a np.array.
        """
        mat_D = np.array([
                [1, 0, -self.x],
                [0, 1, 0],
                [0, 0, 1]
        ])
        mat_H = np.array([
                [1, 0, 0],
                [0, 1, -self.t],
                [0, 0, 1]
        ])
        mat_B = np.array([
                [1, -self.v, 0],
                [0, 1, 0],
                [0, 0, 1]
        ])
        return mat_D @ mat_H @ mat_B

    def mat_linear_motion(self):
        """ Matrix rep. for the action on linear trajectories.

        A linear trajectory f(t)=av+b can be expressed as a vector (b a 1).
        The transformation of the linear trajectory can then be expressed as a 3x3
        matrix.
        """
        return np.matrix([
            [1, self.t, -self.v*self.t-self.x],
            [0, 1, -self.v],
            [0,0,1]
            ])

    def __str__(self):
        return f"D^{self.x}H^P{self.t}B^{self.v}"

    def __repr__(self):
        return str(self)
