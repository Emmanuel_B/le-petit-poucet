{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Visualization of $\\mathfrak{so}_3$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Basic facts\n",
    "\n",
    "#### The (Dx, Dy, L) basis\n",
    "\n",
    "$$\n",
    "D_x = \\begin{pmatrix} 0 & 0 & 1 \\\\ 0 & 0 & 0 \\\\ -1 & 0 & 0 \\end{pmatrix},\n",
    "D_y = \\begin{pmatrix} 0 & 0 & 0 \\\\ 0 & 0 & 1 \\\\ 0 & -1 & 0 \\end{pmatrix},\n",
    "L = \\begin{pmatrix} 0 & -1 & 0 \\\\ 1 & 0 & 0 \\\\ 0 & 0 & 0 \\end{pmatrix}\n",
    "$$\n",
    "\n",
    "Note that $D_x = R_y$, $D_y = -R_x$, $L = R_z$\n",
    "\n",
    "$$\n",
    "[D_x, D_y] = L \\\\\n",
    "[L, D_x] = D_y\\\\\n",
    "[L, D_y] = -D_x\n",
    "$$\n",
    "\n",
    "#### The Killing form\n",
    "\n",
    "The killing form in (Dx, Dy, L) is given by:\n",
    "\n",
    "$$\\begin{pmatrix} -2 & 0 & 0 \\\\ 0 & -2 & 0 \\\\ 0 & 0 & -2 \\end{pmatrix}$$\n",
    "\n",
    "As we can verify on a basis, the killing form can be expressed as:\n",
    "\n",
    "$$k(X, Y) = -\\mathrm{tr}(XY^T)$$\n",
    "\n",
    "#### Conjugacy classes\n",
    "\n",
    "The Killing form splits $\\mathfrak{so}_3$ into spheres, each of which is a conjugacy class.\n",
    "\n",
    "* The __null matrix__\n",
    "* A sphere of radius $\\omega \\ge 0$, which corresponds to rotation around an axis of angular speed $\\omega$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "from scipy.linalg import expm\n",
    "\n",
    "plt.style.use(\"dark_background\")\n",
    "\n",
    "Dx = np.matrix([[0, 0, 1], [0, 0, 0], [-1, 0, 0]])\n",
    "Dy = np.matrix([[0, 0, 0], [0, 0, 1], [0, -1, 0]])\n",
    "L = np.matrix([[0, -1, 0], [1, 0, 0], [0, 0, 0]])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "(0, 0, 0)"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "def killing_so3(X, T):\n",
    "    return -np.trace(X*np.transpose(T))\n",
    "    \n",
    "killing_so3(Dx, Dx), killing_so3(Dy, Dy), killing_so3(L, L)\n",
    "killing_so3(Dx, Dy), killing_so3(L, Dx), killing_so3(L, Dy)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Check that the lie algebra basis is \"well balanced\"\n",
    "\n",
    "- the operation represented by $\\exp({\\pi \\over 2}L)$ is a quarter turn.\n",
    "- this operation maps Dx to Dy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "# test that 2piL is identity\n",
    "np.testing.assert_almost_equal(expm(2*np.pi*L), np.identity(3))\n",
    "# test that piL is not identity\n",
    "np.testing.assert_almost_equal(\n",
    "    expm(np.pi*L), \n",
    "    np.matrix([[-1, 0, 0], [0, -1, 0], [0, 0, 1]]))\n",
    "# now we know that pi/2L has order 4\n",
    "# check that r_90 . Dx = Dy (where the action is given by the conjugation)\n",
    "r_90 = np.matrix(expm(np.pi/2*L))\n",
    "np.testing.assert_allclose(\n",
    "    r_90*Dx*np.linalg.inv(r_90),\n",
    "    Dy)\n",
    "# this in itself proves that r_90 has order 4\n",
    "np.testing.assert_allclose(\n",
    "    (r_90**2)*Dx*np.linalg.inv(r_90**2),\n",
    "    -Dx)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Computations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Computation of the killing form\n",
    "\n",
    "$k(X, Y) = \\mathrm{tr}(\\mathrm{ad}_X \\circ \\mathrm{ad}_Y)$\n",
    "\n",
    "$$\n",
    "[D_x, D_y] = L \\\\\n",
    "[L, D_x] = D_y\\\\\n",
    "[L, D_y] = -D_x\n",
    "$$\n",
    "\n",
    "- **k(L, L) = -2**\n",
    "\n",
    "    $\\mathrm{ad}_L \\circ \\mathrm{ad}_L(X) = [L, [L, X]]$\n",
    "    \n",
    "    Which gives:\n",
    "    $$\\mathrm{ad}_L \\circ \\mathrm{ad}_L(Dx) = -D_x\\\\\n",
    "    \\mathrm{ad}_L \\circ \\mathrm{ad}_L(Dy) = -D_y \\\\\n",
    "    \\mathrm{ad}_L \\circ \\mathrm{ad}_L(L) = 0 \n",
    "    $$\n",
    "\n",
    "- **k(Dx, Dx) = -2**\n",
    "\n",
    "    $\\mathrm{ad}_Dx \\circ \\mathrm{ad}_Dx(X) = [Dx, [Dx, X]]$\n",
    "    \n",
    "    Which gives:\n",
    "    $$\\mathrm{ad}_Dx \\circ \\mathrm{ad}_Dx(Dx) = 0\\\\\n",
    "    \\mathrm{ad}_Dx \\circ \\mathrm{ad}_Dx(Dy) = -D_y\\\\\n",
    "    \\mathrm{ad}_Dx \\circ \\mathrm{ad}_Dx(L) =  -L\n",
    "    $$\n",
    "\n",
    "- **k(Dx, Dy) = 0**\n",
    "\n",
    "    $\\mathrm{ad}_Dx \\circ \\mathrm{ad}_Dy(X) = [Dx, [Dy, X]]$\n",
    "    \n",
    "    Which gives:\n",
    "    $$\\mathrm{ad}_Dx \\circ \\mathrm{ad}_Dy(Dx) = D_y\\\\\n",
    "    \\mathrm{ad}_Dx \\circ \\mathrm{ad}_Dy(Dy) = 0\\\\\n",
    "    \\mathrm{ad}_Dx \\circ \\mathrm{ad}_Dy(L) =  0\n",
    "    $$\n",
    "    \n",
    "- **k(L, Dx) = 0**\n",
    "\n",
    "    $\\mathrm{ad}_L \\circ \\mathrm{ad}_Dx(X) = [L, [Dx, X]]$\n",
    "    \n",
    "    Which gives:\n",
    "    $$\\mathrm{ad}_L \\circ \\mathrm{ad}_Dx(Dx) = 0 \\\\\n",
    "    \\mathrm{ad}_L \\circ \\mathrm{ad}_Dx(Dy) = 0\\\\\n",
    "    \\mathrm{ad}_L \\circ \\mathrm{ad}_Dx(L) = Dx  \n",
    "    $$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
