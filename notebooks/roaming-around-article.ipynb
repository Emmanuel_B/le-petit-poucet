{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Roaming Around Euclidean Space\n",
    "\n",
    "For this article series you will need a basic understanding of Lie groups and Lie algebras, at least a small intuition of Kleinian geometry and familiarity with programming.\n",
    "Kleinian geometry is a way to unify different types of geometries and space into the same framework. Using the language of group theory we can uniformly talk about geometry in our 2D Euclidean space as well as  in a 16 dimensional hyperbolic spacetime.\n",
    "\n",
    "The idea is to use this language to make a very simple software where the user can move around different kind of spaces. The spaces I'm targeting will be 2D for ease of representation:\n",
    "*   2D euclidean space (this is just the space of usual geometry)\n",
    "*   2D Galilean spacetime (this is naive 2D spacetime with 1 dimension of space and one of time, so just the space time if our world had a single dimension)\n",
    "*   2D Minkowski space (the spacetime of special relativity)\n",
    "*   Spherical geometry (what geometry would be like if we were on a sphere)\n",
    "*   Hyperbolic plane (another non-euclidean geometry)\n",
    "*   Projective plane\n",
    "\n",
    "The good thing is that we'll be able to use the same code for all of these spaces, using the expressiveness of Kleinian geometry."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## General formulation\n",
    "\n",
    "__Tools__:\n",
    "\n",
    "*   a 2 dimensional surface (manifold) S embedded in $\\mathbb{R}^m$.\n",
    "\n",
    "    For example, it could be a sphere embedded in $\\mathbb{R}^3$.\n",
    "    \n",
    "*   a group G together with a transitive action on S. We have its Lie algebra $\\mathfrak g$.\n",
    "\n",
    "    Concretely this is a function $G \\times \\mathbb{R}^m \\rightarrow \\mathbb{R}^m$\n",
    "\n",
    "*   a base point $p_0 \\in S$\n",
    "\n",
    "*   a bijective coordinate function $\\phi: U \\rightarrow \\mathbb{R}^m$ where U is a neighborhood of $p_0$ in the ambiant space. This function is such that $\\phi(S \\cap U) \\subset [-1,1]^2\\times\\{0\\}$\n",
    "\n",
    "    Concretely this is given by a function view and a function view_inverse.\n",
    "    \n",
    "\n",
    "__Ingredients__:\n",
    "\n",
    "*   A list of points of $\\mathbb{R}^m$ that we want to show\n",
    "\n",
    "*   A current transformation $T \\in G$\n",
    "\n",
    "*   A Lie algebra basis $(X_1, X_2, ..., X_q)$\n",
    "\n",
    "__Recipe__:\n",
    "\n",
    "*   We display the points using $\\phi (T^{-1} \\cdot c)$\n",
    "\n",
    "    Where $c \\in S \\subset \\mathbb{R}^m$, is a point on the manifold, a \"bread crumb\".\n",
    "\n",
    "*   User chooses a lie Algebra basis vector to move about: X_i\n",
    "\n",
    "*   We move the current transformation by $X_i$: $T = T+TX_i$ where X_i is assumed to be very small.\n",
    "\n",
    "*   We move the current lie algebra basis by $X_i$: $X_j = X_j + [X_i, X_j]$\n",
    "\n",
    "*   We add new points using: $T \\cdot \\phi^{-1} (p)$\n",
    "\n",
    "    *   Where $p \\in \\mathbb{R}^2$ is a point given in the local chart\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "plt.style.use(\"dark_background\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Structure of Euclidean Group\n",
    "\n",
    "\n",
    "For today I'll start with euclidean space. Note that moving around euclidean space is a very simple task and we do not need any Lie group machinery. But because my goal is to introduce those methods, I'll do the moving around in Euclidean space using only the properties of the corresponding Lie group, Lie algebra and the action.\n",
    "The group of Euclidean geometry is none else that the Euclidean group. Since we are in 2 dimension this is just a semi-direct product of SO(2) with the group of 2D translations. This group has dimension 3. We can choose the following representation as a subgroup of GL(3):\n",
    "\n",
    "$$\\left \\{ \\begin{pmatrix} \\cos \\theta && -\\sin \\theta && u \\\\\n",
    "                           \\sin \\theta && \\cos \\theta && v \\\\\n",
    "                           0           && 0           && 1 \\end{pmatrix}, \\forall (\\theta, u, v) \\in \\mathbb{R}^3 \\right\\}$$\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "def make_euclid_isometry(theta, origin_image):\n",
    "    \"\"\" Creates a 3D matrix representing a 2D isometry.\n",
    "    \n",
    "    Args:\n",
    "        theta (float): the rotation angle in radians.\n",
    "        origin_image (1D): A point in 2 dimensions indicating where this transformation\n",
    "            maps the origin.\n",
    "        \n",
    "    Returns:\n",
    "        Numpy 3-by-3 matrix.\n",
    "    \"\"\"\n",
    "    a, b = np.cos(theta), np.sin(theta)\n",
    "    u, v = origin_image\n",
    "    return np.matrix([[a, -b, u], [b, a, v], [0, 0, 1]])\n",
    "\n",
    "RO90 = make_euclid_isometry(np.math.pi/2, [0, 0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And we get a corresponding Lie algebra, spanned by the following basis:\n",
    "\n",
    "$$L = \\begin{pmatrix} 0 && -1 && 0 \\\\\n",
    "                  1 && 0 && 0 \\\\\n",
    "                  0 && 0 && 0 \\end{pmatrix}, D_x = \\begin{pmatrix} 0 && 0 && 1 \\\\\n",
    "                  0 && 0 && 0 \\\\\n",
    "                  0 && 0 && 0 \\end{pmatrix},\n",
    "                  D_y = \\begin{pmatrix} 0 && 0 && 0 \\\\\n",
    "                  0 && 0 && 1 \\\\\n",
    "                  0 && 0 && 0 \\end{pmatrix}$$\n",
    "                  \n",
    "Which obey the following relations:\n",
    "\n",
    "$$[D_x, D_y] = 0 \\\\ [L, D_x] = D_y \\\\ [L, D_y] = -D_x$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "# lie algebra\n",
    "L = np.matrix([[0, -1, 0], [1, 0, 0], [0, 0, 0]])\n",
    "Dx = np.matrix([[0, 0, 1], [0, 0, 0], [0, 0, 0]])\n",
    "Dy = np.matrix([[0, 0, 0], [0, 0, 1], [0, 0, 0]])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally we have an action of this group on $\\mathbb{R}^2$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 38,
   "metadata": {},
   "outputs": [],
   "source": [
    "def act_euclid_isometry(T, point):\n",
    "    \"\"\" Action of the Euclidean isometry group on the plane.\n",
    "    \n",
    "    Args:\n",
    "        T (3-by-3 matrix): Euclidean isometry matrix.\n",
    "        point (np 1D): A point in 2D space.\n",
    "    \n",
    "    Returns:\n",
    "        A point in 2D space.\n",
    "    \"\"\"\n",
    "    # we upgrade the point to 3D:\n",
    "    point_3D = np.array([point[0], point[1], 1]).reshape(3, 1)\n",
    "    point_trans = T*point_3D\n",
    "    return np.array([point_trans[0,0], point_trans[1,0]])\n",
    "    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 40,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([6.123234e-17, 1.000000e+00])"
      ]
     },
     "execution_count": 40,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "act_euclid_isometry(RO90, [1, 0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The problem of the lie group action\n",
    "\n",
    "The lie group action tells us how to move on the group up to first order approximation.\n",
    "\n",
    "If T is a member of the group, and X is a lie algebra element, then $T+X$ is very close to the group.\n",
    "\n",
    "But there are a few issues:\n",
    "\n",
    "*   The lie group action tends to \"leave the group\". That is the points are close to the original group but not exactly on it.\n",
    "*   First order sometimes is not enough: for example when adding the rotation \"infinitesimal transformation\" L, no matter how many time we add it, it will never change the diagonal of the transformation. Meaning as we rotate we go further and further from the original group -> we are no longer rotating.\n",
    "\n",
    "Solutions:\n",
    "\n",
    "*   After each update reproject the transformation back to the group.\n",
    "*   Use a second order transformation.\n",
    "*   Instead of adding, use the matrix exponential: $T \\cdot \\exp(X)$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What would the second order transformation look like?\n",
    "\n",
    "This is the second derivative of $t \\mapsto \\exp(tX)$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$(g\\exp(tX))' = gX$\n",
    "\n",
    "So g gets an update in the $gX$ direction.\n",
    "$g_1 = g+gX$\n",
    "\n",
    "Then g_1 is updated by:\n",
    "$g_1X = (g+gX)X = gX + gX^2$\n",
    "\n",
    "Writing $gX = a_X$, we have:\n",
    "$a_X(n+1) = a_X(n)+a_X(n)X$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Can we move around the space without storing any group element?\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Development\n",
    "\n",
    "1.  Add test for all action functions (action on points, Lie bracket)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "py36_datascience",
   "language": "python",
   "name": "py36_datascience"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
