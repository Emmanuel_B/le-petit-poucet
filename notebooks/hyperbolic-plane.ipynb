{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Roaming around hyperbolic space\n",
    "\n",
    "The group of direct isometries of hyperbolic space is __PSL(2)__. Since this group is just SL(2) quotiented by $\\pm I$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from scipy import linalg\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "plt.style.use(\"dark_background\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Adjoint action\n",
    "\n",
    "The lie algebra $\\mathfrak{sl}_2$ has dimension 3 and is the set of all traceless matrices.\n",
    "\n",
    "The Killing form is a bilinear form on the Lie algebra."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "def killing(X, Y):\n",
    "    \"\"\" Killing form.\n",
    "    \n",
    "    Args:\n",
    "        X (np.matrix): Lie algebra vector.\n",
    "        Y (np.matrix): Lie algebra vector.\n",
    "        \n",
    "    Returns:\n",
    "        Scalar.\n",
    "    \"\"\"\n",
    "    return 4*np.trace(X*Y)\n",
    "\n",
    "def rot_2D(alpha):\n",
    "    \"\"\" Creates a 2D rotation matrix.\n",
    "    \n",
    "    Args:\n",
    "        alpha (float): Angle in radians.\n",
    "        \n",
    "    Returns:\n",
    "        np.matrix 2x2\n",
    "    \"\"\"\n",
    "    c, s = np.cos(alpha), np.sin(alpha)\n",
    "    return np.matrix([[c, -s], [s, c]])\n",
    "\n",
    "P = np.matrix([[0, 1], [1, 0]])\n",
    "Q = np.matrix([[1, 0], [0, -1]])\n",
    "L = np.matrix([[0, -1], [1, 0]])\n",
    "R_90 = rot_2D(np.pi/2)\n",
    "R_45 = rot_2D(np.pi/4)\n",
    "shear1 = np.matrix([[1, 1], [0, 1]])\n",
    "h_acc = np.matrix([[2, 0], [0, 1/2]])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Structure coefficient of $\\mathfrak{sl}_2$\n",
    "[P, Q] = 2L\n",
    "\n",
    "[L, P] = -2Q\n",
    "\n",
    "[L, Q] = 2P\n",
    "\n",
    "### Manual computation of a killing form value\n",
    "\n",
    "$k(L, L) = \\mathrm{tr}(ad_L\\circ ad_L)$\n",
    "\n",
    "$$\n",
    "ad_L \\circ ad_L (L) = [L, [L, L]] = 0 \\\\\n",
    "ad_L \\circ ad_L (P) = [L, [L, P]] = [L, -2Q] = -4P \\\\\n",
    "ad_L \\circ ad_L (Q) = [L, [L, Q]] = [L, 2P] = -4Q\n",
    "$$\n",
    "\n",
    "Finally, $k(L, L) = -8$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Matrix of Killing form in (P, Q, L) basis:\n",
      " [[ 8.  0.  0.]\n",
      " [ 0.  8.  0.]\n",
      " [ 0.  0. -8.]]\n"
     ]
    }
   ],
   "source": [
    "B = np.identity(3)\n",
    "for i, Mi in enumerate([P, Q, L]):\n",
    "    for j, Mj in enumerate([P, Q, L]):\n",
    "        B[i, j] = killing(Mi, Mj)\n",
    "print(\"Matrix of Killing form in (P, Q, L) basis:\\r\\n\", B)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we want to express the adjoint action in this basis.\n",
    "\n",
    "For a matrix $g = \\begin{pmatrix} a & b \\\\ c & d \\end{pmatrix} \\in SL(2)$, we can compute the action on the Lie algebra.\n",
    "\n",
    "For $X \\in \\mathfrak{sl_2}$, the action is given by $\\mathrm{Ad}(g)(X) = gXg^{-1}$ where $\\mathrm{Ad}: \\mathrm{SL}_2 \\rightarrow \\mathrm{GL}(\\mathfrak{sl}_2)$\n",
    "\n",
    "Here is the matrix of this action in the (P, Q, L) basis:\n",
    "\n",
    "$$\\begin{pmatrix} \n",
    "{a^2 - b^2 - c^2 + d^2 \\over 2} & cd - ab & {-a^2 -b^2 + c^2 + d^2 \\over 2} \\\\\n",
    "bd - ac                         & ad + bc & ac + bd \\\\\n",
    "{-a^2 + b^2 -c^2 + d^2 \\over 2} & ab + cd & {a^2 + b^2 + c^2 + d^2 \\over 2}\n",
    "\\end{pmatrix}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We have $d\\mathrm{Ad}_I: \\mathfrak{sl}_2 \\rightarrow \\mathfrak{gl}(\\mathfrak{sl}_2)$\n",
    "it is given by: $d\\mathrm{Ad}_I(X)(Y) = [X, Y]$.\n",
    "\n",
    "Computing the derivative is useful for:\n",
    "\n",
    "- $dAd_I(X)(p)$ gives us the tangent at p\n",
    "- Solving for p in $dAd_I(L)(p) = 0$ gives us the center of the rotation given by L (assuming L is a rotation).\n",
    "\n",
    "We can also compute the derivative in coordinates:\n",
    "\n",
    "${\\delta\\mathrm{Ad} \\over \\delta a} = \\begin{pmatrix} \n",
    "a & -b & -a \\\\\n",
    "- c & d & c \\\\\n",
    "-a & b & a\n",
    "\\end{pmatrix} \\implies {\\delta\\mathrm{Ad} \\over \\delta a}(I) = \\begin{pmatrix} \n",
    "1 & 0 & -1 \\\\\n",
    "0 & 1 & 0 \\\\\n",
    "-1 & 0 & 1\n",
    "\\end{pmatrix}$\n",
    "\n",
    "${\\delta\\mathrm{Ad} \\over \\delta b} = \\begin{pmatrix} \n",
    "- b& -a & -b \\\\\n",
    "d & c & d \\\\\n",
    "b & a & b\n",
    "\\end{pmatrix} \\implies {\\delta\\mathrm{Ad} \\over \\delta b}(I) = \\begin{pmatrix} \n",
    "0 & -1 & 0 \\\\\n",
    "1 & 0 & 1 \\\\\n",
    "0 & 1 & 0\n",
    "\\end{pmatrix}$\n",
    "\n",
    "${\\delta\\mathrm{Ad} \\over \\delta c} = \\begin{pmatrix} \n",
    "- c & d & c \\\\\n",
    "- a & b & a \\\\\n",
    "-c & d & c\n",
    "\\end{pmatrix} \\implies {\\delta\\mathrm{Ad} \\over \\delta c}(I) = \\begin{pmatrix} \n",
    "0 & 1 & 0 \\\\\n",
    "-1 & 0 & 1 \\\\\n",
    "0 & 1 & 0\n",
    "\\end{pmatrix}\n",
    "$\n",
    "\n",
    "${\\delta\\mathrm{Ad} \\over \\delta d} = \\begin{pmatrix} \n",
    "d & c & d \\\\\n",
    "b & a & b \\\\\n",
    "d & c & d\n",
    "\\end{pmatrix}\n",
    "\\implies {\\delta\\mathrm{Ad} \\over \\delta d}(I) = \\begin{pmatrix} \n",
    "1 & 0 & 1 \\\\\n",
    "0 & 1 & 0 \\\\\n",
    "1 & 0 & 1\n",
    "\\end{pmatrix}$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$d\\mathrm{Ad}_I(P) = \\begin{pmatrix} \n",
    "0 & -1 & 0 \\\\\n",
    "1 & 0 & 1 \\\\\n",
    "0 & 1 & 0\n",
    "\\end{pmatrix} + \\begin{pmatrix} \n",
    "0 & 1 & 0 \\\\\n",
    "-1 & 0 & 1 \\\\\n",
    "0 & 1 & 0\n",
    "\\end{pmatrix} = \\begin{pmatrix} \n",
    "0 & 0 & 0 \\\\\n",
    "0 & 0 & 2 \\\\\n",
    "0 & 2 & 0\n",
    "\\end{pmatrix}$\n",
    "\n",
    "$d\\mathrm{Ad}_I(Q) = \\begin{pmatrix} \n",
    "1 & 0 & -1 \\\\\n",
    "0 & 1 & 0 \\\\\n",
    "-1 & 0 & 1\n",
    "\\end{pmatrix} - \\begin{pmatrix} \n",
    "1 & 0 & 1 \\\\\n",
    "0 & 1 & 0 \\\\\n",
    "1 & 0 & 1\n",
    "\\end{pmatrix} = \n",
    "\\begin{pmatrix} \n",
    "0 & 0 & -2 \\\\\n",
    "0 & 0 & 0 \\\\\n",
    "-2 & 0 & 0\n",
    "\\end{pmatrix}$\n",
    "\n",
    "$d\\mathrm{Ad}_I(L) = - \\begin{pmatrix} \n",
    "0 & -1 & 0 \\\\\n",
    "1 & 0 & 1 \\\\\n",
    "0 & 1 & 0\n",
    "\\end{pmatrix} + \n",
    "\\begin{pmatrix} \n",
    "0 & 1 & 0 \\\\\n",
    "-1 & 0 & 1 \\\\\n",
    "0 & 1 & 0\n",
    "\\end{pmatrix} = \n",
    "\\begin{pmatrix} \n",
    "0 & 2 & 0 \\\\\n",
    "-2 & 0 & 0 \\\\\n",
    "0 & 0 & 0\n",
    "\\end{pmatrix}\n",
    "$\n",
    "\n",
    "Finally for a matrix $\\begin{pmatrix} a & b \\\\ c & -a \\end{pmatrix} \\in \\mathfrak{sl}_2$\n",
    "\n",
    "We have:\n",
    "\n",
    "$$\n",
    "dAd_I \\left ( \\begin{pmatrix} a & b \\\\ c & -a \\end{pmatrix} \\right ) = \n",
    "\\begin{pmatrix} 0 & c-b & -2a \\\\ b-c & 0 & b+c \\\\ -2a & b+c & 0 \\end{pmatrix} $$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [],
   "source": [
    "def d3_rep(g):\n",
    "    \"\"\" A three dimensional representation of SL(2).\n",
    "    \n",
    "    Args:\n",
    "        g (np.matrix): Element of SL(2).\n",
    "        \n",
    "    Returns:\n",
    "        3x3 matrix.\n",
    "    \"\"\"\n",
    "    a, b, c, d = g[0,0], g[0, 1], g[1, 0], g[1, 1]\n",
    "    return np.matrix([\n",
    "        [(a**2-b**2-c**2+d**2)/2, c*d-a*b, (-a**2-b**2+c**2+d**2)/2],\n",
    "        [b*d-a*c, a*d+b*c, a*c+b*d],\n",
    "        [(-a**2+b**2-c**2+d**2)/2, a*b+c*d, (a**2+b**2+c**2+d**2)/2]\n",
    "    ])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "# We do a bit of testing of this representation\n",
    "np.testing.assert_allclose(\n",
    "    d3_rep(np.identity(2)), \n",
    "    np.identity(3), \n",
    "    err_msg=\"The image of the identity of SL2 should be the identity of GL3\")\n",
    "\n",
    "np.testing.assert_allclose(\n",
    "    d3_rep(R_45)*d3_rep(R_45),\n",
    "    d3_rep(R_90),\n",
    "    atol=1e-6,\n",
    "    err_msg=\"Representation should respect the multiplication law\")\n",
    "\n",
    "np.testing.assert_allclose(\n",
    "    d3_rep(R_45)*d3_rep(shear1),\n",
    "    d3_rep(R_45*shear1),\n",
    "    atol=1e-6,\n",
    "    err_msg=\"Representation should respect the multiplication law\")\n",
    "\n",
    "np.testing.assert_allclose(\n",
    "    d3_rep(h_acc)*d3_rep(shear1),\n",
    "    d3_rep(h_acc*shear1),\n",
    "    atol=1e-6,\n",
    "    err_msg=\"Representation should respect the multiplication law\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Hyperboloid model of hyperbolic space\n",
    "\n",
    "The hyperbolic plane naturally embbeds into $\\mathbb{R}^3$ as one sheet of a hyperboloid.\n",
    "\n",
    "There is an action of SL(2) on this space which is given by a representation of SL(2) on $\\mathbb{R}^3$ that preserves an indefinite bilinear form."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we check that the action preserves the bilinear form B."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.testing.assert_allclose(\n",
    "    d3_rep(shear1)*B*np.transpose(d3_rep(shear1)),\n",
    "    B,\n",
    "    err_msg=\"Representation should leave B invariant.\")\n",
    "\n",
    "np.testing.assert_allclose(\n",
    "    d3_rep(R_45)*B*np.transpose(d3_rep(R_45)),\n",
    "    B,\n",
    "    err_msg=\"Representation should leave B invariant.\")\n",
    "\n",
    "np.testing.assert_allclose(\n",
    "    d3_rep(h_acc)*B*np.transpose(d3_rep(h_acc)),\n",
    "    B,\n",
    "    err_msg=\"Representation should leave B invariant.\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [],
   "source": [
    "def biliapp(bilinear, X, Y=None):\n",
    "    \"\"\" Applies a bilinear map given by a matrix.\n",
    "    \n",
    "    Args:\n",
    "        bilinear (np.matrix): Matrix that represents the bilinear app.\n",
    "        X, Y (np.matrix): Column vectors. \n",
    "            If Y is None (default) we return the norm of X: B(X, X)\n",
    "            \n",
    "    Returns:\n",
    "        Scalar\n",
    "    \"\"\"\n",
    "    if not Y:\n",
    "        Y = X\n",
    "    return np.float(np.transpose(X)*bilinear*Y)\n",
    "\n",
    "HH = 1/8*B"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Point\n",
      " [[0]\n",
      " [0]\n",
      " [1]] \n",
      "has distance -1.0\n",
      "Point\n",
      " [[ 0]\n",
      " [ 0]\n",
      " [-1]] \n",
      "has distance -1.0\n",
      "Point\n",
      " [[1]\n",
      " [0]\n",
      " [0]] \n",
      "has distance 1.0\n",
      "Point\n",
      " [[-1]\n",
      " [ 0]\n",
      " [ 0]] \n",
      "has distance 1.0\n",
      "Point\n",
      " [[0]\n",
      " [1]\n",
      " [0]] \n",
      "has distance 1.0\n"
     ]
    }
   ],
   "source": [
    "z_n1 = np.matrix([[0], [0], [-1]])\n",
    "z_p1 = np.matrix([[0], [0], [1]])\n",
    "\n",
    "print(\"Point\\r\\n\", z_p1, \"\\r\\nhas distance\", biliapp(HH, z_p1))\n",
    "print(\"Point\\r\\n\", z_n1, \"\\r\\nhas distance\", biliapp(HH, z_n1))\n",
    "print(\"Point\\r\\n\", np.matrix([[1], [0], [0]]), \"\\r\\nhas distance\", biliapp(HH, np.matrix([[1], [0], [0]])))\n",
    "print(\"Point\\r\\n\", np.matrix([[-1], [0], [0]]), \"\\r\\nhas distance\", biliapp(HH, np.matrix([[-1], [0], [0]])))\n",
    "print(\"Point\\r\\n\", np.matrix([[0], [1], [0]]), \"\\r\\nhas distance\", biliapp(HH, np.matrix([[0], [1], [0]])))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can work on the hyperboloid sheet given by $p^THp = -1$, which is equivalent to: $x^2+y^2-z^2=-1$.\n",
    "\n",
    "1.  We notice that this surface is symmetric by reflections on the x=0, y=0 and z=0 planes.\n",
    "\n",
    "2.  We also notice that we have $0 \\leq x^2+y^2 =z^2-1$ so $z^2 \\geq 1$, so the surface is separated in two sheets with a forbidden band for $|z| \\lt 1$.\n",
    "\n",
    "3.  We also notice that this surface is invariant by rotations around the z axis.\n",
    "    So we only need to understand the cut of the surface on the plane x = constant."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Hyperboloid cuts\n",
    "\n",
    "Cutting along the z axis. \n",
    "\n",
    "*   If z = 1, the we have x=y=0, and the cut is just a __point__.\n",
    "*   If z > 1, then we have x^2+y^2=z^2-1 which is the equation for a __circle__ of center (0, 0, z) and radius $\\sqrt{z^2-1}$\n",
    "\n",
    "Cutting along the x axis.\n",
    "\n",
    "We have $(y-z)(y+z)=-1-x^2$, which is the equation of a hyperbole with axes (1, -1) and (1, 1).\n",
    "\n",
    "In particular, if x=0, we have $(y-z)(y+z)=-1$.\n",
    "\n",
    "Writing $d = y-z$ and $a=y+z$, we have $ad=-1$, so $d={-1\\over a}$\n",
    "Finally we can recover the values of y and z, parametrized by $a \\in \\mathbb{R}^*$:\n",
    "\n",
    "$$\n",
    "y={a+d \\over 2} \\\\\n",
    "z={a-d \\over 2} \\\\\n",
    "d={-1 \\over a}\n",
    "$$\n",
    "\n",
    "Which gives finally\n",
    "\n",
    "$$\n",
    "y={a^2 - 1\\over 2a} \\\\\n",
    "z={a^2 + 1\\over 2a}\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from matplotlib import pyplot as plt\n",
    "\n",
    "plt.style.use(\"dark_background\")\n",
    "a = np.linspace(0.2,5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAYAAAAEWCAYAAABv+EDhAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADh0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uMy4xLjMsIGh0dHA6Ly9tYXRwbG90bGliLm9yZy+AADFEAAAgAElEQVR4nO3de1zPh/4H8Nf3UumiRDcVuSUiFLlmuSXCNLe5bDNMm9mM7Ww7h52dbWcXZzs2Y5sZYZjbiGkuE3IPoVIpiiRRoaJCt+/794edfscZFl0+1ef1fDxeD6rv9/t5f6LPq8/n+/l+vhoAAiIiUh2t0gMQEZEyWABERCrFAiAiUikWABGRSrEAiIhUigVARKRSLACq83x8fJCYmFgljx0eHo4pU6ZUyWMTVTUWANUI48aNQ2RkJPLy8nDlyhVs374dvXr1Ktd9U1JS0L9//4d+/dChQ2jTpk1ljVpnWFtbIyQkBPn5+bh48SLGjRun9EhUzVgApLhZs2Zh/vz5+PTTT2Fvb4+mTZviu+++w/Dhw5UerU779ttvUVRUBHt7e0yYMAGLFi2Cu7u70mNRNROGUSqWlpaSl5cno0aNeuhtli9fLv/85z/LPvb19ZW0tDQBICtXrpTS0lK5ffu25OXlydtvv/2H+//37QFISkqKvPXWWxITEyO5ubmybt06MTExeeCyJ06cKIcOHZKFCxdKbm6uJCQkSL9+/cq+Hh4eLlOmTBEA0qJFC9mzZ49cv35drl27JqtXrxYrK6tyL3fIkCESFRUlOTk5cvjwYfHw8HjgTD169JBr166Js7OzAJAOHTpIdna2uLm5lfv7bmZmJoWFheLq6lr2uZUrV8pnn32m+P8Jplqj+ACMiuPv7y/FxcWi0+keeptHFQBwb8Pav3//h97/Qbc/duyYNG7cWKytreXMmTPy8ssvP/C+EydOlOLiYpk5c6bo9XoZM2aM5ObmirW1tQD3F0DLli1lwIABYmxsLDY2NrJ//3756quvyrXcTp06SWZmpnTt2lW0Wq288MILkpKSIsbGxg+c6+OPP5Y9e/ZIvXr15PTp0zJ9+vSyr4WGhkpOTs4DExoaWra8goKC+x7zrbfekq1btyr+f4KpvvAQECmqUaNGuH79OkpLS6t1uQsWLMDVq1eRk5OD0NBQdOrU6aG3zcrKwvz581FSUoINGzbg7NmzGDJkyB9ud/78eezevRtFRUW4fv06vvzyS/j6+pZruUFBQVi8eDGOHz8Og8GAlStXorCwEN27d3/gTB988AGsrKxw/PhxpKen49tvvy372rBhw2Btbf3ADBs2DABgYWGBW7du3feYN2/eRP369cv3DaQ6gQVAirpx4wZsbGyg0+mqdbkZGRllf799+zYsLCweetv09PT7Pk5NTYWjo+MfbmdnZ4e1a9fi8uXLuHnzJlavXg0bG5tyLdfFxQVvvfUWcnJyytKkSZMHLgcASkpKsGLFCnh4eGDevHl/vsL/Iz8/H5aWlvd9ztLSEnl5eY/9WFR7sQBIURERESgsLERgYOBDb1NQUAAzM7Oyjx0cHO77uohU2XwA4OTkdN/HTZs2xZUrV/5wu08//RQiAg8PD1hZWeG5556DRqMp1zLS0tLwySef3Pfburm5OdatW/fA2zs6OuIf//gHli1bhnnz5sHY2Ljsa9u3b0deXt4Ds337dgDAuXPnoNfr0apVq7L7dezYEfHx8eWal+oGFgAp6tatW3j//ffx7bffYvjw4TA1NYVer8egQYPwr3/9CwAQHR2NgIAAWFtbw97eHjNnzrzvMTIzM9GiRYsqm9HOzg4zZsyAXq/HqFGj0LZt27IN6X+rX78+8vPzcfPmTTg6OuLtt98u9zKWLFmCV155BV27dgUAmJmZISAg4KF7JitWrEBwcDCmTJmCq1ev4p///GfZ1wICAlC/fv0HJiAgAMC9vY+QkBB89NFHMDMzQ8+ePTF8+HCsWrXqcb41VMuxAEhxX375Jd5880289957uHbtGtLS0vDaa69hy5YtAIBVq1YhJiYGFy9exK5du7B+/fr77v/ZZ5/hvffeQ05ODt56661Kn+/YsWNwdXXF9evX8cknn2DUqFHIzs7+w+0+/PBDeHl54ebNm9i2bRtCQkLKvYyTJ09i6tSp+Oabb5CTk4Pk5GS8+OKLD7ztjBkzYGdnh7///e8AgEmTJmHSpEnw8fF5rPV69dVXYWpqiqysLKxduxbTpk3DmTNnHusxqHbT4N6zwUT0ABMnTsRLL72E3r17Kz0KUaXjHgARkUqxAIiIVIqHgIiIVIp7AEREKqVXeoDHkZWVhdTUVKXHICKqVVxcXGBnZ/eHz9eqAkhNTYW3t7fSYxAR1SqRkZEP/DwPARERqRQLgIhIpVgAREQqxQIgIlIpFgARkUqxAIiIVIoFQESkUqooANfu3vAZPxo6IyOlRyEieiz1bRph+DszUc/CvNIfWxUF4NHfF8/87U38NXQ9Og8bDI1WFatNRLWYsakpBr4yGX/btgG9xo5Ei86elb6MWnUxuMjIyCd+JXDrHt4ImPkqmri3wdWk89j+9fc4s/9QJU9IRFQxWp0O3sMD4D99KqzsbBGzay+2zV+EG2mXn/gxH7btrFWXgqiIcxGRSDo6GR0G9sPg14Iw5ZsvkBJ1Gtvmf4eUUzFKj0dEhDa9e2DorOlo7NoSF6NjsfLNObgYE1tly1PNHsB/0+p16Bo4FAOnTYGVnS3OHDiM7V8vwtVz5ythSiKix+Ps7oahs16Da/cuuJaahm3zv0Ps7n2V9vgP23aqsgD+w6ieCXzGj0a/Kc+jnoUForbvws5vlyD78pVKWwYR0cNYOzogYMYr8Brij/zsHIQtXoaIDVtQWlJSqcthATyCqWV99J30HHpPGAOtXoejG39B2OJlyL+RU+nLIiIytbTEgKkT4TN+FMQg2L9qHcKXrcLd/IIqWR4LoBwsbW3g9/IkdBv5NEqKinBg1XrsW/FTlf2jEJG66I2N0WvsSAwIehH16lvgxNbt+O2bJcjNzKrS5bIAHoNNU2cMei0InoP9UJB7E3uW/IjD6zahpKioypdNRHWPRqOB1xB/DHo9CA0dGyPh4BFsm/9dtT3vyAJ4Ak5tWyNgxjS08emO3IxM/PZdME5s3Q5DaWm1zUBEtVvrHl0xdNZ0OLVtjbQzidj25bdIOnaiWmdgAVRAS28vDJn5Klw6tEPmhYvYseB7xO7ZX+1zEFHt4ezuhiGzpqN1d2/cuHwFOxZ8j+iduyFS/ZtcFkAlaN/PFwFvvAL7Fs2Qejoe279ehOTjJxWbh4hqnobOjhj8+svwChiIgpxchC1ejiMbNqO0uFixmVT/QrDKELd3P87sP4TOwwbB/9WXMC34G5w9fBTbF3yPy2fOKj0eESnI3LoBBgS9iJ7PjoChpBS7l/xYpWf2VAbuATwhvbExeo4dgQEvTYS5dQNE79yNHd/8gOupaUqPRkTVyNjUFL4Tx6HPi+NhZGKC45t/xa5Fwbh17brSo5XhHkAlKykqwoGV63A8JBS+E8fD94Wx8BjQB8dCQhH2/bIa9Y9PRJVPp9ej++hA+L08CfUbNcTpsHDsWLgYWSmpSo9WbtwDqCQWjazhFzQJ3UcHwlBSikNrNmDvstW4cytP6dGIqBJpNBp0GuyHQa9NhU0TZyRHnsK2+d/h0ul4pUd7KD4JXE0aOjvC/9WX4DXEH3fz8xG+bDUO/rQBxXcLlR6NiCrIrVd3DHljGpzatkZ64jlsX/A9Eg9GKD3Wn2IBVLPGrVsiYMY0uPv2ws2sa9j1/TIc3xwKQwlfQ0BU27h0bI+AN6ahlbcXblxOx46FPyB6R5gip3Q+CRaAQpp7dcSQN6ahuVdHXEtNw86FixGza2+t+Y9DpGYOrVpg8OtBaN/PF7eu30DY98twbNPWSr9YW1VjASis7VO9MGTmNDR2bYm0M4nYPn8RzkUcV3osInqAhk6N4f/qVHgN9UdhwW2EL1+Ng6vXo+jOXaVHeyI8C0hhCQcOI/FQBDwD/DBoehBe/uFrJB07ge1fL8Kl2DNKj0dEuPf+uwOCXkT3UcMhpQbsX7EGe5etwu2bt5QerUooVgDOzs5YuXIl7O3tISL44YcfsGDBAqXGqRZiMODUr78h5re96DF6OAYETcIba4Jxevc+7Fjwfa06fYyoLjG1tES/yRPgM34MdHo9jm0ORdji5biVdU3p0aqcKBEHBwfx9PQUAGJhYSFnz56Vtm3bPvI+kZGRisxaVTExM5MBL0+STyJ2yxfRh2TMh7Olgb2d4nMxjFryn5/Bj4+EyRcxh2X8Z/+QRk2cFZ+rsvOwbadiewAZGRnIyMgAAOTn5yMhIQFOTk5ISEhQaqRqV3j7NnYvXo6I9SHoP3Uieo0dCa8hA3F43SbsWfJjnd3tJFKa3sQEPZ99Bv2nvACLhtaI27sfO75Zgowkdb0tbI14EtjFxQUHDhxA+/btkZd3/wunpk6diqCgIACAjY0NmjdvrsSI1aKBgz38X30JXZ4ejMLbd7BvxU84sGo9iu7cUXo0ojpBp9ej64hh8AuaBCt7W5yLOI7tCxYjLa5uPw/3qBNoFN01MTc3lxMnTsgzzzzzxLsxdS32LZrJxK8+k3mxEfLBvm3Sa9wo0en1is/FMLU1Wp1OugYOldk7Nsm82AiZvmKRtOjiqfhc1ZVHbDuVG0qv18vOnTtl1qxZFV2JOpmmHdrJtOBvZF5shMzesVG8hvqLRqNRfC6GqS3RaLXiNWSg/DV0vcyLjZA31gaLW89uis9V3XnYtlPRQ0A//vgjsrOzMWvWrHLdvja/DqAi3Hp2Q8Ab0+Ds7oYrZ5Ow/evvkXDwiNJjEdVYGo0GHQb2w8BpU+DQsjmunE3Czm9+QPy+Q0qPpogadwioV69eIiISExMjUVFREhUVJYMHD36iFlNDNBqNdPLvL3/9dUPZLmyzTh0Un4thalI0Go149PeVv4SslnmxEfL25p+kw8B+qt9zrpF7AI9LrXsA/02r16HbM09j4LTJsLS1QXz4QWxfuFh1Zy8Q/a/2/XwxcNpkOLVpjayUVOxaFIzo3/ZADAalR1McLwVRxxib1oPP+NHoN/l5mFiYI27vAexduhJp8eo5jZYIANr3ewoDX5kCp7atce3iJexavAzRO3bDUMoLL/4HC6COMrW0xFPPPwuf8aNgZmmJc0cjsXfpSiQdO6H0aERVRqPRoF3fpzDwlcn3NvypaQhbvBxR23dxw/8ALIA6zsTMDD1GB+KpF8bCys4WqafjsTd4FeLDD/DKo1RnaDQaeAzoA7+XJ8HRzRXXLl66t+HfEcYN/yOwAFRCb2yMLk8PRt9Jz8GmqTMyzqcgfNlqnNr+G9+LgGotjVaLjgP7we/lSXBo1QJZKanY/cMKbvjLiQWgMlqdDh38+qL/Sy/A0c0V2VeuYt+KNTi+OZTvTka1hlavQ+ch/ug35QXYNXdBRvIFhC1efu89NfjkbrmxAFSsbe+e6P/SC2ju1RF5N7JxcPUGHF6/CXfz8pUejeiB9MbG8B4+BH0nP4dGzo5ITziHsB+WI27Pfh7SfAIsAEJzr47o/9ILaNu7J+7mF+DI+k04sGo98m5kKz0aEQDA2NQUPUYHwnfiuHvPZcXEIeyHFUg4cFjp0Wo1FgCVcXRzRb8pz6PjwH4oLSnB8c2/Yt+Kn5CdflXp0UilzKws4TNuFHwmjIF5AyskHT2BPUt/5NlslYQFQH/QqIkz+k6eAO+nA6DRahG9czf2Bq9CRvIFpUcjlbCyt8VTz49Fj9GBMDEzQ1z4AewNXoXUmDilR6tTWAD0UJZ2tvB9fix6jLn3QxgffhB7glfyh5CqjH2LZugzaQK8hvhDo9EgansYwpev5i8fVYQFQH/qf3fDkyNPYe/SlTh75JjSo1Ed0dyrI/q+OAHt+vZG0Z27OBayFftXrkXOlQylR6vTWABUbsampug+ajh8J45DA3s7pJ1JxN7gVYjdvY+n3tFj0+p0aN/fF30mjodLh3bIz87B4XWbcHjtRhTk3lR6PFVgAdBj0xkZofPQQeg3+TnYNmuKrJRUhC9bjZO/7kRpSYnS41ENZ2Juhq7PDEPvCWPQyNkR11LTsP/HtTgRup2vRalmLAB6YhqtFh4D+qD/lBfg7O6G3IxM7PtxLY5t2sq3q6Q/sHZ0gM+40eg28mmY1rfA+ZNROLByPeL3HeQepEJYAFQp3Hp2Q7+XXkArby/czS/AqW2/4ciGzbh6Llnp0UhhLbp4oveEMWjftzdEBKd37cX+let4hdoa4GHbTr0Cs1AtdvbIMZw9cgxNO7RDzzEj4D18CHo+OwKpMXGI2LgF0Tt3c/deRYxN68EzYCB8xo2Co5srCnJysXfZahxZvwk3M68pPR79Ce4BUIWYWlqiy9OD0WN0IOxbNMOdW3k4EboDET9vQeb5FKXHoyrSqIkzeo55Bl1HDIWZpSXSE8/h8NqNOLltF0oK+QtATcNDQFTlWnTuhB6jA9HBry/0xsa4cDIaERu34PSucJQUFSk9HlWQVqeDu68Pej77DNx6dkNpcQli9+zDoTU/IyXqtNLj0SOwAKjamDewgvfwIeg+OhC2Lk1QkHsTJ7ZuR8TPW3Dt4iWlx6PHZO3ogG4jnkbXwKGwsrdFbkYmIjb+gmObtiLv+g2lx6NyYAFQtdNoNGjVtTO6jw6ERz9f6Iz0SD5+EhE/b0Hsnv0oLS5WekR6CJ2REdr17Y3uI4bBtUdXAEDioQgc3fgLEg4c4TX4axk+CUzVTkSQdOwEko6dQP1GDeEdOBTdRw3H81/8E3k3shH5yzYc/fkX3LicrvSo9Duntq3RNXAoPAMGwryBFXIzMhH2/TIc3/wrcjMylR6PKhn3AKhaaTQatO7RFd1HB6JdHx/o9HqciziOyK3bcWbfIdzNL1B6RNWxsreF1xB/dB46CI1dW6K4sBBxe/bj+JZtSDp2gufu1wE8BEQ1jqWtDbqOGIZuI4ahoWNjlBQV4eyR4zgdFo74fQdx51ae0iPWWaaWlugwwBeegweiZVcvaLVaXIyOxYnQHYjeuZvf+zqGBUA1lkajQdMO7dDBry86+PVFQ8fGKC0uwbljkTi9Kxzx4Qd4zZhKUK++Bdr16Y1Og/rDrUc36Iz0uJaahlO/7sTJX3/jobg6jAVAtUaTdm3RYWBfdBzYD42cnVBaUoLzkacQExaOuD37kZ+do/SItYZFQ2u07/cUPPr3gWu3LtAZ6ZF95Spidu5B1M4wpCecU3pEqgYsAKqVnNq0Rge/e2Vg26wpDKWluHAyGqfDwnF69z6ehvgAjVu3hPtTPnD37YWmHdpBq9XietplxIbtQ+ze/bh0Op7vq6syLACq9RxcW6KjX190GNgPDi2bAwCunE1CcuQpnI+MwvkTUbhz65bCU1Y/MytLuHb3Ruvu3nDr1Q3WjR0AAJfiziBh/2HE7t2Pq+fOKzwlKYkFQHWKfYtmaN/PF626eqFZpw4wNq0Hg8GAq+eSfy+EU7hwMrpOPplpammJ5p4d0MKrI1p16wyntm7QarW4k5eP5OMncWb/YSQcPMK9IyrDAqA6S6fXo6mHO1p6e6GVd2c06+QBo3omMBgMuJKYhPMn7pXBlbNJyLmSUasOf2h1Oti1aIYm7dqgaXt3NPfqiMauLQEAJUVFSI2Nx7mISJyLOI7L8Yl8gRY9UI0sgODgYAwdOhRZWVnw8PD409uzAKg8dEZGcOnQ7vdC8IJLx/YwMjEBANwtKEBG8gVcPXceV5N+z7nzNeLQkUVDa9i3aAb7ls1h37I5nNq0hlOb1jA2rQcAuJOXj9SYOFw4FY2UUzG4FJfAC69RudTIAujduzfy8/OxcuVKFgBVGb2xMRzbuKKxa0s4tm4Fh9//NLOyLLtNbmYWslJScTPzGm5mZuFm1v//mZuZhYLs3ArtOehNTGDewBJmVlawsreFdWMHNHR0gHVjB1g7NYZt0yYwt25Qdvu7+QW4ei4Zl+ITcDk+AWnxibiemlar9l6o5qiRl4I4ePAgXFxclByBVKCkqAiXTsfj0un4+z5vaWcLx9Yt0di1JRq3boVGTZzg2q0z6ts0gk5//49GaXEJCm/fRtHduyi6fQfFdwtRdOcOiu7cQUlRMbRGeuj0emh1Ouj09/6uNzGGmZUlzK2sYFTP5I9zFRcjNyMTOVcyELtnPzLOpyDzfAoyL6TwWvpULWr8tYCmTp2KoKAgAICNjY3C01BdcivrGm5lXUPioaP3fV6j1cKioTUa2NvByt4WVna2sLSzhYmZKYxNTWFsWg/G9erB2NQUJhbmMDcyhqG0FKUlJTCUlqK4sBB3CwpQWlSEy/GJKMi9ids3b+L2zVsoyL2JvGs3kH01A3nXrvM3elKcKBkXFxeJjY0t120jIyMVnZVhGKY25mHbTi2IiEiVWABERCqlaAGsWbMGERERcHNzQ1paGiZPnqzkOEREqqLok8Djx49XcvFERKrGQ0BERCrFAiAiUikWABGRSrEAiIhUigVARKRSLAAiIpViARARqRQLgIhIpVgAREQqxQIgIlIpFgARkUqxAIiIVIoFQESkUiwAIiKVYgEQEakUC4CISKVYAEREKsUCICJSKRYAEZFKsQCIiFSKBUBEpFIsACIilWIBEBGpFAuAiEilWABERCrFAiAiUikWABGRSrEAiIhUigVARKRSihaAv78/EhMTkZSUhHfffVfJUYiIVEexAtBqtfj2228xePBguLu7Y9y4cWjbtq1S4xARqU65CmD37t0YPHjwfZ9bvHhxhRbctWtXJCcnIyUlBcXFxVi3bh2GDx9eocckIqLyK1cBNG/eHO+++y7ef//9ss916dKlQgt2cnJCWlpa2ceXL1+Gk5PTH243depUREZGIjIyEjY2NhVaJhER/b9yFUBubi769+8Pe3t7bN26FZaWllU9V5klS5bA29sb3t7euH79erUtl4ioritXAWg0GpSWlmL69OnYtGkTDh06BDs7uwotOD09HU2aNCn72NnZGenp6RV6TCIiejzyZwkKCrrvYy8vLwkODv7T+z0qOp1Ozp8/L82aNRMjIyOJjo4Wd3f3R94nMjKyQstkGIZRYx6x7VRuqMGDB8vZs2clOTlZZs+eXZGVYBiGYR6Sh2079VDQjh07sGPHDiVHICJSLb4SmIhIpVgAREQqxQIgIlIpFgARkUqxAIiIVIoFQESkUiwAIiKVYgEQEakUC4CISKVYAEREKsUCICJSKRYAEZFKsQCIiFSKBUBEpFIsACIilWIBEBGpFAuAiEilWABERCrFAiAiUikWABGRSrEAiIhUigVARKRSLAAiIpViARARqRQLgIhIpVgAREQqxQIgIlIpFgARkUqxAIiIVEqRAhg1ahTi4uJQWlqKzp07KzECEZHqKVIAcXFxGDFiBA4cOKDE4omICIBeiYUmJiYqsViictNoNHBwsIa9fQM0bGiBRo0s0ahRfTRsWB8NGpjDyEgHIyM9jIx00P/+p1arxZ3bhbh9uxAFBXdRUHD3978XIicnH+npN5CefgNXrmSjuLhE6VUkUqYAHsfUqVMRFBQEALCxsVF4GqpLjIz0cHV1hLt7E7Rt2wTNmtvDxcUWLi52aNLEBsbGRg+83507hSgqKkFxcSmKi///TxGBqakJzMxMYG5u8tD7A0BmZg7S07Nx+fJ1pF++gcTEyzh9OgWxsanIzs6rqlUmuk+VFUBYWBgcHBz+8Pk5c+Zg69at5X6cJUuWYMmSJQCAyMjISpuP1MXKyhxeXi3h7e0Kr86t4OHhAldXR+j1OgCAwWDA1as5SE3NwvHj57Dx58NITc1CRkYObtzIQ3Z2XtmfRUXl++1dr9fBzOxeIdjYWMLJqRGcnBrB2fnen45OjeDiYofevdvB2tqi7H7p6TcQG3sRcbGpOH36ImJjLyIhIa3cyyUqryorAD8/v6p6aKI/1aaNM3x83OHTux26d3dD69ZOZV9LSclEdPQFbA6JwJkzl3DmTBrOnk3HnTuFlTpDSUkpbt26jVu3biMjIwdxcakPvW3jxg3h4eECD49m8OjQDB4eLujbtwNMTO7tRRQVFeP48STsCz+NffticeRIIu7eLarUeUl9avwhIKLyaNWqMfz8PNF/QEf07t0OtrZWAO4dajlyJBE/rtiDEyeScfJkco08xHL1ajauXs3Grl1RZZ/T63VwdXWEh4cLOnduhad82+Nvs0fjvb+PRWFhMY4dO4t94bHYty8WERGJKCwsVnANqDbSAJDqXmhgYCAWLlwIW1tb5ObmIjo6GoMGDfrT+0VGRsLb27saJqSazsLCFH5+nRAQ0AUD/DrBxcUOAHDxYib27YvDoYPxOHgwHklJVxSetHLVr28KHx939OnjgT59O8DLqwV0Oh3u3i3C0aNnsS/8NHbvjkFERCJEqv1Hm2qoh207FSmAJ8UCUDcXFzs8/XQ3DBnqjT592sPY2Ai5ufnYu/c0dodFIywsGufPX1V6zGplaWmG3r3b/V4IHvD0bAGtVosrV25gc0gENm48jIMHz8BgMCg9KimIBUC1Ups2zhgxoieeGdEDnTu3AgAkJKRh26+R+PXXSBw5koCSklKFp6w5GjQwh7+/F0aO6oWAgC4wMzNBZmYOtmw+io0bD2PfvliUlrIM1IYFQLWGq6sjnn22N8Y82xvt27sAACIiErE55Ag2bz6qut/yn5SZmQkCArpg5KheGDKkCywsTHH9+i38suVeGezde5qvR1AJFgDVaI0bN8S4cU9h3Hjfst/0DxyIw88bDmHz5ghcuZKt8IS1m6mpCfz9PTFyVC8MG9YVlpZmyMnJxy+/HMOmjYcRFhbF00zrsEdtO6W2JDIyUvEZmMqLmZmJPPdcX9n520dSXLJFDBIqR4/Nk5kzh4uTUyPF56urMTExkqFDvWX5ipmSnbNWDBIqObnrZOnS18Xb21Xx+ZjKzyO2ncoPVwkrwdSi9OrlLkuXvi43b60Xg4RK8vkl8uGHE6R1ayfFZ1NbjIz0MmhQZwle9obcytsgBgmVEyfnS1DQILGwMFV8PqZywgJgFI2dXQN5++0RkpC4SAwSKrfyNkhw8Azx8XFXfDbmXm2OCgAAAA8/SURBVOrXN5VXXhksUdELxCChcvPWelm06FXp1KmF4rMxFQsLgKn2aDQa8fPzlA0//1UKizaLQUJl3/7PZOLE/mJuXk/x+ZiHp1s3Nwle9oYU3N5Ydmhu8mQ/MTMzUXw25vHDAmCqLY0aWcpf/vKMnEtaLAYJlaxrP8kXX0wWNzdnxWdjHi8NGpjL668Pk9i4b8ueK1i48GVp395F8dmY8ocFwFR5unRxleUrZsrtO5vKftsfO/YpMTbWKz4bU/H06uUuK1e9Wfbve/DQv+T55/tKvXrGis/GPDosAKZKYmyslwkT+kjE0X+XHTdeuPBladeuqeKzMVWThg3ry6xZw8uez7l+Y418+eVL3MOrwWEBMJUae/sG8o9/jJMrV1eKQULlTMIimT59iNSvzzNH1JQ+fTxkzdq35W5hiBgkVML3fSbjxvlyr6+GhQXAVEo6dmwuy1fMlDt37/3Ah/76vvj5eSo+F6NsbG2t5J13RkpS8g9ikFC5nL5C3nwzkKeS1pCwAJgnjkajkYCALhK2++OyUzgXLAgSV1dHxWdjalY0Go0MHOhZ9n/l+o018sEH46Vhw/qKz6bmsACYx46JiZFMmTJQ4s98JwYJldRLy+Qvf3lGrKzMFZ+Nqfnp2rW1bAqZLQYJlbz8n+XLL1/iK7wVCguAKXcaNDCX2bPHlB3fP3lqvowf7yt6vU7x2ZjaF3f3prJ8xUwpKt4idwtDZOnS17n3WM1hATB/GmdnG5k3b0rZJQG2bf9A+vbtoPhcTN2Ii4udLFz4shTc3iglpb/I+g3viqdnS8XnUkNYAMxD4+bmLMHL3pDCos1SVLxFflz5pnh4NFN8LqZuxs6ugXzyyfOSk7tODBIqO3Z+KE891V7xuepyWADMH9Kli6ts3PQ3KSn9RfILNsrXXwdJ06a2is/FqCOWlmby7ruj5GrGvUONhw5/LkOHeotGo1F8troWFgBTFl/f9rLzt4/EIKFyI3utfPTRBLGxsVR8LkadqVfPWKZNC5DzF5aKQUIl5vRCGT/eV3Q6reKz1ZWwABgZNKizHDz0LzFIqFy5ulLefnsEX7jF1Jjo9TqZMKGPnI79puwy4a+8MlhMTIwUn622hwWg0mg0GgkM7C6RJ74Sg4RKysVgefXVAF6/hamx0Wg0MmxYVzl85IuyX1beeWckf1mpQFgAKotWq5XRo30k5vRCMUionEtaLJMmDRAjI75En6k98fVtLzt2figGCZXsnLXy8cfPi62tleJz1bawAFQSrVYr48b5Slz8vcv3xsV/y+OpTK2Pl1dL2fDzX8tOWFiwgCcsPE5YAHU8Op1WJkzoU3aFxpjTC2X0aB/RarnhZ+pO3NycJTh4hhQWbZbCos2ybPlMadOGVyH9s7AA6mj+d8MfFb1ARozoyVPpmDodZ2cb+eqrlyQv/2cpKf1FNm76m3Tpwje0f1hYAHUsWq1Wxo/3Ldvwn4r6WgIDu3PDz6gqjRpZyocfTpAb2WvFIKGyKWS2tG3bRPG5alpYAHUkWq1Wnn22d9kF2qJjFsgzz/Tghp9RdSwsTGXOnDGSk7tOiku2SHDwDGnShM8R/CcsgFoejUYjI0f2LDtH+nTsNzJyJA/1MMx/p1EjS/nii8ly+84muXM3RObNmyKNGvFFjiyAWpxhw7rKqaivxSChEn/mOxkzxocbfoZ5RJydbWTp0teluGSL5N5cL3//+1gxN6+n+FxKpUYVwOeffy4JCQkSExMjISEhYmVVvvN61VYAAwd6ytFj88QgoXL23GKZMKEPz+phmMdImzbO8vPGv4lBQiUjc5W89tpQVb5dZY0qAD8/P9Hp7l1bfu7cuTJ37tyKrkSdylNPtZf9B+aKQULlQspSmTRpAM/jZ5gKpGvX1rJ7z713KTt/Yak891xfVf0yVaMK4L8TGBgoq1evruhK1Il4e7vKb7vuXaQt7fIKeeWVwXzlLsNUYvz8POXEyfllJ1AMGeKt+EzVkRpbAFu3bpUJEyY89OtTp06VyMhIiYyMlJSUFMW/kVWR9u1dJGTzHDFIqGRmrZZZs4bzWj0MU0XRaDQyerSPJJ79XgwSKgcO/kt69XJXfK6qTLUXQFhYmMTGxv4hTz/9dNltZs+eLSEhIZWxErUyLVs2lpWr3pSS0l8kO2etzJkzRiwseMErhqmO6PU6mTrVXy6nrxCDhMrW0Pfr7Bsh1bg9gIkTJ8qRI0fE1LT8G7y6UgCOjg1l0aJXpbBos+QXbJTPPpso1tYWis/FMGqMqamJvPPOSLmRvVZKSn+RH1e+Kc2b2ys+V2WmRhWAv7+/xMfHi42NTWWtRK1Iw4b15fPPJ0nB7Y1ytzBEFi58WRwcrBWfi2EYSIMG5vLppy9IfsG9n88FC4LEzq6B4nNVRmpUASQlJcmlS5ckKipKoqKiZNGiRRVdiRodc/N6Za9SLCn9RZavmCnNmtWt3zAYpq6kceN7e+hFxVvkVt4G+fDDCWJpaab4XBVJjSqAKliJGhkjI71Mnz6k7D1PQzbPkXbtmio+F8Mwf55WrRrLmrVvi0FCJevaTzJr1vBa++5kLIBqjEajkfHjfSX5/BIxSKjsDf9UunVzU3wuhmEeP15eLcvelOZi6rJa+bocFkA1ZfDgzhIVvUAMEionT82XgQM9FZ+JYZiKp2/fDhJx9N9ll2R55pkeis9U3rAAqjjdurlJ+L7PxCChkpT8g4wd+xSv18MwdTCBgd3LrsYbcfTf0qePh+Iz/VlYAFUUNzdn2bjp3rVGrmaslFdfDeCrdxmmjken08qkSQMk9dIyMUio7Nj5oXh6tlR8roeFBVDJcXRsKIsXTy+72uB77z2r6qsNMowaY2JiJLNmDZdr138Sg4TKmrVvS6tWjRWf63/DAqikWFndO1f4P+fyf/XVS2Jjw+uNM4yaY2lpJh99NEHy8n+WouItsmjRq9K4cUPF5/pPWAAVjLGx/r6mX7X6LZ7LzzDMfbGzayALFgTJ3cIQyS/YKJ9++oI0aGCu+FwsgCeMRqORCRP6yIWUpWXH+jp1aqH494JhmJqb5s3ty67zdSN7rbzzzkgxNTVRbB4WwBNkwIBOZZeOPXlqvgwY0Enx7wHDMLUnHTo0k62h74tBQuVy+goJChoker2u2udgATzmP9p/Xvhx/sJSGT/el6d0MgzzxPHxcZeDh/4lBgmVxLPfy+jR1fu2riyAcqRJE1tZvmKmlJT+ItdvrJFZs4ar8u3jGIapmgwd6i0xpxeKQUIl8sRX4udXPS8UZQE8IlZW5jJ37kS5fWeT3L6zSebOnVgjnrhhGKbuRavVynPP9ZXzF+49r7h7z8fi7e1apctkATwgRkZ6mTFjmGRd+6nsKp1Nmtgqvp4Mw9T9GBvr5bXXhkpG5ioxSKj8vPFv0qaNc5UsiwXwPxk92keSkn8Qg4TKb7s+4pk9DMMoEgsLU/n738dK7s31UlyyRZYufV2cnR/vvVL+LCyA39Orl7scifii7E2hebE2hmFqQmxsLGXevCly526I3L6zSf7978nSsGH9Snls1ReAq6ujbAqZLQYJlbTLK2TSpAGi1dauS7oyDFP307SprQQve0OKS7ZITu46mTNnTIUvM6PqAnjvvWelsGiz3Ly1XubMGSNmZsq9IINhGKY8cXdvWvZL69WMldK3b4cnfqyHbTu1UIELFzIQvHQXXFu9jE8+2YDbtwuVHomI6JHOnLmEkSM+RY/uf0FU1AWcO5de6cvQ4F4T1AqRkZHw9vZWegwiolrlYdtOVewBEBHRH7EAiIhUigVARKRSLAAiIpViARARqRQLgIhIpVgAREQqxQIgIlKpWvVCsKysLKSmpio9xiPZ2Njg+vXrSo+hCK67OtcdUPf614Z1d3FxgZ2d3QO/pvg1L+pSlHjj+poSrrvyc3D9ue6PEx4CIiJSKRYAEZFK6QB8oPQQdc2pU6eUHkExXHf1UvP619Z1r1VPAhMRUeXhISAiIpViARARqRQLoJJ9/vnnSEhIQExMDEJCQmBlZaX0SNVq1KhRiIuLQ2lpKTp37qz0ONXC398fiYmJSEpKwrvvvqv0ONUqODgYmZmZiI2NVXqUaufs7Iy9e/ciPj4ecXFxmDFjhtIjPRHFz0WtS/Hz8xOdTicAZO7cuTJ37lzFZ6rOtGnTRlq3bi3h4eHSuXNnxeep6mi1WklOTpbmzZuLkZGRREdHS9u2bRWfq7rSu3dv8fT0lNjYWMVnqe44ODiIp6enABALCws5e/Zsrfu35x5AJQsLC0NpaSkA4OjRo3B2dlZ4ouqVmJiIc+fOKT1GtenatSuSk5ORkpKC4uJirFu3DsOHD1d6rGpz8OBBZGdnKz2GIjIyMhAVFQUAyM/PR0JCApycnBSe6vGwAKrQ5MmTsWPHDqXHoCrk5OSEtLS0so8vX75c6zYCVHEuLi7w9PTEsWPHlB7lseiVHqA2CgsLg4ODwx8+P2fOHGzduhUAMHv2bJSUlOCnn36q7vGqXHnWn0gtzM3NsWnTJsycORN5eXlKj/NYWABPwM/P75FfnzhxIoYOHYr+/ftX00TV68/WX03S09PRpEmTso+dnZ2Rnp6u4ERUnfR6PTZt2oSffvoJmzdvVnqcx8ZDQJXM398f77zzDp5++mncuXNH6XGoikVGRsLV1RXNmjWDkZERxo4dy70gFQkODkZCQgK++uorpUd5Yoo/E12XkpSUJJcuXZKoqCiJioqSRYsWKT5TdSYwMFDS0tLk7t27kpGRITt37lR8pqrO4MGD5ezZs5KcnCyzZ89WfJ7qzJo1a+TKlStSVFQkaWlpMnnyZMVnqq706tVLRERiYmLKft4HDx6s+FyPE14KgohIpXgIiIhIpVgAREQqxQIgIlIpFgARkUqxAIiIVIoFQESkUiwAIiKVYgEQVcCHH36IN954o+zjjz/+uNZeF57USfFXozFMbY2Li4ucPHlSAIhGo5Hk5GRp2LCh4nMxTHnCi8ERVUBqaipu3LiBTp06wd7eHlFRUaq9Pj7VPiwAogpaunQpXnzxRTg4OGDZsmVKj0NUbrwWEFEFGRkZITY2FkZGRnB1dYXBYFB6JKJy4R4AUQUVFxcjPDwcubm53PhTrcICIKogjUaD7t27Y/To0UqPQvRYeBooUQW0bdsWycnJ2LNnD5KTk5Ueh+ix8DkAIiKV4h4AEZFKsQCIiFSKBUBEpFIsACIilWIBEBGp1P8B2gupSJy7okcAAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "plt.plot((a**2-1)/ 2/a, (a**2+1)/2/a)\n",
    "plt.plot(-(a**2-1)/ 2/a, -(a**2+1)/2/a)\n",
    "plt.xlabel(\"y\")\n",
    "plt.ylabel(\"z\")\n",
    "plt.title(\"Cut in plane x=0\")\n",
    "None"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "matrix([[0.],\n",
       "        [0.],\n",
       "        [1.]])"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "d3_rep(R_45)*z_p1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Testing the action of the Lie algebra basis on (0, 0, 1)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We verify that L acts as a rotation around (0, 0, 1):\n",
    "*   It leaves (0, 0, 1) fixed\n",
    "*   It loops on itself (the subgroup $\\{e^{tL}\\}_{t\\in \\mathbb{R}}$ is a circle)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "# verify the fixed point property\n",
    "angle = np.random.uniform(low=0, high=np.pi*2, size=1)\n",
    "angle = angle[0]\n",
    "np.testing.assert_almost_equal(\n",
    "    d3_rep(linalg.expm(L*angle))*z_p1,\n",
    "    z_p1,\n",
    "    err_msg=\"We expect L to act like a rotation around (0, 0, 1)\")\n",
    "\n",
    "# verify the looping property (rotation characterization)\n",
    "np.testing.assert_almost_equal(\n",
    "    d3_rep(linalg.expm(L*np.pi*2)),\n",
    "    np.identity(3),\n",
    "    err_msg=\"We expect 2piL to be back to the identity\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Given the action of L on Q and P, it looks like:\n",
    "\n",
    "__Basis for (0,0,1)__: Q acts as $D_x$, P as $D_y$ and L as $D_\\theta$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## A Cross product for hyperbolic geometry\n",
    "\n",
    "$w \\rightarrow \\det(v_1, v_2, w)$ is a linear form.\n",
    "So there exists a unique vector $v_1 \\times v_2$ such that \n",
    "$$\\det(v_1, v_2, w) = B(v_1 \\times v_2, w)$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "def h_cross(u, v):\n",
    "    \"\"\" Hyperbolic cross-product.\n",
    "    \n",
    "    Args:\n",
    "        u (array-like): 3D vector.\n",
    "        v (array-like): 3D vector.\n",
    "    Returns:\n",
    "        3D vector u x v such that det(u, v, w) = H(u x v, w) forall w.\n",
    "    \"\"\"\n",
    "    u1, u2, u3 = np.asarray(u).reshape(-1)\n",
    "    v1, v2, v3 = np.asarray(v).reshape(-1)\n",
    "    return np.matrix([u2*v3 - u3*v2, -u1*v3+u3*v1, -u1*v2 + u2*v1])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "matrix([[-1.]])"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "e1 = np.matrix([1, 0, 0])\n",
    "e2 = np.matrix([0, 1, 0])\n",
    "e3 = h_cross(e1, e2)\n",
    "1/8*e3*B*np.transpose(e3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Det(e1, e2, w) = 15.999999999999991\n",
      "H(e1 X e2, w) = [[16.]]\n"
     ]
    }
   ],
   "source": [
    "e1 = np.matrix([1.5, -2, 3])\n",
    "e2 = np.matrix([1, 1, -1])\n",
    "e1_X_e2 = h_cross(e1, e2)\n",
    "w = np.matrix([-1, 1, 3]) # a test vector\n",
    "\n",
    "print(\"Det(e1, e2, w) =\", np.linalg.det(np.stack([e1, e2, w])))\n",
    "print(\"H(e1 X e2, w) =\", 1/8*e1_X_e2*B*np.transpose(w))\n",
    "# 1/8*e3*B*np.transpose(e3)\n",
    "\n",
    "# np.stack([e1, e2])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "matrix([[-1. ,  4.5, -3.5]])"
      ]
     },
     "execution_count": 22,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "e1_X_e2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A matrix M respects the bilinear form B, if and only if:\n",
    "$$M^TBM = B$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Metric on the hyperbolic plane"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's check the image of the parabolic subgroup:\n",
    "\n",
    "$$\n",
    "\\mathrm{Ad}\\left(\\exp\\begin{pmatrix}\n",
    "0 & t \\\\\n",
    "0 & 0\n",
    "\\end{pmatrix}\\right)\n",
    "= \\mathrm{Ad}\\begin{pmatrix}\n",
    "1 & t \\\\\n",
    "0 & 1\n",
    "\\end{pmatrix}\n",
    "= \\begin{pmatrix} \n",
    "{2 - t^2 \\over 2} & -t & {-t^2 \\over 2} \\\\\n",
    "t               & 1 & t \\\\\n",
    "{t^2 \\over 2} & t  & {2 + t^2 \\over 2}\n",
    "\\end{pmatrix}$$\n",
    "\n",
    "Image of the Lorentzian boost group:\n",
    "\n",
    "$$\n",
    "\\mathrm{Ad}\\left(\\exp\\begin{pmatrix}\n",
    "\\lambda & 0 \\\\\n",
    "0 & -\\lambda\n",
    "\\end{pmatrix}\\right)\n",
    "=\n",
    "\\mathrm{Ad}\\begin{pmatrix}\n",
    "e^\\lambda & 0 \\\\\n",
    "0 & e^{-\\lambda}\n",
    "\\end{pmatrix} =\\begin{pmatrix} \n",
    "\\cosh2\\lambda  & 0 & -\\sinh2\\lambda \\\\\n",
    "0                         & 1  & 0 \\\\\n",
    "-\\sinh2\\lambda & 0  & \\cosh2\\lambda\n",
    "\\end{pmatrix}$$\n",
    "\n",
    "Image of the rotation subgroup:\n",
    "\n",
    "$$\n",
    "\\mathrm{Ad}\\left(\\exp\\begin{pmatrix}\n",
    "0 & -\\theta \\\\\n",
    "\\theta & 0\n",
    "\\end{pmatrix}\\right)\n",
    "=\n",
    "\\mathrm{Ad}\\begin{pmatrix}\n",
    "\\cos\\theta & -\\sin\\theta \\\\\n",
    "\\sin\\theta & \\cos\\theta\n",
    "\\end{pmatrix} =\n",
    "\\begin{pmatrix} \n",
    "\\cos2\\theta  & \\sin2\\theta & 0 \\\\\n",
    "-\\sin2\\theta & \\cos2\\theta & 0 \\\\\n",
    "0            & 0           & 1\n",
    "\\end{pmatrix}$$\n",
    "\n",
    "\n",
    "Image of another Lorentzian boost subgroup:\n",
    "\n",
    "$$\n",
    "\\mathrm{Ad}\\left(\\exp\\begin{pmatrix}\n",
    "0 & \\alpha\\\\\n",
    "\\alpha & 0\n",
    "\\end{pmatrix}\\right)\n",
    "= \\mathrm{Ad}\\left(\\begin{pmatrix}\n",
    "\\cosh\\alpha & \\sinh\\alpha\\\\\n",
    "\\sinh\\alpha & \\cosh\\alpha\n",
    "\\end{pmatrix}\n",
    "\\right)\n",
    "= \\begin{pmatrix} \n",
    "1 & 0 & 0 \\\\\n",
    "0 & \\cosh2\\alpha & \\sinh2\\alpha \\\\\n",
    "0 & \\sinh2\\alpha & \\cosh2\\alpha\n",
    "\\end{pmatrix}\n",
    "$$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's start from the point (0, 0, 1) of norm -1. (1, 0, 0) is tangent to the hyberboloid at this point.\n",
    "Because the action restricted to SO(2) has spin 2, a rotation of pi/2 is given by $\\phi(R_{\\pi \\over 4})$, which maps (1, 0, 0) to (0, -1, 0).\n",
    "So now we know that (1, 0, 0) and (0, -1, 0) form a local orthogonal basis for the tangent space at (0,0,1)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "py36_datascience",
   "language": "python",
   "name": "py36_datascience"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
