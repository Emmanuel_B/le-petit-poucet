Source repository for my space-time blog.

In this blog, we explore different aspects of space, some of them mathematical, some of them more intuitive.

## Local Development

You will need to have make and docker compose installed.

Use `make development` ! 

It will start a livereload server.


## How to deploy

The website is hosted on gitlab pages.
The deployment is handled by `pages` job in the pipeline.
Deployment is automatic when pushin on the main branch.

## Old and Deprecated

### How to run the tests

From "root/le-petit-poucet" run `npm test`

